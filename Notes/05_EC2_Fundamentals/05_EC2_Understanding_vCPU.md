# EC2 — Understanding vCPU

* Multiple threads per CPU (multithreading).
* Each thread is a virtual CPU (vCPU)

## Example: m5.2xlarge

* 4 CPU.
* 2 threads per CPU.
* = 8 vCPU in total.

## EC2 — Optimizing CPU options

* EC2 instances come with a combination of RAM and vCPU.
* In some cases, you want to change the vCPU options:
* n° of CPU cores. Usually to decrease licensing (per core) costs. You keep the RAM and reduce CPUs.
* n° of threads per core. Disable multithreading to have 1 thread per core. For HPC workloads.
