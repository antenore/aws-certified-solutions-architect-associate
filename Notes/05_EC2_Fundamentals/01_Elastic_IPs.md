# Elastic IPs

* Static IP associated to an EC2 instance, or NIC
* Allocated to the account.
* Charged hourly if not used, and NOT released from the account.
* Free of charge if used/associated.
* Can be disassociated and associated multiple times.
* You can use your own IP brought to AWS. It doesn't count for the limits.
* MAX 5 Elastic IP per region.
* Mainly used to remap to another instance in case of failure.
