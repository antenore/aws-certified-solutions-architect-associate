# EC2 Nitro

* The new AWS EC2 virtualisation technology.
* Better performance.
* Better networking.
* Higher EBS speed (64000 EBS IOPS, ma 32000 on non-Nitro)
* Better underlying security.
* Not all instance type and not all Bare metal uses Nitro.
* Remember, when you need High Speed EBS, you choose a Nitro based instance type.
