# EC2 — Amazon Machine Image (AMI)

* Set of customization of an EC2 instance.
* OS, config, additional software, monitoring, …
* AMI are built for a specific region and can be copied across regions.
* There are 3 types of AMI:
    * Public AMI: Provided by AWS.
    * Your own AMI. Create EC2 instance, customize, make an AMI, use it.
    * AWS Marketplace AMI, made by third parties.
