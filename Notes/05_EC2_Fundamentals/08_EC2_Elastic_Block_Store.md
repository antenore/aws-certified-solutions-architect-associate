# EBS – Elastic Block Store

* Network drive that can be attached to an EC2 instance.
* Can be hot-plugged.
* Data is persistent across reboot.
* Only one instance at a time.
* Multiple EBS per instance.
* Locked to an AZ.
* 30 GB of free EBS storage per Month, General Purpose (SSD) or Magnetic.
* They have a bit of latency due to the fact they are network based.
* You can move an EBS through snapshots.
* It's provisioned with a size in GB and IOPS capacity.
* Billed for that capacity, can be scaled up or down.
* Delete on Termination flag, can be set and unset.
* Root is deleted by default, it can be turned off to preserve data.
* Other EBS volume are not terminated, by default.
* Good for long term storage.
* Good but limited performance.

## EBS Snapshots

* Backup of an EBS volume at a point in time (snapshot).
* Better to detach the volume, but not required.
* Often used to move EBS volume (the content) to different AZ and Regions.
* Or for disaster recovery strategy.
* Snapshots are billed for the space the occupy.

## EBS Snapshots Archive.

* Moved to an "Archive tier", 75% cheaper, 24 to 72 hours to restore.

## EBS Snapshots Recycle Bin.

* Setup rules to retain deleted snapshots.
* Retention 1 day to 1 Year.

## EBS Volume types

* gp2 / gp3 (SSD): General purpose SSD volumes. For most common workloads.
* io1 / io2 (SSD): Highest performance volumes, mission critical low-latency or high-throughput workloads.
* st1 (HDD): Low cost HDD for frequently accessed, throughput-intensive workloads.
* sc1 (HDD): Lowest cost HDD for less frequently accessed workloads.
* Size, Throughput, IOPS (I/O Ops per seconds).
* gp2, gp3, io1, io2 only can be used as boot volumes.

### General Purpose SSD

* Cost effective
* System boot, Virtual desktops, Dev and Test environments.
* 1 GiB – 16 TiB
* gp3: Baseline 3000 IOPS, 125 MiB/s, Can increase IOPS up to 16000, and up to 1000 MiB/s **independently**
* gp2: IOPS and size are linked, max IOPS is 16000. So with 3 IOPS we can have 5334 GB MAX.

## EBS Volume Types Use cases

### Provisioned IOPS (PIOPS) SSD

* Critical business applications with high IOPS performance
* Or applications that needs more than 16000 IOPS.
* Great for DB workloads.
* io1, io2 (4 GiB — 16 TiB): Max PIOPS: 64000 for Nitro instances and 32000 for the others.
* PIOPS can be increased **independently** of storage size.
* io2 have more durability and more IOPS per GiB __at the same price of io1!!!__
* io2 Block Express (4 GiB - 64 TiB): Sub-millisecond latency, Max 256000 PIOPS (1000:1 IOPS:GiB ratio).
* Support **EBS Multi-attach**

### HDD Based

* Cannot boot.
* 125 MiB to 16 TiB.
* Throughput optimized (stl): (500 MiB/s, 500 IOPS) normally used for Data Warehouse, Log Processing, Big Data.
* Cold HDD (scl): When lower cost is important. 250 MiB, 250 IOPS. For infrequent data access.

##  EBS Multi-Attach – io1/io2 family

* Can be attached to multiple instances in the same AZ
* Max 3 Instances.
* Each instance R/W permissions.
* Higher application availability in clustered applications.
* Apps must manage concurrent write operations.

## EBS Encryption

* Encryption at rest and in flight.
* Snapshots are encrypted, if the EBS is encrypted.
* Restored snapshots are encrypted, as well.
* You can Encrypt a volume from an unencrypted snapshot, etc.
* It's done transparently by AWS.
* Minimal impact on latency.
* It uses KMS keys (AES-256).
