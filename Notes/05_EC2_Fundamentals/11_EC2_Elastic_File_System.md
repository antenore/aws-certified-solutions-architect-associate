# EC2 - Elastic File System

* Managed NFS, can be mounted on many EC2.
* Multi AZ.
* Highly available, scalable, expensive (3x gp2), pay per use.
* EFS -> **Security Group** -> AZ1, AZ2, etc -> EC2 Instances.
* NFSv4.1 protocol.
* Use cases: CM, web content, data sharing, Wordpress, etc
* Only compatible with Linux based AMI.
* Can be encrypted at rest.
* POSIX file system, with a standard file API.
* Scale automatically, pay-per-use, no capacity planning.

## EFS Performace and Storage Classes.

When we setup an EFS we have different options.

### EFS Scale

* 1000 concurrent NFS clients, 10 GB+/s throughput.
* Grow automatically to Petabyte-scale NFS.

### Perfomance mode (at creation time)

* General purpose (default) latency-sensitive use cases (web server, CMS, etc).
* Max I/O - higher latency, throughput, highly parallel (big data, media processing).

### Throughput mode

* Bursting (1 TB = 50 MiB/s + burst of up to 100 MiB/s)
* Provisioned: You choose the throughput regardless of the storage size.

### EFS - Storage Classes

#### Storage Tiers (lifecycle m. - move file after N days)

* Standard: Frequently accessed files.
* Infrequent access (EFS-IA): cost to retrieve, lower to store. You need to enable a Lifecicle policy.

#### Availability and durability

* Regional: Multi-AZ, great for Prod.
* One Zone: Great for dev. Backup enabled, compatible with EFS One Zone-IA. Over 90% in cost savings.
