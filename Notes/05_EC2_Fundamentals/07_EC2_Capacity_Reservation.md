# EC2 — Capacity Reservation

* Ensure you have Capacity when you need.
* Manual or planned end-date.
* No need for 1 – 3 years commitment.
* It's immediate, you get billed as soon as it starts.
* You choose the AZ (only one), the n° of instances, the instances attributes (type, OS, tenancy, etc.)
* It can be combined with Reserved Instances and Saving Plans.
