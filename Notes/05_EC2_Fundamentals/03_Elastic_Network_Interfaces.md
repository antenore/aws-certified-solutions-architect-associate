# Elastic Network Interfaces (ENI).

* Logical VPC component that represent a virtual network card.
* One Primary private IPv4.
* One Elastic IPv4 per private IPv4.
* One public IPv4.
* One or more secondary IPv4 IPs.
* One or more security groups.
* A MAC address.
* Can be attached to EC2 instances on the fly (moved).
* Good for failover recovering.
* Bound to a specific AZ.
* When creating an EC2 instance, we have an ENI by default, with primary and secondary IPs.
* Manually created ENI are not destroyed when we shut down instances (except if has Delete on Terminating flag).

## ENI use cases:

* Management Network / Back-net.
* Multi-Interface Applications.
* MAC-Based Licensing.
* Low-Budget High Availability.
