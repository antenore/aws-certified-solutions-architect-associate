# EC2 - Instance Store

* High-performance hardware disk
* Directly attached to the instance
* Better I/O performance.
* Ephemeral (lost when stopped).
* Good for buffer, caches, scratch data, temporary content.
* Risk of data loss.
* Backups and Replication in charge to you.
