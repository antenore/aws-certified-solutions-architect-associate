# EC2 Hibernate

* Like OS hibernate +
* The RAM state is saved in a file inside the EBS volume.
* You must have enough free space on the EBS
* The root volume must be EBS, encrypted, and large.
* Instance RAM must be Max. 150 GB.
* Not all instance types support it (>= C3 =< C5, I3, M3, M4, R3, R4, T2, T3, …).
* Not supported on bare metal instances.
* On-Demand, Reserved, and Spot instances.
* Max. 60 days.
