# Placement Groups

* The EC2 service attempts to place instances spread across underlying hardware to minimize failure.
* Placement groups allow to fine tune the placement.
* MAX 500 Placement groups per account per Region.
* Must have a unique name within your AWS account for the region.
* Placement groups cannot be merged.
* An instance cannot span multiple PG.
* Cannot reserve capacity for a PG, only for each EC2.
* Cannot use Dedicated Hosts in PG.

## Cluster placement group

* For HPC apps, all instances close together in the same AZ.
* Low network latency.
* high network throughput.
* Recommended when high traffic between EC2 instances.
* Better launch all EC2 instances in one shot (to get enough hardware).
* Better use the same instance type.
* Adding instances to a cluster may result in a capacity error (stop/start all instances).
* Not all instance types are supported (current generation, T2, Mac1, some few previous gen, etc.)
* The slowest instance will limit the network speed of the cluster.
* Up to 10 Gbps between cluster' instances.
* Up to 5 Gbps Internet traffic, and AWS Direct Connect (to on-premises).
* Up to 5 Gbps between instances that are not in a cluster PG.

## Partition placement group

* Partition 1 don't share HW with partition 2. Etc.
* Distributed and replicated workloads (Hadoop, Cassandra, Kafka, HDFS, HBase, etc.).
* Each partition has its own set of racks.
* Max 7 partitions for each AZ.
* Can have partitions in multiple AZ of the same region.
* MAX 2 partitions when using Dedicated Instances.

## Spread placement group

* Strictly place a small group of instances across distinct underlying hardware.
* To reduce failures.
* Each instance is in its own rack.
* MAX 7 running instances per AZ.
* If 3 AZ in a region, 21 instances MAX.
* Can span multiple AZ on the same region.
* It may fail if there's not enough unique hardware available. Try again later (much later).
* Dedicated Instances are NOT supported.
