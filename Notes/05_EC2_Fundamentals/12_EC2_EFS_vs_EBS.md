# EBS vs EFS

## Elastic Block Store

* EBS volumes can be attached to only one instance at a time.
* EBS are locked at the AZ level.
* To migrate an EBS volume we use snapshosts.
* By default EBS are terminated with the EC2 instance, it can be configured.

## Elastic File System

* NFS can be mounted on 100s of instances across AZ.
* Only for Linux.
* Good for website files (Wordpress).
* More expensive then EBS.
* Using EFS-IA (infrequent Access) can reduce costs.
