# Amazon CloudTrail

* Governance, compliance and audit
* Enabled by default
* History of events and or API calls within AWS account
    * Console
    * SDK
    * CLI
    * AWS Services
* CloudTrail → CloudWatch Logs or S3
* Trail is applied to All Regions (default) or a single Region.
* Who did what? When? Who deleted resources? To investigate.

## CloudTrail Events:

* Management Events:
    * Operations performed on AWS resources.
    * Enabled by default
    * Can be separated between Read Events from Write Events.
* Data Events:
    * By default, **not logged**.
    * Amazon S3 object-level activity (GetObject, DeleteObject, PutObject), Read/Write Events.
    * AWS Lambda function execution activity (Invoke API)

## CloudTrail Insights

* Enable to detect unusual activity
    * inaccurate resource provisioning.
    * service limits
    * Burst of AWS IAM actions.
    * Gaps in periodic maintenance activity.
    * …
* CloudTrail Insights creates a baseline analyzing normal management events.
* And then continuously analyzes **write** events to detect unusual patters.
    * → CloudTrail Console
    * → Amazon S3 (optional)
    * → Amazon EventBridge (optional)
* CloudTrail Events Retention:
    * 90 Days by default
    * Can send to S3 and use Athena to query for long-term retention.
