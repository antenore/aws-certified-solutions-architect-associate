# AWS CloudWatch

* Metrics for each AWS service
* Namespace -> Metric -> Dimension (attribute)
* Up to 10 dimensions per metric
* Metrics have timestamps.
* Dashboard of metrics.

## EC2 Detailed monitoring

* By default, metric every 5 minutes
* Enable detailed monitoring (paying) to get every 1 minute.
* **Needed to scale faster with ASG**
* Free Tier — 10 detailed monitoring metrics.
* **N.B.**: Memory is not pushed, a custom metric pushed within the EC2 is required

## CloudWatch Cutom Metrics

* Example: RAM usage, disk space, logged-in users, …
* Using API **PutMetricData** (CLI or SDK)
* Can set dimensions (attributes) to segment metrics:
    * Instance.id
    * Environment.name
    * …
* Metric resolution
    * Standard: 60 secs.
    * High Resolution: 1, 5, 10, 30 seconds for a higher cost.
* **N.B.** Data points are accepted with up to two weeks in the past and two hours in the future.

## CloudWatch Dashboards

* Pricing:
    * 3 dashboards with up to 50 metrics for free
    * $3/dashboard/month afterwards

## CloudWatch Logs

* **Log groups**: arbitrary names to group logs
* **Log stream**:
* Log expiration policy (you pay for log storage)
* Destinations (optional):
    * S3
    * Kinesis Data Streams
    * Kinesis Data Firehose
    * AWS Lambda
    * ElasticSearch
* Sources:
    * SDK, CloudWatch Logs Agent, CloudWatch Unified Agent.
    * Elastic Beanstalk: Application logs collection
    * ECS: collection from containers
    * AWS Lambda: Collection form function logs.
    * VPC Flow logs
    * API Gateway
    * CloudTrail based on filter
    * Route53.
    * WorkMail, SES
    * … Many other (all probably)

## Filters and Insight

* Filter expression (like by IP, etc)
* Filters can be used to trigger CloudWatch alarms.
* CloudWatch Logs Insight to query logs and add queries to the CloudWatch Dashboard

## S3 Export

* Up to 12 hours before Log data is available
* API -> CreateExportTask
* To have real-time use Log Subscription instead

## Log Subscription

* Subscription filters -> Lambda, or Kinesis

## Logs Aggregation, Multi-account and Multi region

* Using Subscription Filters

## CloudWatch Logs for EC2

* No logs by default from EC2
* Setup CloudWatch Agent
* Needs IAM permissions.
* Also on premises.
* CloudWatch Logs Agent:
    * Old Agent
    * Only to CloudWatch Logs
* CloudWatch Unified Agent
    * additional metrics such as RAM, processes, etc
    * To CloudWatch like Logs Agent
    * Centralized configuration through SSM Parameter Store
    * Out Of The Box: CPU, Disk metrics, RAM, Netstat, Processes, Swap Space

## CloudWatch Alarms.

* Alarms States: OK, INSUFFICIENT_DATA, ALARM
* Period:
    * Number of seconds to evaluate the metric
    * High resolution for custom metrics: 10, 30 or multiple of 60 secs.
* Targets:
    * Stop, Terminate, Reboot, Recover actions
    * Auto Scaling Action
    * SNS notifications -> Everything else.
* Can be triggered by hand with the CLI with `aws cloudwatch set-alarm-state …` for testing purposes

## CloudWatch Events

* Event Pattern: Intercepts events from AWS services
    * S3, EC2 Instances start, CodeBuild Failure, Trusted Advisor, etc.
    * With CloudTrail can intercept any API call.
* Scheduling / Cron: Pretty much
* It generates a JSON payload to be passed to the target:
    * Lambda, Batch, ECS Task.
    * SQS, SNS, Kinesis Data Streams, Kinesis Data Firehose
    * Step functions, CodePipeline, CodeBuild
    * SSM, EC2 Actions
    * …

