# AWS Config

* Auditing and recording compliance of AWS resources
* Records configuratons and changes over time.
* i.e.:
    * Unrestricted SSH access to a security group
    * Buckets with public access?
    * How ALB config have changed over time?
* Can receive SSNS notifications
* Per-region based.
* Can be aggregated across regions, and accounts.
* Can store on S3 and use Athena to analyze.

## Config Rules

* > 75 AWS managed Rules
* Define your own custom rules using AWS Lambda.

## Pricing

* No free-tier
* $0.003 per configuration item recorded per region.
* $0.001 per config rule evaluation per region.

## AWS Config Resource

* View compliance of a resource over timer
* View configuration of a resource over time
* View CloudTrail API calls of a resource over time

## Remediation

* AWS Config doesn't change anything, it's an audit tool.
* Automated remediation of non-compliant resources can be done with SSM Automation Documents.

## Notifications

* We can use EventBridge to trigger notifications
* AWS Config → Triggers → EventBridge → (Lambda, SNS, SQS, …)
* OR
* AWS Config → SNS → Admin


