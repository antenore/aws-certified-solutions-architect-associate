# Amazon EventBridge

* CloudWatch Events Evolution
* Default Event Bus: To receive events from AWS Services (CloudWatch events)
* Partner Event Bus: from SaaS services or Applications (Zendesk, DataDog, Segment, etc.)
* Custom Event Bus: To create your own events from your own application
* Thanks to **resource Policies**, these events come from other accounts.
* Can archive events, by all or a filter, for ever or a period.
* Archives are good to **replay** events.
* Rules: how to process events, the same as CloudWatch Events.

## Schema Registry

* It can infer the schema analyzing the events.
* With the Schema Registry we can generate code for the applications
* The application, in this way, knows the event data structure in advance.

## Amazon EventBridge — Resource-based Policy

* To manage permissions for a specific Event Bus
* It can also allow/deny events from another AWS account or Region.
* Usually used to aggregate all events from an AWS Organization in a single AWS account or AWS region.

## Notes

* EventsBridge extends Cloud Events
* **EventBridge substitutes CloudWatch Events**
