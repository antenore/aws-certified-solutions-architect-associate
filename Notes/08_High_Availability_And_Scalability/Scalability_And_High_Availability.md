# Scalability And High Availability

* Vertical Scalability
* Horizontal Scalability => Elasticity
* Scalability is linked but different to High Availability

## Vertical Scalability

* Increase the size
* Scale up, a bigger machine, Scale down smaller machines.
* There's a physical limit to the size, usually hardware limit.

## Horizontal Scalability

* Increase the number of instances.
* Distributed systems.
* Not all applications can be distributed.
* Scale-out (more), scale-in (less).
* Auto Scaling Group, and
* Load Balancer

## High Availability

* Usually goes with horizontal scalability.
* At least in two data centers (AZ).
* The goal is to survive a data center loss.
* Passive availability (e.g. RDS Multi AZ) one active at a time.
* Active availability, all active (e.g. in horizontal scaling).
* Auto Scaling Group multi AZ and
* Load Balancer multi AZ.
