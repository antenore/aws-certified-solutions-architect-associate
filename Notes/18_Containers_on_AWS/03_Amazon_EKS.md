# Amazon Elastic Kubernetes Service (EKS)

* Managed Kubernetes cluster on AWS
* Alternative to ECS, with different API
* EKS supports EC2 or Fargate
* Use case: Kubernetes on premises -> migration to EKS
