# Amazon ECS

## EC2 Launch Type

* ECS = Elastic Container Service
* ECS is a cluster composed of N EC2 instances
* With EC2 launch Type, we provision and manage the EC2 instances
* You must install the ECS agent
* ECS stop/start containers

## Fargate Launch Type

* It's all serverless, it's fully managedA
* You create tasks, and Fargate do the rest
* To scale just increase the number of tasks

## ECS IAM roles

* EC2 Instance Profile for EC2 Launch Type
    * Used by ECS agent
    * API calls to ECS service
    * Container logs to CloudWatch
    * Pull docker imgaes from ECR
    * Access sensitive data in Secret Manager or SSM Parameter Store

* ECS Task Role (Fargate)
    * Each task can have a role.
    * Use different roles for each ECS service you run

## ECS Load Balancer integrations

* ALB recommended. Works for most cases
* NLB recommended only for high throughput, paired, usually, with AWS Private Link
* ELB (old). Not recommended. No Fargate, no advanced features.

## Data Volumes (EFS)

* EFS (network file system) can be directly mounted onto ECS tasks
* Fargate + EFS = Serverless it's the best solution
* Persistent multi-AZ shared storage for containers.

* **NB** FSx Lustre not supported, S3 cannot be mounted as a FS

## ECS Auto Scaling Service

* AWS Application Auto Scaling
    * Average CPU, or
    * Average Memory, scale on RAM
    * ALB requests count per target - ALB metrics
* Target Tracking - Scaling based on specific CloudWatch metric set as target
* Step Scaling - Scaling based on CloudWatch Alarms
* Schedule Scaling - Predictable charges, date/time
* ECS Auto Scaling ≠ EC2 Auto Scaling
* Fargate Auto Scaling is much easier

## Auto Scaling EC2 Instances

* Using Auto Scaling Group Scaling, or
* **ECS Cluster Capacity Provider**
    * Automatic based on ECS Tasks needs/charges
    * Paired with ASG

## ECS Rolling Updates

* During an update we can set thresholds for minimum and max capacity
* ECS with stop/start tasks in order to roll new versions in the availability capacity frame.
* If you set 4 on 8 tasks (50%), ECS will stop 4, create new tasks with v2, stop again 4, and roll up new 4 v2
* If you set min 100% and max 150%, ECS won't terminate, but will add new instances with v2, up to 150%
* Afterwards it will stop X instances to go back to 100%, and again, and again till all are at v2

## ECS tasks launched by Event Bridge

* Complete serverless example
* Client upload object to s3
* S3 generate event to EventBridge
* That invoke an ECS task through Fargate.
* The tasks, using a docker image will process the object, i.e. an image
* And finally save the result in a DynamoDB

OR

* Using EventBridge to schedule the run of an ECS tasks (Fargate) every hour.





