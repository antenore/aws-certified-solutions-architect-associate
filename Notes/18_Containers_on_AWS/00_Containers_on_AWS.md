## Docker introduction

* Use cases on AWS
    * Microservices architecture,
    * lift-and-shift apps from on-premises to AWS
* You can use Docker Hub or
* Amazon provide Amazon Elastic Container Registry (ECR)
    * Can be a private repository or
    * The Amazon Public repository (Amazon ECR Public Gallery)
* You can build your own Dockers and run it on an EC2 instance, or
* Using Amazon Elastic Container Service (Amazon ECS), or
* Amazon Elastic Kubernetes Service (Amazon EKS), and finally
* AWS Fargate
    * Amazon's own Serverless container platform
    * Works with ECS and EKS
