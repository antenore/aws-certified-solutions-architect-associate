# Amazon Elastic Container Registry (ECR)

* Amazon own Docker repository
* Private and Public ([Amazon Public Gallery](https://gallery.ecr.aws))
* Integrate with ECS, backed by S3
* Access control through IAM (error => policy)
* Image vulnerability scanning, versioning, tags, lifecycle, …)


