# Amazon MQ

* Fully AWS managed Apache ActiveMQ
* Usually used to migrate existing on-premises MQ to AWS
* Does not scale as SQS or SNS
* Active/passive high availability model.
