# Amazon SQS

* Producers send messages in the SQS Queue
* Consumers poll messages in the Queue
* Older offering
* Fully managed, to decouple applications.
* Unlimited throughput, unlimited number of messages in queue
* 4 up to 14 days message retention
* Low latency (<10 ms)
* Max 256 KB message size.
* Can occasionally have duplicated messages
* Best-effort message ordering.

## SQS — Producing messages

* With the SDK
* Persisted till consumed.
* Up to 256 KB.
* On EC2, private servers, AWS Lambda, …

## SQS — Consuming messages

* On EC2, private servers, AWS Lambda, …
* Poll SQS for messages, receives up to 10 messages at a time.
* Process the messages.
* Delete the messages.
* Messages are sent in parallel, that´s why is consumer responsability to delete, and deal with duplicated messages.
* We could need to scale (ASG) the consumer to consume many messages.

Example, Web application that process videos, the processing is done on the back
The back-end receive messages to process videos, when it´s done the video is
uploaded into S3, and the user can download it.
Think when you order a report on PayPal, or some online banking apps.

* encryption
    * In-flight HTTPS
    * At-rest KMS
    * Client side (customer responsability)
* Access control: IAM polocies for SQS API calls
* SQS Access Policies: Like S3
    * Cross-account access
    * For other sefvices to write to SQSA

## SQS — Access Policy

* In Cross Account Access for instance.
* S3 event notifications in SQS (S3 needs perms to publish to SQS)

## SQS Message Timeout Visibility

* When a consumer get a message, during (default) 30 seconds the message is invisible to other consumers
* After 30 seconds it becomes again visible and if hasn´t beed deleted can be treated twice.
* The consumer App can change this timeout at runtime with ChangeMessageVisibility (0 to 43200 seconds or 12 hours)
* It´s like to say 'wait I didn´t finish to process'
* Can be set in the console (the default) or using the SDKA

## SQS — Deall Letter Queue

* We can set a threshold of how many times the message can go back in the queue (after each ChangeMessageVisibility timout)
* After the MaximumReceives threshold the message goes in the dead letter queue.
* Useful for debugging
* It expires as well, normally set to 14 days.
* We can redrive the dead messages in the normal queu after we fixed the problem.
* We must create the DLQ with the parameters you want and set this as the Dead Letter Queue of your real Queue.

## SQS — Delay Queues

* Default 0, no delay
* We can also use this with the SDK with DelaySeconds

## SQS Long Polling

* LongPolling decreases the number of API calls and latency, instead of continuously polling, we wait longer
* WaitTimeSeconds

## SQS Request-Response Systems

* Remember SQS Temporary Queue Client
* Cost effective, it uses virtual queues instead of creating/deleting SQS queue

## SQS FIFO Queue

* Limited throughput
* Perfect order
* Name must end with .fifo

# SQS with Auto Scaling Group (ASG)

* Using CloudWatch custom metric based on the Queue Length
* The ASG will be done on the EC2 instances (consumers)
* So, generally, we have an ASG in front, our producers, and one ASG on the back
* SQS itself can scale with ASG as well.
