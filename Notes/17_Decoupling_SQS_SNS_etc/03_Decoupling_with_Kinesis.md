# Amazon Kinesis

* Real-time collect, process, analyze data, like appl logs, metrics, IoT, website clickstreams., …
* Kinesis products
    * K. Data Streams: Capture, process and store
    * K. Data Firehose: Load data into AWS data store
    * K. Data Analytics: Analyze with SQL or Apache Flink
    * K. Video Streams: Capture, process and store video streams

## K. Data Streams

* Queues = **Shards**
* App, or client, SDK, Kinesis agents, Kinesis Producer Library (KPL) are producers
    * Send **records** to Data Streams through Shards
        * Records = Partition key + Data Blob
    * Records are sent to the Consumers:
        * Apps, SDK, Kinesis Client Library (KCL)
        * Lambda
        * K. Data Firehose
        * K. Data Analytic
        * Record have an added **sequence number** based on the shard
* up to 365 days retention
* Can reprocess/**replay** data
* Data cannot be deleted (immutability)
* Auto ordering, same partition -> same shard

### Pricing model

* Provisioned mode:
    * You choose n° of shards, scale manually, or API
    * 1MB/s IN (1000 records per sec)
    * 2MB/s OUT (classic or fan-out model)
    * Pay per shard per hour
* On-demand mode:
    * Scale automatically, no manual provisioning.
    * 4MB/s IN by def.
    * Pay per stream per hour and data in/out per GB

## K. Data Firehose

* Fully managed Service and serverless.
* Can read mainly from K. Data Streams, or
    * Apps
    * Clients
    * SDK, KPL
    * Kinesis Agents
    * CloudWatch
    * AWS IoT
* Record up to 1 MB
* Many data formats, conversions and compression or
* Custom transformations using Lambda functions
* All or failed data can be sent to an S3 backup bucket
* Batch writes data to a Data Base. (in batches, not sequential)
    * S3
    * Redshift (COPY through S3)
    * Amazon ElasticSearch
    * 3rd parties destinations
        * Datalog, MongoDB, New Relic, Splunk, …
    * Custom destination using API and HTTP/S Endpoint
* **Pay** for data going through Firehose.
* **Near** Real Time

### K. Data Streams vs K. Data Firehose

* Scalability, manual, automatic (KDF)
* Replay, not in KDF
* Persistence, Not in KDF
* Fully managed, only KDF
* Auto transformation and load data to AWS and 3rd parties. Out-Of-Box in KDF
* Speed, KDS is faster, KDF Near Real Time

## K. Data Nalytics (SQL appl)

* Source KDS or KDF
* KDA -> again to KDF or KDS.
    * KDS -> Lambda --> anywhere
    * KDF -> S3, Redshift, etc
* Real-time analytics on Kinesis Streams using SQL
* Fully managed, auto-scale
* Pay for actual consumption rate.
* Can create streams out of the real-time queries.
* Use cases:
    * Time-series analytics
    * Real-time dashboards
    * Real-time metrics



