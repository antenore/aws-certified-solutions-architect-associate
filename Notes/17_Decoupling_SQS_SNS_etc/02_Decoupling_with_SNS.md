# Amazon SNS

* Publisher/subscriber pattern
* Even SQS can be a subscriber of SNS.
* The publisher (event producer) sends messages to one SNS topic
* Up to 12500000 subscription per topic.
* Up to 100000 topics limit.
* Subscribers:
    * Emails, SMS and mobile notifications, HTTP/S Endpoints
    * SQS
    * Lambda
    * Kinesis Data Firehose
* Publishers can be most of the AWS services, or custom one using the API.

## How to publish

* Topic Publish (SDK)
    * Create a topic
    * Create one or more subscription
    * Publish to the topic
* Direct Publish (mobile apps SDK)
    * Create a platform app
    * Create a platform endpoint
    * Publish to the platform endpoint
    * Google GCM, Apple APNS, Amazon ADM, …

## Security

* HTTPS
* KMS
* Client-side encryption
* IAM access policies (to secure SNS API)
* SNS Access Policy
    * cross-account access to SNS topics
    * For other sercices to write to an SNS topic

## SNS + SQS: Fan Out

* SQS is a subscriber of SNS
* Usually with multiple SQS
* SQS has persitence, delayed processing, and retries
* SQS need access policy
* For example
    * S3 event -> SNS Topic -> 2xSQS Queues and 1 Lambda function

## SNS FIFO Topic

* Like SQS, messages are ordered.
* Only SQS FIFO can be the only subscriber at the moment.
* Throughput is limited by SQS FIFO throughput.

## SNS Message filtering

* JSON policy that a subscriber like SQS can apply to receive only some messages.
* Example SQS queue that wants to receive only "placed orders" and one for "cancelled orders", etc



