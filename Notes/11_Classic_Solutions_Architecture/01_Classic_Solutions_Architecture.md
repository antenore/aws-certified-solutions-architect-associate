# Solution Architecture Discussion

## Introduction

* How all technologies fit together.
* Sample cases:
    * WhatIsTheTime.com
    * MyClothes.com
    * MyWordPress.com
    * Instantiating applications quickly
    * Beanstalk.

## Stateless Web App: WhatIsTheTime.com

* Allows people to know what time it is.
* DB less.
* Start small ands accept downtime.
* Fully scale vertically and horizontally, no downtime.

To understand must see lecture 119.

* Public vs Private IP and EC2 instances.
    * User "What's the time?" -> Elastic IP -> Micro T2 (Public EC2)
    * We get more traffic, we scale up to M5
    * Downtime for upgrade, even if we have Elastic IP.
    * More users -> We scale horizontally.
* Elastic IP vs Route 53 vs Load Balancers.
    * As we get more and more users, we put in place Route 53 with an A record, 1H TTL.
    * No need of elastic IP anymore.
    * At this point if we scale down or up, the DNS will take 1 hour to update.
    * We add an ELB with Health Checks and Security groups.
* Route 53 TTL, A records and Alias Records.
    * An ELB doesn't have Elastic IP (IP changes overtime), so we need an Alias record on Route 53
    * No downtime when adding or removing instances
* Manually managed EC2 instances vs Auto Scaling Groups.
    * Adding and removing EC2 instances by hand is time consuming.
    * We implement Auto Scaling Groups
* Multi AZ to survive disasters.
    * We have everything in one Zone, what if Godzilla comes and shits on our DC?
    * For high availability AWS recommend Multi-AZ.
* ELB Health Checks.
* Security Group Rules.
* Capacity reservation to cut costs.

It's important to understand the requirements and choose the best solution that fit the requirements.

* The 5 pillars for a well architected application:
    * Costs
    * Performance
    * Reliability
    * Security
    * Operational excellence.

## Stateful Web App: MyClothes.com

* Allows people to buy clothes online.
* Shopping cart.
* Hundreds of concurrent users.
* Horizontal scalability, as much stateless as possible.
* Must not loose sopping cart.
* User' details stored in a DB.


* ELB Stickiness.
    * A user is lied to an instance, so he/she won't loose the shopping cart.
    * If we loose an EC2 instance, we loose the cart.
* Using cookie (store data in the cookie)
    * We set as much info as possible in the cookie
    * It's stateless, but…
    * The cart content is stored in the cookie (max 4B).
    * HTTP requests are heavier.
    * Security risk, cookie can be altered.
    * Cookies must be validated.
    * Cookies must be less than 4KB.
* Using cookie (ElsticCache)
    * We send only the session ID in the cookie.
    * The app on EC2 will store and retrieve sessions from ElastiCache.
    * Alternatively we can use DynamoDB to store data, like ElastiCache.
    * Much more secure.

* To store user info, like addresses, phone numbers, emails, etc, we can use Amazon RDS
    * To scale we can use Master + Read Replicas
    * Or Write Through
        * We can again use ElatiCache with RDS it improve reads.

* High Availability
    * ELB can be Multi AZ
    * Auto Scaling Groups can be Multi AZ
    * ElastiCache can be Multi AZ
    * RDS can be Multi AZ

* Tight security
    * Security group between the user and the ELB
    * Security group between ElastiCache, RDS referencing the ELB security group.

## Stateful Web App: MyWordPress.com

* We want a typical WordPress web site where we can store images.
* A common nice solution is to use Aurora DB to have easy Multi-AZ and Read-Replicas.
* If we store images on EBS we can only use one EC2 instance, because EBS cannot be shared.
* Better using EFS (Elastic File System)
    * More expansive
    * Each EC2 instance need an ENI to access the shared EFS. (TODO: review EFS, EBS, ENI)
    * The above can be accomplished with Elastic Beanstalk easily.
* What about S3?

## Quickly instantiate Applications

* EC2:
    * Using Golden AMI: We can create a Golden AMI so that it has everything we need to boostrap our instance.
    * User Data: Can be used to finish the EC2 instance set-up. Things that cannot be the same on all instances.
* RDS Databases:
    * We can restore from a snapshot taken after the DB creation, that maybe includes the initial DB import.
* EBS Volumes:
    * We can restore form a snapshot to have all the data in one shot.

## Elastic Beanstalk

* Devs problems on AWS
    * Managing infra
    * Deploying code
    * Configuring DB, ELB, etc
    * Scaling
* Most apps have the same architecture (ALB + ASG)
* Devs just want to deploy and run their code
* Possibly, consistently across different applications and environments.

* Elastic Beanstalk is a developer centric view of deploying an application on AWS.
* It automatically uses EC2, ASG, ELB, RDS, etc.
* Managed service
    * Automatically handles capacity provisioning, load balancing, scaling, health check monitoring, EC2 config, etc.
    * Just the application code is the developer responsibility.
* We still have full control over the configuration
* Beanstalk is free, you pay for the underlying instances.

### Components

* Application: collection of Elastic Beanstalk components (ens, versions, config, …)
* Application version: an iteration of your application code.
* Environment:
    * Collection of AWS resources running and application version (one version at a time).
    * Tiers: Web Server Environment Tier and Worker Environment Tier
    * Dev, test, prod, etc

### Supported platform

* By default:
    * Go, Java with Tomcat, .NET Core on Linux, .NET on Windows Server, Node.js, PHP, Python, Ruby
    * Packer Builder, single container Docker, Multi-container Docker, Pre-configured Docker
* If not in the default set, you can write your own custom platform (advanced).

### Web Server Tier vs. Worker Tier

* Web Server is the classical WebApp we know, where the client access the web servers.
* In a Worker Tier the clients don't access the application.
* We usually have an SQS Queue that pull messages from EC2 instances and
* the scaling is based on the number of messages messages,
* Can be coupled with a Web Tier that push messages to SQS

