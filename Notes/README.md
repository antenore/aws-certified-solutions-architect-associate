## [01_elastic_ips](05_EC2_Fundamentals/01_Elastic_IPs.md)

- [Elastic IPs](05_EC2_Fundamentals/01_Elastic_IPs.md#elastic-ips)

## [02_placement_groups](05_EC2_Fundamentals/02_Placement_Groups.md)

- [Placement Groups](05_EC2_Fundamentals/02_Placement_Groups.md#placement-groups)
  * [Cluster placement group](05_EC2_Fundamentals/02_Placement_Groups.md#cluster-placement-group)
  * [Partition placement group](05_EC2_Fundamentals/02_Placement_Groups.md#partition-placement-group)
  * [Spread placement group](05_EC2_Fundamentals/02_Placement_Groups.md#spread-placement-group)

## [03_elastic_network_interfaces](05_EC2_Fundamentals/03_Elastic_Network_Interfaces.md)

- [Elastic Network Interfaces (ENI).](05_EC2_Fundamentals/03_Elastic_Network_Interfaces.md#elastic-network-interfaces-eni)
  * [ENI use cases:](05_EC2_Fundamentals/03_Elastic_Network_Interfaces.md#eni-use-cases)

## [04_ec2_hibernate](05_EC2_Fundamentals/04_EC2_Hibernate.md)

- [EC2 Hibernate](05_EC2_Fundamentals/04_EC2_Hibernate.md#ec2-hibernate)

## [05_ec2_understanding_vcpu](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md)

- [EC2 — Understanding vCPU](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md#ec2-—-understanding-vcpu)
  * [Example: m5.2xlarge](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md#example-m52xlarge)
  * [EC2 — Optimizing CPU options](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md#ec2-—-optimizing-cpu-options)

## [06_ec2_nitro](05_EC2_Fundamentals/06_EC2_Nitro.md)

- [EC2 Nitro](05_EC2_Fundamentals/06_EC2_Nitro.md#ec2-nitro)

## [07_ec2_capacity_reservation](05_EC2_Fundamentals/07_EC2_Capacity_Reservation.md)

- [EC2 — Capacity Reservation](05_EC2_Fundamentals/07_EC2_Capacity_Reservation.md#ec2-—-capacity-reservation)

## [08_ec2_elastic_block_store](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md)

- [EBS – Elastic Block Store](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-–-elastic-block-store)
  * [EBS Snapshots](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-snapshots)
  * [EBS Snapshots Archive.](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-snapshots-archive)
  * [EBS Snapshots Recycle Bin.](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-snapshots-recycle-bin)
  * [EBS Volume types](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-volume-types)
    + [General Purpose SSD](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#general-purpose-ssd)
  * [EBS Volume Types Use cases](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-volume-types-use-cases)
    + [Provisioned IOPS (PIOPS) SSD](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#provisioned-iops-piops-ssd)
    + [HDD Based](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#hdd-based)
  * [EBS Multi-Attach – io1/io2 family](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-multi-attach-–-io1io2-family)
  * [EBS Encryption](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-encryption)

## [09_ec2_amazon_machine_image](05_EC2_Fundamentals/09_EC2_Amazon_Machine_Image.md)

- [EC2 — Amazon Machine Image (AMI)](05_EC2_Fundamentals/09_EC2_Amazon_Machine_Image.md#ec2-—-amazon-machine-image-ami)

## [10_ec2_instance_store](05_EC2_Fundamentals/10_EC2_Instance_Store.md)

- [EC2 - Instance Store](05_EC2_Fundamentals/10_EC2_Instance_Store.md#ec2---instance-store)

## [11_ec2_elastic_file_system](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md)

- [EC2 - Elastic File System](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#ec2---elastic-file-system)
  * [EFS Performace and Storage Classes.](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#efs-performace-and-storage-classes)
    + [EFS Scale](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#efs-scale)
    + [Perfomance mode (at creation time)](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#perfomance-mode-at-creation-time)
    + [Throughput mode](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#throughput-mode)
    + [EFS - Storage Classes](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#efs---storage-classes)
      - [Storage Tiers (lifecycle m. - move file after N days)](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#storage-tiers-lifecycle-m---move-file-after-n-days)
      - [Availability and durability](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#availability-and-durability)

## [12_ec2_efs_vs_ebs](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md)

- [EBS vs EFS](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md#ebs-vs-efs)
  * [Elastic Block Store](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md#elastic-block-store)
  * [Elastic File System](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md#elastic-file-system)### [01_elastic_ips](05_EC2_Fundamentals/01_Elastic_IPs.md)

- [Elastic IPs](05_EC2_Fundamentals/01_Elastic_IPs.md#elastic-ips)

## [02_placement_groups](05_EC2_Fundamentals/02_Placement_Groups.md)

- [Placement Groups](05_EC2_Fundamentals/02_Placement_Groups.md#placement-groups)
  * [Cluster placement group](05_EC2_Fundamentals/02_Placement_Groups.md#cluster-placement-group)
  * [Partition placement group](05_EC2_Fundamentals/02_Placement_Groups.md#partition-placement-group)
  * [Spread placement group](05_EC2_Fundamentals/02_Placement_Groups.md#spread-placement-group)

## [03_elastic_network_interfaces](05_EC2_Fundamentals/03_Elastic_Network_Interfaces.md)

- [Elastic Network Interfaces (ENI).](05_EC2_Fundamentals/03_Elastic_Network_Interfaces.md#elastic-network-interfaces-eni)
  * [ENI use cases:](05_EC2_Fundamentals/03_Elastic_Network_Interfaces.md#eni-use-cases)

## [04_ec2_hibernate](05_EC2_Fundamentals/04_EC2_Hibernate.md)

- [EC2 Hibernate](05_EC2_Fundamentals/04_EC2_Hibernate.md#ec2-hibernate)

## [05_ec2_understanding_vcpu](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md)

- [EC2 — Understanding vCPU](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md#ec2-—-understanding-vcpu)
  * [Example: m5.2xlarge](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md#example-m52xlarge)
  * [EC2 — Optimizing CPU options](05_EC2_Fundamentals/05_EC2_Understanding_vCPU.md#ec2-—-optimizing-cpu-options)

## [06_ec2_nitro](05_EC2_Fundamentals/06_EC2_Nitro.md)

- [EC2 Nitro](05_EC2_Fundamentals/06_EC2_Nitro.md#ec2-nitro)

## [07_ec2_capacity_reservation](05_EC2_Fundamentals/07_EC2_Capacity_Reservation.md)

- [EC2 — Capacity Reservation](05_EC2_Fundamentals/07_EC2_Capacity_Reservation.md#ec2-—-capacity-reservation)

## [08_ec2_elastic_block_store](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md)

- [EBS – Elastic Block Store](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-–-elastic-block-store)
  * [EBS Snapshots](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-snapshots)
  * [EBS Snapshots Archive.](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-snapshots-archive)
  * [EBS Snapshots Recycle Bin.](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-snapshots-recycle-bin)
  * [EBS Volume types](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-volume-types)
    + [General Purpose SSD](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#general-purpose-ssd)
  * [EBS Volume Types Use cases](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-volume-types-use-cases)
    + [Provisioned IOPS (PIOPS) SSD](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#provisioned-iops-piops-ssd)
    + [HDD Based](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#hdd-based)
  * [EBS Multi-Attach – io1/io2 family](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-multi-attach-–-io1io2-family)
  * [EBS Encryption](05_EC2_Fundamentals/08_EC2_Elastic_Block_Store.md#ebs-encryption)

## [09_ec2_amazon_machine_image](05_EC2_Fundamentals/09_EC2_Amazon_Machine_Image.md)

- [EC2 — Amazon Machine Image (AMI)](05_EC2_Fundamentals/09_EC2_Amazon_Machine_Image.md#ec2-—-amazon-machine-image-ami)

## [10_ec2_instance_store](05_EC2_Fundamentals/10_EC2_Instance_Store.md)

- [EC2 - Instance Store](05_EC2_Fundamentals/10_EC2_Instance_Store.md#ec2---instance-store)

## [11_ec2_elastic_file_system](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md)

- [EC2 - Elastic File System](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#ec2---elastic-file-system)
  * [EFS Performace and Storage Classes.](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#efs-performace-and-storage-classes)
    + [EFS Scale](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#efs-scale)
    + [Perfomance mode (at creation time)](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#perfomance-mode-at-creation-time)
    + [Throughput mode](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#throughput-mode)
    + [EFS - Storage Classes](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#efs---storage-classes)
      - [Storage Tiers (lifecycle m. - move file after N days)](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#storage-tiers-lifecycle-m---move-file-after-n-days)
      - [Availability and durability](05_EC2_Fundamentals/11_EC2_Elastic_File_System.md#availability-and-durability)

## [12_ec2_efs_vs_ebs](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md)

- [EBS vs EFS](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md#ebs-vs-efs)
  * [Elastic Block Store](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md#elastic-block-store)
  * [Elastic File System](05_EC2_Fundamentals/12_EC2_EFS_vs_EBS.md#elastic-file-system)### [elastic_load_balancing](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md)

- [Elastic Load Balancing](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#elastic-load-balancing)
  * [Health Checks](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#health-checks)
  * [Types of AWS Load Balancer](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#types-of-aws-load-balancer)
    + [Classic Load Balancer (v1 - old generation)](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#classic-load-balancer-v1---old-generation)
    + [Application Load Balancer (v2 - new generation)](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#application-load-balancer-v2---new-generation)
      - [ALB - Target Groups](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#alb---target-groups)
    + [Network Load Balancer (v2 - new generation)](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#network-load-balancer-v2---new-generation)
    + [Gateway Load Balancer](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#gateway-load-balancer)
  * [Load Balancer Security Groups](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#load-balancer-security-groups)
  * [Sticky Sessions (Session Affinity)](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#sticky-sessions-session-affinity)
    + [Cookie names](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#cookie-names)
  * [Cross-Zone Load Balancing](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#cross-zone-load-balancing)
  * [SSL/TLS Certificates](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#ssltls-certificates)
    + [Server Name Indication (SNI)](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#server-name-indication-sni)
    + [ELB - SSL certificates support](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#elb---ssl-certificates-support)
    + [Connection Draining](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#connection-draining)
  * [Auto Scaling Group (ASG)](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#auto-scaling-group-asg)
  * [Special notes for Solution Architects](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#special-notes-for-solution-architects)
    + [ASG Default Termination Policy](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#asg-default-termination-policy)
    + [ASG Lifecycle Hooks](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#asg-lifecycle-hooks)
    + [ASG Launch Template vs Launch Configuration](08_High_Availability_And_Scalability/Elastic_Load_Balancing.md#asg-launch-template-vs-launch-configuration)

## [scalability_and_high_availability](08_High_Availability_And_Scalability/Scalability_And_High_Availability.md)

- [Scalability And High Availability](08_High_Availability_And_Scalability/Scalability_And_High_Availability.md#scalability-and-high-availability)
  * [Vertical Scalability](08_High_Availability_And_Scalability/Scalability_And_High_Availability.md#vertical-scalability)
  * [Horizontal Scalability](08_High_Availability_And_Scalability/Scalability_And_High_Availability.md#horizontal-scalability)
  * [High Availability](08_High_Availability_And_Scalability/Scalability_And_High_Availability.md#high-availability)### [01_amazon_rds](09_AWS_Fundamentals/01_Amazon_RDS.md)

- [RDS Overview](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-overview)
  * [Introduction](09_AWS_Fundamentals/01_Amazon_RDS.md#introduction)
  * [RDS vs own DBs on EC2](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-vs-own-dbs-on-ec2)
  * [RDS Backups](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-backups)
  * [RDS Storage Auto Scaling (exam)](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-storage-auto-scaling-exam)
  * [RDS Read Replicas (vs Multi AZ)](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-read-replicas-vs-multi-az)
    + [Read Replicas use cases](09_AWS_Fundamentals/01_Amazon_RDS.md#read-replicas-use-cases)
    + [Read Replicas Network costs](09_AWS_Fundamentals/01_Amazon_RDS.md#read-replicas-network-costs)
  * [RDS Multi AZ (Disaster Recovery)](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-multi-az-disaster-recovery)
  * [RDS Security - Encryption](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-security---encryption)
    + [RDS Security - Encryption Operations](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-security---encryption-operations)
    + [RDS Security - Network and IAM](09_AWS_Fundamentals/01_Amazon_RDS.md#rds-security---network-and-iam)

## [02_amazon_aurora](09_AWS_Fundamentals/02_Amazon_Aurora.md)

- [Amazon Aurora](09_AWS_Fundamentals/02_Amazon_Aurora.md#amazon-aurora)
  * [Introduction](09_AWS_Fundamentals/02_Amazon_Aurora.md#introduction)
  * [Aurora High Availability and Read Scaling](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora-high-availability-and-read-scaling)
  * [Aurora DB Cluster](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora-db-cluster)
  * [Aurora Security.](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora-security)
  * [Hands-On notes](09_AWS_Fundamentals/02_Amazon_Aurora.md#hands-on-notes)
  * [Aurora Replicas - Auto Scaling](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora-replicas---auto-scaling)
  * [Aurora - Custom Endpoints](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora---custom-endpoints)
  * [Aurora Serverless](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora-serverless)
- [Aurora Multi-Master](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora-multi-master)
- [Global Aurora](09_AWS_Fundamentals/02_Amazon_Aurora.md#global-aurora)
  * [Aurora Machine Learning](09_AWS_Fundamentals/02_Amazon_Aurora.md#aurora-machine-learning)

## [03_amazon_elastic_cache](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md)

- [Amazon ElastiCache](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#amazon-elasticache)
  * [ElastiCache - Solution Architectures](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#elasticache---solution-architectures)
  * [DB Cache.](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#db-cache)
  * [User Session Store (stateless application example)](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#user-session-store-stateless-application-example)
  * [Redis vs Memcached](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#redis-vs-memcached)
  * [Solution Architect special notes](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#solution-architect-special-notes)
    + [ElastiCache Security](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#elasticache-security)
    + [ElastiCache Patterns](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#elasticache-patterns)
    + [ElastiCache Redis Use Case](09_AWS_Fundamentals/03_Amazon_Elastic_Cache.md#elasticache-redis-use-case)### [01_amazon_route_53](10_Route_53/01_Amazon_Route_53.md)

- [Amazon Route 53](10_Route_53/01_Amazon_Route_53.md#amazon-route-53)
  * [Records](10_Route_53/01_Amazon_Route_53.md#records)
  * [Record Types](10_Route_53/01_Amazon_Route_53.md#record-types)
  * [Hosted Zones](10_Route_53/01_Amazon_Route_53.md#hosted-zones)
  * [Registering a domain (Hands On)](10_Route_53/01_Amazon_Route_53.md#registering-a-domain-hands-on)
    + [DNS, ELB, EC2 (Hands On)](10_Route_53/01_Amazon_Route_53.md#dns-elb-ec2-hands-on)
  * [TTL](10_Route_53/01_Amazon_Route_53.md#ttl)
  * [CNAME vs ALIAS](10_Route_53/01_Amazon_Route_53.md#cname-vs-alias)
    + [Alias Records.](10_Route_53/01_Amazon_Route_53.md#alias-records)
  * [Routing Policies](10_Route_53/01_Amazon_Route_53.md#routing-policies)
    + [Simple](10_Route_53/01_Amazon_Route_53.md#simple)
  * [Weighted](10_Route_53/01_Amazon_Route_53.md#weighted)
  * [Latency based](10_Route_53/01_Amazon_Route_53.md#latency-based)
  * [Health Checks](10_Route_53/01_Amazon_Route_53.md#health-checks)
    + [Monitor Endpoint](10_Route_53/01_Amazon_Route_53.md#monitor-endpoint)
    + [Calculated Health Checks](10_Route_53/01_Amazon_Route_53.md#calculated-health-checks)
    + [Private Hosted Zones](10_Route_53/01_Amazon_Route_53.md#private-hosted-zones)
  * [Routing policies](10_Route_53/01_Amazon_Route_53.md#routing-policies)
    + [Failover, Active/Passive](10_Route_53/01_Amazon_Route_53.md#failover-activepassive)
    + [Geolocation](10_Route_53/01_Amazon_Route_53.md#geolocation)
    + [Geoproximity Routing Policy](10_Route_53/01_Amazon_Route_53.md#geoproximity-routing-policy)
    + [Traffic flow](10_Route_53/01_Amazon_Route_53.md#traffic-flow)
    + [Multi-Value Routing Policies](10_Route_53/01_Amazon_Route_53.md#multi-value-routing-policies)
    + [Using a 3rd party registrar and Route 53 as DNS](10_Route_53/01_Amazon_Route_53.md#using-a-3rd-party-registrar-and-route-53-as-dns)### [01_classic_solutions_architecture](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md)

- [Solution Architecture Discussion](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#solution-architecture-discussion)
  * [Introduction](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#introduction)
  * [Stateless Web App: WhatIsTheTime.com](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#stateless-web-app-whatisthetimecom)
  * [Stateful Web App: MyClothes.com](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#stateful-web-app-myclothescom)
  * [Stateful Web App: MyWordPress.com](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#stateful-web-app-mywordpresscom)
  * [Quickly instantiate Applications](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#quickly-instantiate-applications)
  * [Elastic Beanstalk](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#elastic-beanstalk)
    + [Components](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#components)
    + [Supported platform](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#supported-platform)
    + [Web Server Tier vs. Worker Tier](11_Classic_Solutions_Architecture/01_Classic_Solutions_Architecture.md#web-server-tier-vs-worker-tier)### [01_amazon_s3](12_Amazon_S3/01_Amazon_S3.md)

- [Amazon S3](12_Amazon_S3/01_Amazon_S3.md#amazon-s3)
  * [Objects and Buckets](12_Amazon_S3/01_Amazon_S3.md#objects-and-buckets)
  * [Versioning](12_Amazon_S3/01_Amazon_S3.md#versioning)
  * [Encryption](12_Amazon_S3/01_Amazon_S3.md#encryption)
    + [SSE-S3](12_Amazon_S3/01_Amazon_S3.md#sse-s3)
    + [SSE-KMS](12_Amazon_S3/01_Amazon_S3.md#sse-kms)
    + [SSE-C](12_Amazon_S3/01_Amazon_S3.md#sse-c)
    + [Client Side Encryption](12_Amazon_S3/01_Amazon_S3.md#client-side-encryption)
    + [Encryption in transit (SSL/TLS)](12_Amazon_S3/01_Amazon_S3.md#encryption-in-transit-ssltls)
  * [Bucket Policies](12_Amazon_S3/01_Amazon_S3.md#bucket-policies)
  * [Bucket settings for Block Public Access](12_Amazon_S3/01_Amazon_S3.md#bucket-settings-for-block-public-access)
  * [Other security options](12_Amazon_S3/01_Amazon_S3.md#other-security-options)
  * [S3 Websites](12_Amazon_S3/01_Amazon_S3.md#s3-websites)
  * [S3 CORS - Cross Origin Resource Sharing](12_Amazon_S3/01_Amazon_S3.md#s3-cors---cross-origin-resource-sharing)### [01_aws-sdk_iam-roles_policies](13_AWS-SDK_IAM-Roles_Policies/01_AWS-SDK_IAM-Roles_Policies.md)

- [AWS IAM Roles, Policies, SDK, CLI, etc](13_AWS-SDK_IAM-Roles_Policies/01_AWS-SDK_IAM-Roles_Policies.md#aws-iam-roles-policies-sdk-cli-etc)
  * [AWS EC2 Instance Metadata](13_AWS-SDK_IAM-Roles_Policies/01_AWS-SDK_IAM-Roles_Policies.md#aws-ec2-instance-metadata)
  * [AWS SDK Overview.](13_AWS-SDK_IAM-Roles_Policies/01_AWS-SDK_IAM-Roles_Policies.md#aws-sdk-overview)### [01_advanced_s3_and_athena](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md)

- [Advanced Amazon S3 and Athena](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#advanced-amazon-s3-and-athena)
  * [MFA-delete](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#mfa-delete)
  * [Default Encryption](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#default-encryption)
  * [S3 Access Logs](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#s3-access-logs)
  * [S3 Replication (CRR, SRR)](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#s3-replication-crr-srr)
  * [S3 pre-signed URLs](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#s3-pre-signed-urls)
  * [S3 Stoage Classes](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#s3-stoage-classes)
  * [S3 Events](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#s3-events)
  * [Amazon EventBridge](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#amazon-eventbridge)
  * [S3 - Request Pays](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#s3---request-pays)
  * [Amazon Athena](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#amazon-athena)
  * [Glacier Vault Lock](14_Advanced_S3_and_Athena/01_Advanced_S3_and_Athena.md#glacier-vault-lock)### [01_cloudfront_and_aws-global_accelerator](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md)

- [AWS CloudFront & AWS Global Accelerator](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#aws-cloudfront--aws-global-accelerator)
  * [AWS CloudFront](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#aws-cloudfront)
    + [CloudFront vs S3 Cross Region Replication](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#cloudfront-vs-s3-cross-region-replication)
  * [CloudFront Signed URL / Signed Cookies](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#cloudfront-signed-url--signed-cookies)
    + [Pricing of CloudFront](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#pricing-of-cloudfront)
    + [Multiple Origin](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#multiple-origin)
    + [Origin Groups](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#origin-groups)
    + [CloudFron FieldLevel Encryption](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#cloudfron-fieldlevel-encryption)
  * [AWS Global Accelerator](15_CloudFront_And_AWS-Global_Accelerator/01_CloudFront_And_AWS-Global_Accelerator.md#aws-global-accelerator)### [01_aws_storage_extras](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md)

- [AWS Storage Extras](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#aws-storage-extras)
  * [AWS Snow Family](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#aws-snow-family)
    + [Data migration](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#data-migration)
    + [Edge Computing](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#edge-computing)
    + [AWS OpsHub](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#aws-opshub)
    + [Snowball to Glacier](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#snowball-to-glacier)
  * [Amazon FSx](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#amazon-fsx)
    + [FSx for Windows File Server](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#fsx-for-windows-file-server)
    + [FSx for Lustre](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#fsx-for-lustre)
    + [Storage Gateway](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#storage-gateway)
  * [AWS Transfer Family](16_AWS_Storage_Extras/01_AWS_Storage_Extras.md#aws-transfer-family)### [00_decoupling_sqs_sns_etc](17_Decoupling_SQS_SNS_etc/00_Decoupling_SQS_SNS_etc.md)

- [Decoupling applications with SQS, SNS, Kinesis, MQ, etc.](17_Decoupling_SQS_SNS_etc/00_Decoupling_SQS_SNS_etc.md#decoupling-applications-with-sqs-sns-kinesis-mq-etc)
  * [Messaging and Integration](17_Decoupling_SQS_SNS_etc/00_Decoupling_SQS_SNS_etc.md#messaging-and-integration)

## [01_decoupling_with_sqs](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md)

- [Amazon SQS](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#amazon-sqs)
  * [SQS - Producing messages](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs---producing-messages)
  * [SQS - Consuming messages](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs---consuming-messages)
  * [SQS - Access Policy](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs---access-policy)
  * [SQS Message Timeout Visibility](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs-message-timeout-visibility)
  * [SQS - Deall Letter Queue](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs---deall-letter-queue)
  * [SQS - Delay Queues](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs---delay-queues)
  * [SQS Long Polling](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs-long-polling)
  * [SQS Request-Response Systems](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs-request-response-systems)
  * [SQS FIFO Queue](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs-fifo-queue)
- [SQS with Auto Scaling Group (ASG)](17_Decoupling_SQS_SNS_etc/01_Decoupling_with_SQS.md#sqs-with-auto-scaling-group-asg)

## [02_decoupling_with_sns](17_Decoupling_SQS_SNS_etc/02_Decoupling_with_SNS.md)

- [Amazon SNS](17_Decoupling_SQS_SNS_etc/02_Decoupling_with_SNS.md#amazon-sns)
  * [How to publish](17_Decoupling_SQS_SNS_etc/02_Decoupling_with_SNS.md#how-to-publish)
  * [Security](17_Decoupling_SQS_SNS_etc/02_Decoupling_with_SNS.md#security)
  * [SNS + SQS: Fan Out](17_Decoupling_SQS_SNS_etc/02_Decoupling_with_SNS.md#sns--sqs-fan-out)
  * [SNS FIFO Topic](17_Decoupling_SQS_SNS_etc/02_Decoupling_with_SNS.md#sns-fifo-topic)
  * [SNS Message filtering](17_Decoupling_SQS_SNS_etc/02_Decoupling_with_SNS.md#sns-message-filtering)

## [03_decoupling_with_kinesis](17_Decoupling_SQS_SNS_etc/03_Decoupling_with_Kinesis.md)

- [Amazon Kinesis](17_Decoupling_SQS_SNS_etc/03_Decoupling_with_Kinesis.md#amazon-kinesis)
  * [K. Data Streams](17_Decoupling_SQS_SNS_etc/03_Decoupling_with_Kinesis.md#k-data-streams)
    + [Pricing model](17_Decoupling_SQS_SNS_etc/03_Decoupling_with_Kinesis.md#pricing-model)
  * [K. Data Firehose](17_Decoupling_SQS_SNS_etc/03_Decoupling_with_Kinesis.md#k-data-firehose)
    + [K. Data Streams vs K. Data Firehose](17_Decoupling_SQS_SNS_etc/03_Decoupling_with_Kinesis.md#k-data-streams-vs-k-data-firehose)
  * [K. Data Nalytics (SQL appl)](17_Decoupling_SQS_SNS_etc/03_Decoupling_with_Kinesis.md#k-data-nalytics-sql-appl)

## [04_decoupling_with_mq](17_Decoupling_SQS_SNS_etc/04_Decoupling_with_MQ.md)

- [Amazon MQ](17_Decoupling_SQS_SNS_etc/04_Decoupling_with_MQ.md#amazon-mq)### [00_containers_on_aws](18_Containers_on_AWS/00_Containers_on_AWS.md)

- [Docker introduction](18_Containers_on_AWS/00_Containers_on_AWS.md#docker-introduction)

## [01_amazon_ecs](18_Containers_on_AWS/01_Amazon_ECS.md)

- [Amazon ECS](18_Containers_on_AWS/01_Amazon_ECS.md#amazon-ecs)
  * [EC2 Launch Type](18_Containers_on_AWS/01_Amazon_ECS.md#ec2-launch-type)
  * [Fargate Launch Type](18_Containers_on_AWS/01_Amazon_ECS.md#fargate-launch-type)
  * [ECS IAM roles](18_Containers_on_AWS/01_Amazon_ECS.md#ecs-iam-roles)
  * [ECS Load Balancer integrations](18_Containers_on_AWS/01_Amazon_ECS.md#ecs-load-balancer-integrations)
  * [Data Volumes (EFS)](18_Containers_on_AWS/01_Amazon_ECS.md#data-volumes-efs)
  * [ECS Auto Scaling Service](18_Containers_on_AWS/01_Amazon_ECS.md#ecs-auto-scaling-service)
  * [Auto Scaling EC2 Instances](18_Containers_on_AWS/01_Amazon_ECS.md#auto-scaling-ec2-instances)
  * [ECS Rolling Updates](18_Containers_on_AWS/01_Amazon_ECS.md#ecs-rolling-updates)
  * [ECS tasks launched by Event Bridge](18_Containers_on_AWS/01_Amazon_ECS.md#ecs-tasks-launched-by-event-bridge)

## [02_amazon_ecr](18_Containers_on_AWS/02_Amazon_ECR.md)

- [Amazon Elastic Container Registry (ECR)](18_Containers_on_AWS/02_Amazon_ECR.md#amazon-elastic-container-registry-ecr)

## [03_amazon_eks](18_Containers_on_AWS/03_Amazon_EKS.md)

- [Amazon Elastic Kubernetes Service (EKS)](18_Containers_on_AWS/03_Amazon_EKS.md#amazon-elastic-kubernetes-service-eks)### [00_serverless](19_Serverless/00_Serverless.md)

- [What is serverless](19_Serverless/00_Serverless.md#what-is-serverless)
  * [Serverless example](19_Serverless/00_Serverless.md#serverless-example)

## [01_aws_lambda](19_Serverless/01_AWS_Lambda.md)

- [AWS Lambda](19_Serverless/01_AWS_Lambda.md#aws-lambda)
  * [Benefits](19_Serverless/01_AWS_Lambda.md#benefits)
  * [Security](19_Serverless/01_AWS_Lambda.md#security)
  * [Lambda language support](19_Serverless/01_AWS_Lambda.md#lambda-language-support)
  * [Main Integrations](19_Serverless/01_AWS_Lambda.md#main-integrations)
  * [Some examples](19_Serverless/01_AWS_Lambda.md#some-examples)
  * [Lambda limits summerize](19_Serverless/01_AWS_Lambda.md#lambda-limits-summerize)
- [Lamba@Edge](19_Serverless/01_AWS_Lambda.md#lambaedge)

## [02_amazon_dynamodb](19_Serverless/02_Amazon_DynamoDB.md)

- [Amazon DynamoDB](19_Serverless/02_Amazon_DynamoDB.md#amazon-dynamodb)
  * [Basics](19_Serverless/02_Amazon_DynamoDB.md#basics)
  * [Example](19_Serverless/02_Amazon_DynamoDB.md#example)
  * [Read/write Capacity Modes](19_Serverless/02_Amazon_DynamoDB.md#readwrite-capacity-modes)
  * [DynamoDB Accelerator (DAX)](19_Serverless/02_Amazon_DynamoDB.md#dynamodb-accelerator-dax)
  * [DynamoDB Streams](19_Serverless/02_Amazon_DynamoDB.md#dynamodb-streams)
  * [DynamoDB Global Tables](19_Serverless/02_Amazon_DynamoDB.md#dynamodb-global-tables)
  * [DynamoDB Time To Live (TTL)](19_Serverless/02_Amazon_DynamoDB.md#dynamodb-time-to-live-ttl)
  * [DynamoDB indexes](19_Serverless/02_Amazon_DynamoDB.md#dynamodb-indexes)
  * [DynamoDB Transactions](19_Serverless/02_Amazon_DynamoDB.md#dynamodb-transactions)

## [03_amazon_api_gateway](19_Serverless/03_Amazon_API_Gateway.md)

- [API GAteway](19_Serverless/03_Amazon_API_Gateway.md#api-gateway)
  * [Integrations:](19_Serverless/03_Amazon_API_Gateway.md#integrations)
  * [Endpoint Types](19_Serverless/03_Amazon_API_Gateway.md#endpoint-types)
- [API Gateway - Security](19_Serverless/03_Amazon_API_Gateway.md#api-gateway---security)

## [04_aws_cognito](19_Serverless/04_AWS_Cognito.md)

- [AWS Cognito](19_Serverless/04_AWS_Cognito.md#aws-cognito)

## [05_aws_serverless_application_model](19_Serverless/05_AWS_Serverless_Application_Model.md)

- [AWS SAM - Serverless Application Model](19_Serverless/05_AWS_Serverless_Application_Model.md#aws-sam---serverless-application-model)### [00_choosing_the_right_db](21_AWS_Databases/00_Choosing_The_Right_DB.md)

- [Choosing the right DB](21_AWS_Databases/00_Choosing_The_Right_DB.md#choosing-the-right-db)
  * [Database Types](21_AWS_Databases/00_Choosing_The_Right_DB.md#database-types)

## [01_rds](21_AWS_Databases/01_RDS.md)

- [RDS - Solution Architect Perspective](21_AWS_Databases/01_RDS.md#rds---solution-architect-perspective)

## [02_auroramd](21_AWS_Databases/02_Aurora.md.md)

- [Aurora - Solution Architect Perspective](21_AWS_Databases/02_Aurora.md.md#aurora---solution-architect-perspective)

## [03_elasticache](21_AWS_Databases/03_ElastiCache.md)

- [ElastiCache ̣— Solution Architect Perspective](21_AWS_Databases/03_ElastiCache.md#elasticache-̣—-solution-architect-perspective)

## [04_dynamodb](21_AWS_Databases/04_DynamoDB.md)

- [DynamoDB — Solution Architect Perspective](21_AWS_Databases/04_DynamoDB.md#dynamodb-—-solution-architect-perspective)

## [05_s3](21_AWS_Databases/05_S3.md)



## [06_athena](21_AWS_Databases/06_Athena.md)

- [Athena — Solution Architect Perspective](21_AWS_Databases/06_Athena.md#athena-—-solution-architect-perspective)

## [07_redshift](21_AWS_Databases/07_Redshift.md)

- [Redshift — Solution Architect Perspective](21_AWS_Databases/07_Redshift.md#redshift-—-solution-architect-perspective)

## [08_aws_glue](21_AWS_Databases/08_AWS_Glue.md)

- [AWS Glue — Solution Architect Perspective](21_AWS_Databases/08_AWS_Glue.md#aws-glue-—-solution-architect-perspective)

## [09_amazon_neptune](21_AWS_Databases/09_Amazon_Neptune.md)

- [Amazon Neptune — Solution Architect Perspective](21_AWS_Databases/09_Amazon_Neptune.md#amazon-neptune-—-solution-architect-perspective)

## [10_amazon_opensearch](21_AWS_Databases/10_Amazon_OpenSearch.md)

- [Amazon OpenSearch — Solution Architect Perspective](21_AWS_Databases/10_Amazon_OpenSearch.md#amazon-opensearch-—-solution-architect-perspective)

## [_template](21_AWS_Databases/_template.md)

- [_template — Solution Architect Perspective](21_AWS_Databases/_template.md#_template-—-solution-architect-perspective)### [01_aws_cloudwatch](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md)

- [AWS CloudWatch](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#aws-cloudwatch)
  * [EC2 Detailed monitoring](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#ec2-detailed-monitoring)
  * [CloudWatch Cutom Metrics](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#cloudwatch-cutom-metrics)
  * [CloudWatch Dashboards](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#cloudwatch-dashboards)
  * [CloudWatch Logs](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#cloudwatch-logs)
  * [Filters and Insight](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#filters-and-insight)
  * [S3 Export](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#s3-export)
  * [Log Subscription](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#log-subscription)
  * [Logs Aggregation, Multi-account and Multi region](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#logs-aggregation-multi-account-and-multi-region)
  * [CloudWatch Logs for EC2](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#cloudwatch-logs-for-ec2)
  * [CloudWatch Alarms.](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#cloudwatch-alarms)
  * [CloudWatch Events](22_AWS_Monitoring_And_Audit/01_AWS_CloudWatch.md#cloudwatch-events)

## [02_amazon_eventbridge](22_AWS_Monitoring_And_Audit/02_Amazon_EventBridge.md)

- [Amazon EventBridge](22_AWS_Monitoring_And_Audit/02_Amazon_EventBridge.md#amazon-eventbridge)
  * [Schema Registry](22_AWS_Monitoring_And_Audit/02_Amazon_EventBridge.md#schema-registry)
  * [Amazon EventBridge — Resource-based Policy](22_AWS_Monitoring_And_Audit/02_Amazon_EventBridge.md#amazon-eventbridge-—-resource-based-policy)
  * [Notes](22_AWS_Monitoring_And_Audit/02_Amazon_EventBridge.md#notes)

## [03_amazon_cloudtrail](22_AWS_Monitoring_And_Audit/03_Amazon_CloudTrail.md)

- [Amazon CloudTrail](22_AWS_Monitoring_And_Audit/03_Amazon_CloudTrail.md#amazon-cloudtrail)
  * [CloudTrail Events:](22_AWS_Monitoring_And_Audit/03_Amazon_CloudTrail.md#cloudtrail-events)
  * [CloudTrail Insights](22_AWS_Monitoring_And_Audit/03_Amazon_CloudTrail.md#cloudtrail-insights)

## [04_aws_config](22_AWS_Monitoring_And_Audit/04_AWS_Config.md)

- [AWS Config](22_AWS_Monitoring_And_Audit/04_AWS_Config.md#aws-config)
  * [Config Rules](22_AWS_Monitoring_And_Audit/04_AWS_Config.md#config-rules)
  * [Pricing](22_AWS_Monitoring_And_Audit/04_AWS_Config.md#pricing)
  * [AWS Config Resource](22_AWS_Monitoring_And_Audit/04_AWS_Config.md#aws-config-resource)
  * [Remediation](22_AWS_Monitoring_And_Audit/04_AWS_Config.md#remediation)
  * [Notifications](22_AWS_Monitoring_And_Audit/04_AWS_Config.md#notifications)### [01_aws_security_token_service](23_Advanced_Identity_and_Access_Manaegement/01_AWS_Security_Token_Service.md)

- [AWS STS — Security Token Service](23_Advanced_Identity_and_Access_Manaegement/01_AWS_Security_Token_Service.md#aws-sts-—-security-token-service)
  * [Using STS to Assume a Role](23_Advanced_Identity_and_Access_Manaegement/01_AWS_Security_Token_Service.md#using-sts-to-assume-a-role)

## [02_identity_federation](23_Advanced_Identity_and_Access_Manaegement/02_Identity_Federation.md)

- [Identity Federation in AWS](23_Advanced_Identity_and_Access_Manaegement/02_Identity_Federation.md#identity-federation-in-aws)

## [03_aws_organizations](23_Advanced_Identity_and_Access_Manaegement/03_AWS_Organizations.md)

- [AWS Organizations](23_Advanced_Identity_and_Access_Manaegement/03_AWS_Organizations.md#aws-organizations)
  * [Strategies / use cases](23_Advanced_Identity_and_Access_Manaegement/03_AWS_Organizations.md#strategies--use-cases)
  * [Service Control Policies (SCP)](23_Advanced_Identity_and_Access_Manaegement/03_AWS_Organizations.md#service-control-policies-scp)

## [04_iam_advanced](23_Advanced_Identity_and_Access_Manaegement/04_IAM_Advanced.md)

- [Advanced IAM](23_Advanced_Identity_and_Access_Manaegement/04_IAM_Advanced.md#advanced-iam)
  * [IAM Conditions](23_Advanced_Identity_and_Access_Manaegement/04_IAM_Advanced.md#iam-conditions)
  * [IAM for S3](23_Advanced_Identity_and_Access_Manaegement/04_IAM_Advanced.md#iam-for-s3)
  * [IAM Roles vs Resource Based Policies](23_Advanced_Identity_and_Access_Manaegement/04_IAM_Advanced.md#iam-roles-vs-resource-based-policies)
  * [IAM Permission Boundaries](23_Advanced_Identity_and_Access_Manaegement/04_IAM_Advanced.md#iam-permission-boundaries)

## [05_aws_resource_access_manager](23_Advanced_Identity_and_Access_Manaegement/05_AWS_Resource_Access_Manager.md)

- [AWS Resource Access manager (RAM)](23_Advanced_Identity_and_Access_Manaegement/05_AWS_Resource_Access_Manager.md#aws-resource-access-manager-ram)

## [06_aws_single_sign-on](23_Advanced_Identity_and_Access_Manaegement/06_AWS_Single_Sign-On.md)

- [AWS Single Sign-On (SSO)](23_Advanced_Identity_and_Access_Manaegement/06_AWS_Single_Sign-On.md#aws-single-sign-on-sso)### [00_aws_security_and_encryption](24_AWS_Security_And_Encryption/00_AWS_Security_And_Encryption.md)

- [AWS Security And Encryption](24_AWS_Security_And_Encryption/00_AWS_Security_And_Encryption.md#aws-security-and-encryption)
  * [Overview](24_AWS_Security_And_Encryption/00_AWS_Security_And_Encryption.md#overview)

## [01_aws_key_management_service](24_AWS_Security_And_Encryption/01_AWS_Key_Management_Service.md)

- [AWS Key Management Service (KMS)](24_AWS_Security_And_Encryption/01_AWS_Key_Management_Service.md#aws-key-management-service-kms)
  * [KMS — Customer Master Key (CMK) Types](24_AWS_Security_And_Encryption/01_AWS_Key_Management_Service.md#kms-—-customer-master-key-cmk-types)
  * [AWS KMS (Key Management Service)](24_AWS_Security_And_Encryption/01_AWS_Key_Management_Service.md#aws-kms-key-management-service)
  * [KMS — When, Why, How](24_AWS_Security_And_Encryption/01_AWS_Key_Management_Service.md#kms-—-when-why-how)
  * [KMS Key Policies](24_AWS_Security_And_Encryption/01_AWS_Key_Management_Service.md#kms-key-policies)
  * [KMS Key Rotation](24_AWS_Security_And_Encryption/01_AWS_Key_Management_Service.md#kms-key-rotation)

## [02_ssm_parameter_store](24_AWS_Security_And_Encryption/02_SSM_Parameter_Store.md)

- [SSM Parameter Store](24_AWS_Security_And_Encryption/02_SSM_Parameter_Store.md#ssm-parameter-store)

## [03_aws_secrets_manager](24_AWS_Security_And_Encryption/03_AWS_Secrets_Manager.md)

- [AWS Secrets manager](24_AWS_Security_And_Encryption/03_AWS_Secrets_Manager.md#aws-secrets-manager)

## [04_cloudhsm](24_AWS_Security_And_Encryption/04_CloudHSM.md)

- [Cloud HSM](24_AWS_Security_And_Encryption/04_CloudHSM.md#cloud-hsm)

## [05_aws_shield](24_AWS_Security_And_Encryption/05_AWS_Shield.md)

- [AWS Shield](24_AWS_Security_And_Encryption/05_AWS_Shield.md#aws-shield)

## [06_aws_web_application_firewall](24_AWS_Security_And_Encryption/06_AWS_Web_Application_Firewall.md)

- [AWS WAF — Web Application Firewall](24_AWS_Security_And_Encryption/06_AWS_Web_Application_Firewall.md#aws-waf-—-web-application-firewall)

## [07_aws_firewall_manager](24_AWS_Security_And_Encryption/07_AWS_Firewall_manager.md)

- [AWS Firewall Manager](24_AWS_Security_And_Encryption/07_AWS_Firewall_manager.md#aws-firewall-manager)

## [08_amazon_guardduty](24_AWS_Security_And_Encryption/08_Amazon_GuardDuty.md)

- [Amazon GuardDuty](24_AWS_Security_And_Encryption/08_Amazon_GuardDuty.md#amazon-guardduty)

## [09_amazon_inspector](24_AWS_Security_And_Encryption/09_Amazon_Inspector.md)

- [Amazon Inspector](24_AWS_Security_And_Encryption/09_Amazon_Inspector.md#amazon-inspector)

## [10_amazon_macie](24_AWS_Security_And_Encryption/10_Amazon_Macie.md)

- [Amazon Macie](24_AWS_Security_And_Encryption/10_Amazon_Macie.md#amazon-macie)

## [11_security_and_compliance](24_AWS_Security_And_Encryption/11_Security_And_Compliance.md)

- [Security and compliance](24_AWS_Security_And_Encryption/11_Security_And_Compliance.md#security-and-compliance)
  * [AWS Shared Responsability Model](24_AWS_Security_And_Encryption/11_Security_And_Compliance.md#aws-shared-responsability-model)### [00_virtual_private_cloud_introduction](25_AWS_Networking_VPC/00_Virtual_Private_Cloud_Introduction.md)

- [Virtual Private Cloud (VPC) introduction](25_AWS_Networking_VPC/00_Virtual_Private_Cloud_Introduction.md#virtual-private-cloud-vpc-introduction)
  * [Classless Inter-Domain Routing (CIDR) — IPv4](25_AWS_Networking_VPC/00_Virtual_Private_Cloud_Introduction.md#classless-inter-domain-routing-cidr-—-ipv4)
  * [Public vs Private](25_AWS_Networking_VPC/00_Virtual_Private_Cloud_Introduction.md#public-vs-private)
  * [Default VPC overview](25_AWS_Networking_VPC/00_Virtual_Private_Cloud_Introduction.md#default-vpc-overview)

## [01_virtual_private_cloud_overview](25_AWS_Networking_VPC/01_Virtual_Private_Cloud_Overview.md)

- [Virtual Private Cloud (VPC) — Overview](25_AWS_Networking_VPC/01_Virtual_Private_Cloud_Overview.md#virtual-private-cloud-vpc-—-overview)
  * [VPC — Subnet (IPv4), reserved IPs](25_AWS_Networking_VPC/01_Virtual_Private_Cloud_Overview.md#vpc-—-subnet-ipv4-reserved-ips)

## [02_internet_gateway](25_AWS_Networking_VPC/02_Internet_Gateway.md)

- [Internet Gateway (IGW)](25_AWS_Networking_VPC/02_Internet_Gateway.md#internet-gateway-igw)

## [03_bastion_hosts](25_AWS_Networking_VPC/03_Bastion_Hosts.md)

- [Bastion Hosts](25_AWS_Networking_VPC/03_Bastion_Hosts.md#bastion-hosts)

## [04_nat_instance](25_AWS_Networking_VPC/04_NAT_Instance.md)

- [NAT Instance (**outdated**, but still relevant)](25_AWS_Networking_VPC/04_NAT_Instance.md#nat-instance-outdated-but-still-relevant)

## [05_nat_gateways](25_AWS_Networking_VPC/05_NAT_Gateways.md)

- [NAT Gateways (NATGW)](25_AWS_Networking_VPC/05_NAT_Gateways.md#nat-gateways-natgw)
  * [NAT Gateway with High Availability](25_AWS_Networking_VPC/05_NAT_Gateways.md#nat-gateway-with-high-availability)

## [06_dns_and_route53_private_zones](25_AWS_Networking_VPC/06_DNS_And_Route53_Private_Zones.md)

- [DNS Resolution Options and Route 53 Private Zones.](25_AWS_Networking_VPC/06_DNS_And_Route53_Private_Zones.md#dns-resolution-options-and-route-53-private-zones)

## [07_security_groups_and_nacls](25_AWS_Networking_VPC/07_Security_Groups_And_NACLs.md)

- [Security Groups and NACLs](25_AWS_Networking_VPC/07_Security_Groups_And_NACLs.md#security-groups-and-nacls)
  * [SG](25_AWS_Networking_VPC/07_Security_Groups_And_NACLs.md#sg)
  * [NACL](25_AWS_Networking_VPC/07_Security_Groups_And_NACLs.md#nacl)
  * [Ephemeral Ports](25_AWS_Networking_VPC/07_Security_Groups_And_NACLs.md#ephemeral-ports)
  * [VPC Reachability Analyzer to check if everything is fine.](25_AWS_Networking_VPC/07_Security_Groups_And_NACLs.md#vpc-reachability-analyzer-to-check-if-everything-is-fine)

## [08_vpc_peering](25_AWS_Networking_VPC/08_VPC_Peering.md)

- [VPC Peering](25_AWS_Networking_VPC/08_VPC_Peering.md#vpc-peering)

## [09_vpc_endpoints](25_AWS_Networking_VPC/09_VPC_Endpoints.md)

- [VPC Endpoints](25_AWS_Networking_VPC/09_VPC_Endpoints.md#vpc-endpoints)

## [10_vpc_flow_logs](25_AWS_Networking_VPC/10_VPC_Flow_Logs.md)

- [VPC Flow Logs](25_AWS_Networking_VPC/10_VPC_Flow_Logs.md#vpc-flow-logs)

## [11_aws_site_to_site_vpn](25_AWS_Networking_VPC/11_AWS_Site_to_Site_VPN.md)

- [AWS Site-to-Site VPN](25_AWS_Networking_VPC/11_AWS_Site_to_Site_VPN.md#aws-site-to-site-vpn)
  * [Exam tips](25_AWS_Networking_VPC/11_AWS_Site_to_Site_VPN.md#exam-tips)
  * [AWS VPN CloudHub](25_AWS_Networking_VPC/11_AWS_Site_to_Site_VPN.md#aws-vpn-cloudhub)

## [12_direct_connect_dx](25_AWS_Networking_VPC/12_Direct_Connect_DX.md)

- [Direct Connect (DX)](25_AWS_Networking_VPC/12_Direct_Connect_DX.md#direct-connect-dx)
  * [Direct Connect Gateway](25_AWS_Networking_VPC/12_Direct_Connect_DX.md#direct-connect-gateway)
  * [Direct Connect — Connection Types](25_AWS_Networking_VPC/12_Direct_Connect_DX.md#direct-connect-—-connection-types)
  * [Direct Connect — Encryption](25_AWS_Networking_VPC/12_Direct_Connect_DX.md#direct-connect-—-encryption)
  * [Direct Connect — Resiliency](25_AWS_Networking_VPC/12_Direct_Connect_DX.md#direct-connect-—-resiliency)

## [13_aws_privatelink_vpc_endpoint_services](25_AWS_Networking_VPC/13_AWS_PrivateLink_VPC_Endpoint_Services.md)

- [AWS PrivateLink VPC Endpoint Services](25_AWS_Networking_VPC/13_AWS_PrivateLink_VPC_Endpoint_Services.md#aws-privatelink-vpc-endpoint-services)
  * [AWS PrivateLink (VPC Endpoint Services)](25_AWS_Networking_VPC/13_AWS_PrivateLink_VPC_Endpoint_Services.md#aws-privatelink-vpc-endpoint-services)

## [14_ec2_classic_aws_classiclink_deprecated](25_AWS_Networking_VPC/14_EC2_Classic_AWS_ClassicLink_deprecated.md)

- [EC2 Classic and AWS ClassicLink ( deprecated )](25_AWS_Networking_VPC/14_EC2_Classic_AWS_ClassicLink_deprecated.md#ec2-classic-and-aws-classiclink--deprecated-)

## [15_transit_gateway](25_AWS_Networking_VPC/15_Transit_Gateway.md)

- [Transit Gateway](25_AWS_Networking_VPC/15_Transit_Gateway.md#transit-gateway)

## [16_vpc_traffic_mirroring](25_AWS_Networking_VPC/16_VPC_Traffic_Mirroring.md)

- [VPC — Traffic Mirroring](25_AWS_Networking_VPC/16_VPC_Traffic_Mirroring.md#vpc-—-traffic-mirroring)

## [17_vpc_ipv6](25_AWS_Networking_VPC/17_VPC_IPv6.md)

- [VPC — IPv6](25_AWS_Networking_VPC/17_VPC_IPv6.md#vpc-—-ipv6)

## [18_egress_only_internet_gateway](25_AWS_Networking_VPC/18_Egress_only_Internet_Gateway.md)

- [Egress-only Internet Gateway](25_AWS_Networking_VPC/18_Egress_only_Internet_Gateway.md#egress-only-internet-gateway)### [00_disaster_recovery](26_Disaster_Recovery_And_Migration/00_Disaster_Recovery.md)

- [Disaster Recovery in AWS](26_Disaster_Recovery_And_Migration/00_Disaster_Recovery.md#disaster-recovery-in-aws)
  * [Disaster Recovery Strategies](26_Disaster_Recovery_And_Migration/00_Disaster_Recovery.md#disaster-recovery-strategies)
  * [Disaster Recovery Tips](26_Disaster_Recovery_And_Migration/00_Disaster_Recovery.md#disaster-recovery-tips)

## [01_db_migration_service](26_Disaster_Recovery_And_Migration/01_DB_Migration_Service.md)

- [Database Migration Service (DMS)](26_Disaster_Recovery_And_Migration/01_DB_Migration_Service.md#database-migration-service-dms)

## [02_on-premises_strategies](26_Disaster_Recovery_And_Migration/02_On-Premises_Strategies.md)

- [On-Premises strategies with AWS](26_Disaster_Recovery_And_Migration/02_On-Premises_Strategies.md#on-premises-strategies-with-aws)

## [03_datasync](26_Disaster_Recovery_And_Migration/03_DataSync.md)

- [AWS DataSync](26_Disaster_Recovery_And_Migration/03_DataSync.md#aws-datasync)
  * [Transferring large amount of data scenario](26_Disaster_Recovery_And_Migration/03_DataSync.md#transferring-large-amount-of-data-scenario)

## [04_aws_backup](26_Disaster_Recovery_And_Migration/04_AWS_Backup.md)

- [AWS Backup](26_Disaster_Recovery_And_Migration/04_AWS_Backup.md#aws-backup)
  * [AWS Backup Vault Lock](26_Disaster_Recovery_And_Migration/04_AWS_Backup.md#aws-backup-vault-lock)### [101_machine_learning_overview](27_Machine_Learning/101_Machine_Learning_Overview.md)

- [Machine Learning](27_Machine_Learning/101_Machine_Learning_Overview.md#machine-learning)
  * [Rekognition](27_Machine_Learning/101_Machine_Learning_Overview.md#rekognition)
  * [Transcribe](27_Machine_Learning/101_Machine_Learning_Overview.md#transcribe)
  * [Polly](27_Machine_Learning/101_Machine_Learning_Overview.md#polly)
  * [Translate](27_Machine_Learning/101_Machine_Learning_Overview.md#translate)
  * [Amazon Lex & Connect](27_Machine_Learning/101_Machine_Learning_Overview.md#amazon-lex--connect)
  * [Comprehend](27_Machine_Learning/101_Machine_Learning_Overview.md#comprehend)
  * [SageMaker](27_Machine_Learning/101_Machine_Learning_Overview.md#sagemaker)
  * [Amazon Forecast](27_Machine_Learning/101_Machine_Learning_Overview.md#amazon-forecast)
  * [Amazon Kendra](27_Machine_Learning/101_Machine_Learning_Overview.md#amazon-kendra)
  * [Personalize](27_Machine_Learning/101_Machine_Learning_Overview.md#personalize)
  * [Textract](27_Machine_Learning/101_Machine_Learning_Overview.md#textract)### [01_event_processing](28_More_Solutions_Architectures/01_Event_Processing.md)

- [Event Processing in AWS](28_More_Solutions_Architectures/01_Event_Processing.md#event-processing-in-aws)
  * [Lambda, SNS and SQS](28_More_Solutions_Architectures/01_Event_Processing.md#lambda-sns-and-sqs)

## [00_other_services](29_Other_Services/00_Other_Services.md)

- [Other AWS Services](29_Other_Services/00_Other_Services.md#other-aws-services)
  * [CICD (Continuous Integration and Delivery)](29_Other_Services/00_Other_Services.md#cicd-continuous-integration-and-delivery)
  * [CloudFormation, Infrastructure as Code](29_Other_Services/00_Other_Services.md#cloudformation-infrastructure-as-code)
    + [CloudFormation StackSet](29_Other_Services/00_Other_Services.md#cloudformation-stackset)
  * [AWS Step Functions](29_Other_Services/00_Other_Services.md#aws-step-functions)
  * [AWS Simple Workflow Service (SWF)](29_Other_Services/00_Other_Services.md#aws-simple-workflow-service-swf)
  * [Amazon Elastic Map Reduce (EMR)](29_Other_Services/00_Other_Services.md#amazon-elastic-map-reduce-emr)
  * [AWS Opsworks](29_Other_Services/00_Other_Services.md#aws-opsworks)
  * [AWS Workspaces](29_Other_Services/00_Other_Services.md#aws-workspaces)
  * [AWS AppSync](29_Other_Services/00_Other_Services.md#aws-appsync)
  * [Cost Explorer](29_Other_Services/00_Other_Services.md#cost-explorer)
