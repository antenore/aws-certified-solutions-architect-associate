# AWS CloudFront & AWS Global Accelerator

## AWS CloudFront

* CDN
* Content cached at the edge location (216++ Point of Presence)
* DDoS protection
* Expose HTTPS, can tal HTTPS internally
* Started with S3 buckets
    * files distribution and caching
    * Enhanced sec with Origin Access Identity (OAI)
    * Used as in ingress for file upload.
* With Custom Origin (HTTP)
    * ALB
    * EC2 instance
    * S3 Website
    * Any HTTP backend

* Client -> EDGE -> OAI -> S3 Origin. Edge cache the content.
* To allow Edge location IPs in the Security Groups (for EC2 origin) there's an amazon website with all the Edge' IPs.
* CloudFront can use Geo restriction (Allowed Edges by geo location)

### CloudFront vs S3 Cross Region Replication

* CloudFront
    * Global Edge network
    * Files cached for a TTL
    * Great for static content must be available everywhere.
* S3 Cross Region Replication:
    * To setup on each region
    * Files updated in near real-time
    * Read Only
    * Great for dynamic content, low latency in few regions.

## CloudFront Signed URL / Signed Cookies

* Distribute paid shared content to premium users.
* We define:
    * Includes URL expiration
    * IP ranges allowed to access data
    * Trusted Signers (AWS accounts allowed to upload content)
* URL validity:
    * e.g. Few minutes for shared content
    * e.g. Years for Private content.
* Signed URL => Access individual files
* Signed Cookies = Multiple files (one cookie, many files)

### Pricing of CloudFront

* Pricing change by Edge Location
* Reducing Edge location -> Reduce costs
* Limit some expensive Edge location for cost saving

### Multiple Origin

* Based on content type
* Based on path pattern

### Origin Groups

* High availability
* Do failover

### CloudFron FieldLevel Encryption

* Edge location can encrypt up to 10 fields in the POST request, like credit card and similar.

## AWS Global Accelerator

* It use Anycast IPs to automatically route traffic to the closest Edge location
* This avoids many hops in the public internet (e.g. Between USA and India there are at least 6 hops)
* It reduces latency
* **2 Anycast IP addressed are assigned to your application**.
* With Elastic IP, EC2, ALB, NLB, public or private.
* Has Health Checks
* Great for DRP, games, IoT, etc
* DDoS protection
* No caching compared to CloudFront
*


