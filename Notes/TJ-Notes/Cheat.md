# Loose Notes

| Service                       | Cheat note |
|-------------------------------|------------------------------------------------------------------------------------------------------------|
| Application Load Balancer     | Have Weighted Target Groups. Only one ALB for multiple target groups. (remember NLB doesn't have weighted) |
| Application Load Balancer     | Typically: Client → HTTPS (443) → ALB → HTTP (80) → EC2 → PostgreSQL (5432). |
| Application Load Balancer     | Can use Cognito to authenticate users using User Pools, offloading the application backend.|
| Athena                        | Using with Apache Parquet improve performance and lowers costs (lower space on S3, 6x faster for queries).|
| Aurora Replica fail-over      | Aurora flips CNAME record to the healthy instance.|
| Aurora server-less failover   | Attempt to recreate DB instance in a new AZ|
| Aurora single inst. failover  | Attempt to create a new DB instance in the same AZ, best-effort basis.|
| Aurora multi-master           | No failoveras there's always another master that can take over.|
| Auto Scaling Group            | Adding an EC2 req.: running state, AMI, not member another SG, same AZ as the SG, with LB same VPC. |
| Auto Scaling Group            | Remember, the minimum required by a question must take into account if the AZ goes down|
| Auto Scaling group            | When need a set of adjustment, then use Step scaling|
| Auto Scaling group            | Lifecycle hook EC2_INSTANCE_LAUNCHING/EC2_INSTANCE_TERMINATING to Pending:Wait/Terminating:Wait|
| Auto Scaling Group            | Launch Template: Mixed On-demand and Spot instances. Launch Configuration: No Mix. |
| CloudFormation                | StackSets: An administrator can create and manage stacks across multiple accounts and regions (using templates)|
| CloudFormation                | CreationPolicy associated to a resource, it prevents from reaching create complete till enough success signals.|
| CloudFormation                | To signal a resource use cfn-signal helper script or SignalResource API |
| CloudFront                    | High Availability. Add an origin group with a primary and secondary origin. Auto failover to secondary if 504|
| CloudFront                    | origin access control (OAC) and origin access identity (OAI legacy). Send authenticated requests to S3 |.
| CloudTrail                    | Encrypt by default on S3 with SSE, you could use also AWS KMS key|
| Cognito                       | User Pools: Directory of users, authentication + WUI + Social sign-in + profiles + MFA + migration workflows with Lambda.|
| Cognito                       | Identity Pools: AWS Credentials for users (User Pools, IdPs, OpenID, SAML) to access AWS resources using pool tokens.|
| Connector                     | Integrates with IAM Roles. Good when you are already using groups in the corporate AD.|
| DLM                           | Amazon Data Lifecycle Manager, can be used to automate EBS snapshots creation.|
| DMS                           | Database Migration Service, if S3 is the target, full-load and change data capture (CDC) are written to CSV.|
| DynamoDB                      | Gateway Endpoint, see same consideration for S3.|
| DynamoDB                      | Follow feature: Stream -> Lambda (remember IAM role for perms) -> SNS|
| EBS                           | Data moving between EBS and EC2 are encrypted|
| EBS                           | Encryption By Default is per region and works only if the instance type support encrypted EBS|
| EBS Snapshots                 | Asynchronous|
| EBS Snapshots                 | Automatically encrypted|
| EBS Snapshots                 | Do not impact concurrent read/write|
| EC2                           | **non-default** subnets, don't have a public IP, hence you add one to allow the EC2 to connect to internet.|
| EC2 Billing                   | Reserved, till end of the contract.|
| EC2 Billing                   | Billed: Running, Stopping if to hibernate.|
| EC2 Billing                   | Not Billed: Pending, stopping if preparing for stop, Stopped, shutting-down, terminated.|
| ECS                           | ECS scaling per CPU, RAM and ALBRequestCountPerTarget (N° of req. completed per target in ALB target group)|
| EFA                           | ENA plus bypass OS capabilities|
| EFA                           | Not supported on Windows|
| EFS                           | For EC2, ECS (auto mount), General Purpose or Max I/O mode, and Bursting Throughput or Provisioned Throughput|
| EIP                           | No charges for a **running** instance with **only 1** EIP **attached**|
| EKS                           | Relies on Kubernetes RBAC, plus AWS IAM Authenticator for Kubernetes using aws-auth ConfigMap (AWS auth configuration map)|
| ELB                           | One ELB per region, NOT across multiple regions.|
| EMR                           | Managed Hadoop (or Spark, HBase, Presto, Flink). It can be used also for log analysis.|
| Egres-Only Internet Gateway   | VPC with **IPv6** (only) to connect from a private subnet to internet (but not the contrary).|
| Elastic Beanstalk             | Choose Beanstalk in lieu of ECS when you need automatic scaling, monitoring, balancing and container placement.|
| Geo Proximity                 | Remember bias. |
| Global Accelerator            | Also used as entry point for multi-region application (it scales the network). (remember white list)|
| Global Accelerator            | Like a CloudFront replacement when the protocol is different from HTTP (like gaming servers on UDP)|
| Global accelerator            | Optimize the path from your users to your application. TCP, and UDP performance improvement.|
| Global Accelerator            | Good fit for non-HTTP use cases, or HTTP when static IP is required (deterministic, fast regional failover) |
| Glue                          | Extract, Transform and Load (ETL) service. Auto discovery data to Glue Data Catalog, queried/analaysed afterwards|
| Guard duty                    | Only threats detection, is not a service to solve problems.|
| HDD                           | High Throughput, and low cost|
| HDD                           | No boot|
| HDD                           | large, sequential I/O operations|
| HDD                           | Large streaming workloads, Big Data, Data warehouse, Log processing, infrequently accessed data.|
| HSM                           | Administrator wrong login attempts > 2, zeros the HSM, cannot be restored.|
| IAM                           | Permission boundaries applied to users and roles NOT groups.|
| Intel 82599 Virtual Function  | Not supported by some instance type.|
| Interface Endpoint            | Used to access a service without leaving the AWS network. Most AWS services, and some AWS partners services.|
| Kinesis Data Firehose         | into S3, Redshift, Elasticsearch and Splunk.|
| Kinesis Data Firehose         | No data storage. Can **transform** data with a Lambda (not destination).|
| Kinesis Data Firehose         | NO replay functionality.|
| Kinesis Data Streams          | into Kinesis Data Analytics, Spark on EMR, EC2, AWS Lambda.|
| Kinesis Data Streams          | We can raise the number of shards, we can increase the write capacity assigned to the shared table.|
| Kinesis Data Streams          | Replay functionality.|
| Kinesis & Firehose            | Kinesis manual shard management (need administration), Firehose fully managed (no administration)|
| KMS                           | KMS keys must have 1 key policy (who can access the key?). IAM policies are helpless without the key policy.|
| Lambda@Edge                   | to customize CloudFront content, closer to the user. Viewer/Origin Response/Request |
| Mobile / 5G                   | AWS WaveLength|
| NAT Gateways                  | If you can choose in lieu of NAT Instance|
| Nat Gateways                  | When from private subnet you connect to internet.|
| Nat Instances                 | NOT supported anymore. |
| Network Firewall              | can be used also to inspect traffic, not only to block|
| OpsWorks                      | Puppet, Chef|
| Proton                        | Deploy serverless or container-based apps through pipelines and templates |
| Provisioned IOPS SSD ( io1 )  | 4 GiB to 16 TiB, You can provision 100-64000 IOPS with ratio 50:1 per GiB of the volume (100 GiB -> 5000 IOPS)|
| Provisioned IOPS SSD ( io1 )  | DB, MongoDB, etc, High Throughput.|
| Provisioned IOPS SSD ( io1 )  | For low latency set low queue length|
| Provisioned IOPS SSD ( io1 )  | From 1280 GiB = 64000 IOPS.|
| RAID 0                        | When I/O performance is of the utmost importance. Temp files Instance Store, EBS preferable more flexibility and persistence|
| RAID 1                        | Mirroring, not advised for booting|
| RDS Events                    | ONLY DB instance events, DB parameter group events, DB security group events, and DB snapshot events |
| RDS                           | Can invoke Lambda from native functions or stored procedures. To integrate with other AWS services (SQS) |
| RDS                           | For automatic fail-over use Multi-AZ. RDS set up a standby instance in another AZ **in the same Region**.|
| RDS                           | Multi-AZ failover, in case of switch the CNAME is switched to the secondary instance.|
| RDS                           | SSL = rds.force_ssl parameter (DB must be rebooted)|
| RDS                           | You can authenticate with IAM if you have MySQL or PostgreSQL. Tokens have a validity of 15 minutes.|
| RDS                           | Automated backup retention up to 35 days MAX.|
| RDS                           | Snapshots saved to S3, you DON'T have direct access to the snapshot files.|
| Redshift Cluster              | Cross-Region Snapshots Copy have to be enabled (additional charges?)|
| S3                            | Access Control: IAM Policies → User Level Control, ACL → Account Level, Bucket Policies → Both (Account and User).|
| S3                            | Who upload the file is the owner of the object, NOT the owner of the bucket. You need an IAM role to workaround cross-account loading|
| S3                            | Access point (Network origin) it's to allow access only from a specific VPC, you can have multiple access points|
| S3                            | At least 3500 PUT requests per second, and 5500 GET requests per seconds, for each prefix (unlimited prefixes)|
| S3                            | Integrated with CloudTrail, and have Enable Server access logging for additional metrics.|
| S3                            | 30 days in the current storage class before transition to STANDARD_IA or ONEZONE_IA.|
| S3                            | NO min storage duration only for Standard and Intelligent-Tiering|
| S3                            | Objects are private by default, read access can be granted during the upload phase.|
| S3 SELECT                     | Bucket's name and object's key.|
| S3 Gateway endpoints          | Public IP, S3 DNS names, no access from on-premises or another region, **Not billed**.|
| S3 Interface endpoints        | Private IP, require endpoint DNS names, allow from on-premises and other regions (peering or TGW), **billed**.|
| S3 Glacier                    | Can retrieve data in 1 – 5 minutes using expedited retrieval ($$)|
| S3 Standard IA                | Can transit to, only after 30 days in previous class.|
| S3 Intelligent Tiering        | Additional fee for monitoring and automation for moving S3 objects between S3 and S3-IA. NOT cost effective.|
| S3 Transfer Accelerator       | For files > 1 GiB use S3TA, otherwise CloudFront. |
| SDD                           | Can boot|
| SDD                           | small, random I/O operations|
| SDD                           | Transactional workloads, Critical business apps that requires high IOPS, Large DB.|
| Secret Manager                | Protect secrets needed to access your applications, services and resources. Automatic rotation.|
| SQS                           | ApproximateAgeOfOldestMessage metric to ensure messages are processed in a specif time period.|
| SQS                           | Change retention period with SetQueueAttributes action up to 14 days (default 4)|
| SQS                           | Default retention period 4 days, after SQS delete messages from queue|
| SQS                           | Long polling to reduce costs using ReceiveMessageWaitTimeSeconds > 0, but lower throughput|
| SQS                           | Visibility timeout 30 seconds, the default, up to 12 hours.|
| SQS FIFO                      | High Throughput, Exactly-Once Processing (no duplicates), First-In-First-Out Delivery (strict order)|
| SQS Standard                  | Unlimited Throughput, At-Least-Once Delivery, Best-Effort Ordering|
| SSM Parameter Store           | More than Secret Manger, but does not rotate secrets.|
| Step Functions                | Serverless orchestration.|
| Storage Gateway Cached Volume | Keep files on S3 frequently accessed files stored/cached locally on on-premises. Cost saving.|
| SWF                           | Simple Workflow Service, in addition and concert with SQS to decouple architectures.|
| Systems Manager Run Command   | To remotely configure EC2 instances|
| Timestream                    | Serverless time series database service that is commonly used for IoT and operational applications |
| Traffic Mirror                | Alone it does nothing but duplicate the network traffic and send it somewhere it can be used/analysed.|
| Transit Gateway               | Simplify connectivity between multiple VPC, or to connect to any VPC with a single VPN through a Transit GW.|
| Transit Gateway               | Use equal-cost multi-path (ECMP) routing to use multiple VPN tunnels (one Transit GW, any VPC)|
| Trusted Advisor               | Cost optimisation, Security, Fault Tolerance, Performance, Service Limits.|
| VPC Gateway Endpoint          | S3, Dynamo DB, other services, PrivateLink (Endpoint Services), etc|
| VPC Interface Endpoint        | Most of the AWS services, except (rule of thumb)|
| VPC Endpoint Service          | You can set up an application as an AWS PrivateLink-powered service. Other can connect using VPN endpoint.|
| WAF                           | Can geo-block from specific countries.|
| WAF                           | Can have ACL to allow traffic from approved IP addresses.|
| X-Ray                         | Traces users requests as they travel through different services.|
