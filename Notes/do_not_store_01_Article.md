[How I Passed AWS Solutions Architect Associate (SAA-C02) in 2021 | The Startup](https://medium.com/swlh/how-i-passed-aws-solutions-architect-associate-saa-c02-in-2021-70ec503fa963)

# How I Passed AWS Solutions Architect Associate (SAA-C02) in 2021

## An FAQ about preparing and taking the AWS Solutions Architect Associate exam.

I passed the AWS Solutions Architect Associate exam in January 2021 with a score of 94%. In this article, I’d like to quickly summarize why I chose this certification program and how I prepared. I’ll answer multiple frequently asked questions and discuss if it’s a good choice for Backend/Frontend Engineers, Data Engineers/Scientists, DevOps/Sysops, and Engineering Managers. Let’s get started!

![](https://miro.medium.com/max/680/0*CNLfP938vNtb8wkG.png)

# AWS Solutions Architect Associate (SAA-C02) FAQ

## Why should I choose the Solutions Architect Associate among the other AWS certifications?

That’s because attending this certification program will teach you how to _design_ reliable, secure, performant, and cost-efficient systems using AWS. You’ll find out how to map business requirements into a robust architecture and how to guide it through the project. I find it useful during my daily job as a backend software engineer and when working on personal projects.

Whenever you’ll be designing an architecture for your system, you’ll be more proficient in picking the best tools for the job. You’ll know the most important AWS services from the five angles:

- Operational Excellence
- Reliability
- Security
- Performance
- Cost-efficiency

They are called “_The 5 Pillars_” and are defined in the [AWS Well-Architected Framework](https://aws.amazon.com/architecture/well-architected). This certification puts a strong emphasis on those in the context of solution architecture on AWS. More on that later.

There’s a competing certification for developers called [AWS Certified Developer Associate](https://aws.amazon.com/certification/certified-developer-associate/). It focuses more on developing, deploying, and maintaining applications on AWS. After some consideration, I chose, however, Solutions Architect as it gives a broad overview of “_what’s there in AWS_” and “_why and when and how can I use it to solve my problems_”. It’s true SAA-C02 doesn’t focus that much on the specifics of the services related to software development, but it teaches you when and why would you want to apply them. You’ll be able to profoundly argue e.g. why service X or Y would be a good choice in this particular solution and why other alternatives don’t fit well. All of that in terms of the 5 pillars. You wouldn’t then struggle to apply them in practice either.

## Is this certificate recommended for Backend Engineers?

Absolutely. It covers services you need for the backend development like Kinesis, SQS, DynamoDB, RDS/Aurora, Elasticache, CloudWatch, Lambda, S3, EC2, IAM, ECS… What’s more, the exam is based on the [AWS Well-Architected Framework](https://docs.aws.amazon.com/wellarchitected/latest/framework/wellarchitected-framework.pdf). This is an official AWS guideline that teaches you how to drive your architectural decisions based on reliability, security, efficiency, and cost-effectiveness rather than a gut feeling.

However, as mentioned, the exam puts more emphasis on solution architectures to different problems across different industries rather than pure software development. That means you’ll also learn about services and techniques not that relevant for daily backend development. For example, connecting on-premise datacenter with AWS cloud. Services like AWS Storage Gateway, FSx, Snowball or DirectConnect might fall in that category.

You’ll learn:

- How to build highly scalable backend systems either provisioned (EC2-based) or serverless (Lambda-based). How to scale (ASG) and load balance (ALB/NLB/CLB). How to optimize cost with Spot and Reserved Instances.
- What’s the difference between Kinesis vs SQS vs SQS FIFO vs SNS and when to use each. What are performance, security, and cost-effectiveness considerations.
- When to use DynamoDB vs RDS vs Elasticache. How DynamoDB scales and how to increase its performance. Achieving high availability with RDS databases. Using encryption. How to use backups and snapshots with point-in-time recovery/time traveling. Auto-scaling with Aurora Serverless.
- Why, when, and how to use API Gateway. For example, how to perform user authorization against Cognito user pool.
- Using KMS and storing secrets for backend applications in SSM or Secrets Manager. Automatic credential rotation.
- How to do logging, monitoring, and alerting with CloudWatch.
- How to use CloudFormation and how to detect and handle drifts.
- IAM roles, policies, users, groups. Best practices of getting programmatic access to AWS API (using credentials, STS, metadata service).
- and much more…

All of that in the context of real-world scenarios.

## What about Frontend Engineers?

Yes! You’ll learn about Cloudfront, S3, Route53, Lambda, Cognito, IAM, CloudWatch. Again, with emphasis on reliability, security, efficiency, and cost-effectiveness rather than a gut feeling. You’ll learn:

- How to build highly scalable static/dynamic websites with Cloudfront and S3 with little effort. How to cost-effectively distribute it to AWS Edge locations around the world to provide the best performance and experience to the users.
- What is the consistency and performance characteristics of S3 and how to increase it.
- How to use WAF, Shield, and S3 encryption to increase security and prevent DDoS attacks.
- How to use Lambda@Edge to customize the content served by CloudFront.
- How to use Route53 to set up your own domain and hosted zone with different routing policies.
- How to optimize costs of storing static files in S3.

## What about Data Engineers/Scientists?

I have a bit mixed feelings here. This exam touches not that many services useful for Data Engineers/Scientists. You’ll see:

- AWS Glue
- EMR
- S3
- Athena
- Redshift

During the exam, I had just one or two questions in this area. If you want to explore AWS from the data perspective, I’d recommend looking into the specialty certificates:

- [AWS Certified Machine Learning Specialty](https://aws.amazon.com/certification/certified-machine-learning-specialty/)
- [AWS Certified Data Analytics Specialty](https://aws.amazon.com/certification/certified-data-analytics-specialty/)

## What about DevOps/SysOps?

Yes! You’ll learn:

- How to set up a VPC from scratch.
- How to create private/public subnets with high availability. Setting up internet gateway, NAT gateway, route tables.
- How to connect two VPCs with VPC peering.
- How to enable private connection from services inside a VPC to AWS services without going through the internet with VPC endpoints.
- How to monitor VPC with Flow Logs.
- How to connect on-premise data center with VPC: VPN, DirectConnect, TransitGateway, CloudHub.
- How to manage larger organizations in AWS with multiple accounts and cross-account access using AWS Organizations. How to easily globally restrict access to certain services.
- How to use Route53 to set up domain(s) and hosted zone(s) with different routing policies.

## What about Engineering Managers?

I would say yes. However, the exam is very technical and will require you to study all of the mentioned above. If you’re an Engineering Manager, the Solutions Architect exam will help get have a better overview of how to use AWS to meet compliance requirements and what are its auditing capabilities.

You’ll be exposed to industry-specific solution architectures and to trade-offs those architectures come with. This might be interesting for you to understand them and see what are the other options. This will help you work with engineering teams more productively. You can find example solutions architectures here: [https://aws.amazon.com/solutions/](https://aws.amazon.com/solutions/).

Besides, you’ll learn:

- How to use AWS Trusted Advisor to increase security, optimize costs and monitor service limits.
- How to use AWS Config to continuously monitor if the current configuration meets all the compliance requirements.
- How to use AWS CloudTrail for auditing. How to perform analytics of the audit logs with Athena.
- What are security considerations when using encryption in AWS (S3, EBS, RDS). When to use each encryption method in S3.
- How to make sure that data cannot be easily deleted from S3.

## What did I like about this certification?

I liked the fact, that it focuses on solutions architecture. That means you’ll learn how to _map_ real-world problems into AWS implementation always considering reliability, security, efficiency, and cost-effectiveness. I find this a superpower and probably the best entry point to all other AWS certificates.

## What did I dislike about this certification?

Since I’m a backend engineer, I only use a subset of services required for the exam on a daily basis. As you’re required to learn about different areas of AWS, like connecting on-premise datacenters, you might find some of them useless at this point of your career. As mentioned, this is where I had a bit of mixed feelings at the beginning and I was considering switching to the AWS Certified Developer path. However, from the time perspective, I find it great I could explore all the areas in AWS from the solutions architecture perspective. After going through this path you’ll be ready to dive deeper into the specific areas of your interest more smoothly.

## What are the prerequisites?

AWS says that you need at least one year of commercial experience. While I do see the benefit of having that experience in the context of the exam, I’m sure you can prepare well without it.

Before you invest that much time for preparation, make sure you understand _why_ you want to attend it. Are you using/will you be using AWS? Will you be designing highly scalable systems? Will it be useful in your daily job? Does this certification program play well with your short/long-term roadmap? Will you be working closely as a consultant/freelancer with the customers having their infrastructure on AWS? Will it be useful in your next (pet) project?

## How to prepare?

I didn’t participate in the official AWS training. In hindsight, you don’t really need one as there are enough high-quality materials online. I did the following steps in the same order as listed below to prepare:

1. I took [this course](https://www.udemy.com/course/aws-certified-solutions-architect-associate-saa-c02/) by

    [Stéphane Maarek](https://medium.com/u/98189c4cef0?source=post_page-----70ec503fa963--------------------------------)

    and I totally recommend it. It puts emphasis on **understanding** rather than cramming with full hands-on for each AWS service required for the exam. It doesn’t blindly teach you things just to pass it. It’s a pity AWS documentation lacks such hands-on videos. This course might serve as a good reference in the future as it’s constantly updated and videos are re-recorded to catch up with the changes. After this course, my first scores in the preparation exams were around 75%–80%.
2. I took [those six preparation exams](https://www.udemy.com/course/aws-certified-solutions-architect-associate-amazon-practice-exams-saa-c02/) by

    [Jon Bonso](https://medium.com/u/c90a38dd4dea?source=post_page-----70ec503fa963--------------------------------)

    . They resemble the questions you’ll see in the exam, although they’re obviously not the same. This is a good learning opportunity for you and the ultimate way to consolidate your knowledge after the course. After each practice exam, make sure you deeply understand why you answered wrong or had to guess. Those preparation exams come with solid explanations. Study them well, but also go to AWS console/CLI and try out things yourself if possible! **Understanding is your goal and passing the exam is just a side effect. Not the other way around :).**
3. I took [another six preparation exams](https://www.udemy.com/course/aws-certified-solutions-architect-associate-practice-tests-k/) by Neal Davies. Although the ones in step 2. somewhat better resemble the real exam, it’s also a good learning opportunity. I found those questions a bit more tricky though.
4. In parallel with practice exams, I was reading the FAQs for the most important AWS services. In my case those were: Cloudfront, DynamoDB, AWS Glue, RDS, Aurora, Lambda, ECS, Kinesis Data Streams/Firehose/Analytics, CloudTrail, AWS SWF, AWS Step Functions, Route53, EC2, S3, VPC, SQS, IAM, Inspector, Trusted Advisor, AWS Backup, DataSync, Global Accelerator, Storage Gateway, Elastic Beanstalk. I read carefully all the FAQs that were interesting for me as a backend engineer, for example, DynamoDB, RDS, Aurora, Lambda, Kinesis, S3, EC2, Route53, Cloudfront, SQS, ECS, … I only skimmed through others. Don’t get discouraged by the length though.
5. I read the [AWS Well-Architected Framework whitepaper](https://docs.aws.amazon.com/wellarchitected/latest/framework/wellarchitected-framework.pdf).

That’s it. You have a solid practical knowledge about AWS and now you’re ready for the exam.

## What scores did I achieve in the practice exams?

With [preparation exams](https://www.udemy.com/course/aws-certified-solutions-architect-associate-amazon-practice-exams-saa-c02/) by

[Jon Bonso](https://medium.com/u/c90a38dd4dea?source=post_page-----70ec503fa963--------------------------------)

I received in order: 73%, 76%, 80%, 87%, 96%, 83%. With [preparation exams](https://www.udemy.com/course/aws-certified-solutions-architect-associate-practice-tests-k/) by Neal Davies I received in order: 90%, 80%, 86%. I skipped the last 3 as I felt I’m ready for the exam.

If your scores are around 80%-85%, then you’re ready to take the exam.

## How long does it take to prepare?

In my case, it was 2 months. I studied intensively since the beginning of November 2020. I had 2 weeks break in the middle and ultimately passed the exam in mid-January 2021.

It took me 1.5 months to go through the course (step 1.). I spend around 1–2h daily + a few hours during the weekends. During this time I also experimented with AWS on my own to try out things.

It took me around 2 weeks for the practice exams. On one morning I was doing the test and on another, I was going through the explanations. I was reading the FAQs in between. I repeated that until I felt ready.

My advice here is to try to study regularly. It takes usually between 2–4 months to prepare if you work full time depending on your schedule & prior experience with AWS. Sometimes even longer.

## What are the learning tips?

- Take your time and do all the hands-on. Your goal is to be able to design systems with AWS and hands-on experience is the only way to deeply understand how things work. Use your personal free-tier AWS account.
- Imagine you could improve the architecture of the system in your current project/pet project. What would you do to improve: reliability, security, cost-efficiency, performance?
- Go to [AWS Solution Architectures library](https://aws.amazon.com/solutions/) and pick an architecture in a domain you’re familiar with. Try to design it without looking at the solution. Compare your result with the one found in the library.
- Always consider reliability, security, cost-efficiency, performance when learning new AWS services.
- Learning about VPCs is a good opportunity to refresh your knowledge about networking.

## Are there example questions?

They’re available on the AWS exam website [here](https://d1.awsstatic.com/training-and-certification/docs-sa-assoc/AWS-Certified-Solutions-Architect-Associate_Sample-Questions.pdf).

## What is the format of the exam?

It takes 140 minutes to answer 65 questions. 72% is the passing score.

Most of the questions are single-select answers although there are a few with multi-select answers as well. It’s explicitly stated in the question how many answers should you select.

## What’s the price?

It’s $150.

## What if I fail?

Then you need to wait 2 weeks and you can retake it. Unfortunately, you need to pay $150 for every retake.

## Can I take it remotely?

Yes! You can take it remotely and it’s quite convenient. More on that below.

## What are other recommended resources?

- [Official AWS exam website](https://aws.amazon.com/certification/certified-solutions-architect-associate/)
- [Official AWS exam guide](https://d1.awsstatic.com/training-and-certification/docs-sa-assoc/AWS-Certified-Solutions-Architect-Associate_Exam-Guide.pdf)
- [r/AWSCertifications subreddit](https://www.reddit.com/r/AWSCertifications/)
- [AWS library of solution architectures](https://aws.amazon.com/solutions/) — this one is interesting. You can find solution architectures to common problems across different industries.
- [AWS Architecture Center](https://aws.amazon.com/architecture/)
- [A study guide for the exam](https://tutorialsdojo.com/aws-certified-solutions-architect-associate-saa-c02/)

## What are the tips for taking the exam?

- Make sure you’re in a silent room.
- Use a wired internet connection e.g. with Ethernet cable.
- Call in 30 minutes before as the registration takes around 10 minutes.
- If you don’t find a perfect time slot, refresh the website as they’re dynamically updated.
- Star questions you’re not sure about and go back to them at the end. The exam software supports that.
- Review all the questions before finishing the exam. I accidentally didn’t select two answers I was sure about and realized when reviewing.
- Read questions carefully. It happens that a single word changes the meaning.
- Some answers can be deducted **only** by eliminating the wrong ones. Don’t forget about that.

## What exam provider should I choose? PearsonVUE or PSI?

I chose PearsonVUE as it was recommended to me. I had a positive experience and everything went smoothly. The registration was fast and it took around 10 minutes. I had no problems during the exam.

## How long do I need to wait for an exam after the purchase?

There are timeslots always available 24h a day 7 days a week. You can even take the exam in an hour.

## What happens before the exam?

You have to call in 30 minutes before the exam. You’ll be asked to take pictures of your room and show your ID for verification. It takes around 10–15 minutes. Make sure you call in 30 minutes before and don’t be late. Make sure to empty your desk.

## What happens during the exam?

You, your screen, and your voice are recorded all the time and you’re supervised by a proctor. They’re watching you all the time. You cannot eat nor drink. You cannot leave your computer during the exam.

## What happens after the exam?

If you pass, congratulations! You’ll receive a certificate in five business days. Your score will not be visible there. However, you’ll get information with the score in a separate document. Additionally, you get multiple benefits like 50% discount for the next exam, access to special lounges at AWS summit etc.

Most importantly, you’ve built a strong foundational knowledge about AWS and you’re ready to use it in real-world scenarios!

# Summary

That’s it! I hope this FAQ gave you a better understanding of why you’d consider attending this certification over the others and what are the possible ways of preparation.

One last thing. Keep in mind, that public cloud offering is growing fast and AWS is [leading in this area for 10 consecutive years already](https://aws.amazon.com/blogs/aws/aws-named-as-a-cloud-leader-for-the-10th-consecutive-year-in-gartners-infrastructure-platform-services-magic-quadrant/). It means it’s a good long-term investment for all IT experts no matter what your current role is.

If you have any questions, feel free to [get in touch on Twitter](https://twitter.com/bszwej) or write a comment. I’ll be extending this article over time as new questions arrive. Enjoy your day :)!
