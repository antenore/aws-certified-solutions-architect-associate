# AWS Storage Extras

## AWS Snow Family

* Portable devices (small -> huge) to
* Data migration
    * Snowcone
    * Snowball Edge
    * Snowmonile
* Edge computing
    * Snowcone
    * Snowball Edge

### Data migration

* Time to transfer. Transferring  > 10 TB takes time
* If it takes more then a week, better to use a Snow device.
* Delivered by post (or truck)

* Snowball Edge for data transfer
    * Big Box
    * TBs to PBs data in/out AWS
    * Pay per data transfer job
    * Block Storage and S3 compatible storage
    * Snowball Edge Storage optimized
        * 80 TB block storage or S3
    * Snowball Edge Compute Optimized
        * 42 TB block storage or S3
    * Large DC migration, DC decom, DRP.
    * Up to 15 nodes
* AWS Snowcone
    * Smaller (2.1 KG)
    * 8 TB
    * Space constrained environment.
    * Can be sent back or connected using AWS DataSync
* AWS Snowmobile
    * Huge truck
    * 100 PB each (more trucks till exabytes)
    * Temp controlled, GPS, 24/7 video surveillance
* You install AWS OpsHub on your PC to manage Snow Family devices.
* AWS console
    -> order (device, S3 bucket, EC2 AMI, SNS topic, OpsHub down)
    -> receive
    -> install
    -> use
    -> send back (or DataSync)
    -> Auto up to S3 bucket
    -> Wipe by AWS

### Edge Computing

* Process data while being created on an edge location
* Not connected or accessible areas, in the wild, IoT, etc
* Snowcone
    * 2 CPU, 4GB RAM, ETH/WIFI
    * USB-C power or Battery
* Snowball Edge - Compute Optimized
    * Up to 52 vCPU, 208 GiB RAM, optional GPU
    * 42 TB storage
* Snowball Edge - Storage Optimized
    * Up to 40 vCPU, 80 GiB RAM
    * Object storage clustering (up to 15 nodes)
* All can run EC2 and Lambda
* Saving plan for long term deployment 1 - 3 years

### AWS OpsHub

* Before AWS CLI to manage Snow, now a GUI you install on your PC/laptop

### Snowball to Glacier

* Cannot do it directly
* Snowball -> S3 -> S3 lifecycle policy -> Glacier

## Amazon FSx

* Fully managed for Lustre, Windows File Server, NetApp ONTAP
* To use 3rd parties file systems

### FSx for Windows File Server

* Like EFS but for Windows
* SMB and NTFS
* MS AD integration, ACLs, quotas
* Supported on Linux (can be mounted on)
* SDD/HDD
* Can Multi AZ
* Daily backup to S3

### FSx for Lustre

* Distributed FS
* L(inux)(c)lustre
* HPC, AI, Machine Learning, Video processing
* SDD/HDD
* Read/write to S3
* Scratch (temporary) or Persistent FS (replicated. long term, etc)

### Storage Gateway

* Hybrid cloud
* Storage Gateway to connect on-premises and AWS
    * File
        * NFS/SMB
        * S3 standard, S3 IA, S3 One Zone IA
        * IAM Roles
        * Local File Cache to reduce latency
        * Can be mounted
        * AD integration
    * Volume
        * Block storage iSCSI backed by S3 and EBS snapshots
        * Cached volume for low latency
        * Stored volumes: on premises, backup on S3.
        * App serv -> iSCSI -> V gateway -> HTTPS -> S3 Bucked <-> EBS Snapshots
    * Tape
        * Virtual Tape Library
        * Backup to Glacier
        * Works with backup vendors
* You can virtualize or buy a Storage Gateway Hardware Appliance on amazon.com
* Fx Storage File Gateway
    * Like File Gateway
    * Total integration with Windows
    * Local Cache to reduce latency

## AWS Transfer Family

* FTP
* FTPS
* SFTP
* Fully managed
* Pay per provisioned endpoints per hour + data transfer in GB
*

