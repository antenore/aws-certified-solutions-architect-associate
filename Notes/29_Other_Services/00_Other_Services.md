# Other AWS Services

## CICD (Continuous Integration and Delivery)

* Developers push code to the repository, CICD ruin tests and builds against the new code.
    * Orchestrate: AWS CodePipeline
        * Code: AWS CodeCommit
        * Build and Test: AWS CodeBuild
        * Deploy & Provision: AWS Elastic Beanstalk or AWS CodeDeploy + User-managed EC2 instance fleet (CloudFormation)
    * All of each components can be used separately (Pipeline included)

## CloudFormation, Infrastructure as Code

* Like Ansible, Puppet but for almost all AWS Services (most of all services)
* Manual
    * CloudFormation Designer
* Automated way
    * YAML template files (**resources**, parameters, output and others)
    * AWS CLI to deploy the templates

### CloudFormation StackSet

* CloudFormation for cross-account and regions.

### CloudFormation CreationPolicy

* You can associate the CreationPolicy attribute with a resource
* to prevent its status from reaching create complete until AWS CloudFormation receives a specified number of success signals
* or the timeout period is exceeded.
* To signal a resource, you can use the cfn-signal helper script or SignalResource API.
* AWS CloudFormation publishes valid signals to the stack events
* so that you track the number of signals sent.
* The creation policy is invoked only when AWS CloudFormation creates the associated resource.
* Now, the only AWS CloudFormation resources that support creation policies are
    * AWS::AutoScaling::AutoScalingGroup,
    * AWS::EC2::Instance,
    * and AWS::CloudFormation::WaitCondition.

Use the CreationPolicy attribute when you want to wait on resource configuration actions before stack creation proceeds. For example, if you install and configure software applications on an EC2 instance, you might want those applications to be running before proceeding. In such cases, you can add a CreationPolicy attribute to the instance, and then send a success signal to the instance after the applications are installed and configured.

## AWS Step Functions

* Server-less visual workflow to orchestrate Lambda Functions
* Using JSON state machine

## AWS Simple Workflow Service (SWF)

* Old, not advised by AWS, replaced by Step Functions except if
    * You need external signals to intervene in the process
    * If you need chil processes that returns values to the parent process

## Amazon Elastic Map Reduce (EMR)

* Helps to create Hadoop clusters (Big Data) (Presto, Spark, HBase, Hadoop)
* Hundreds of EC2

## AWS Opsworks

* = Managed Chef and Puppet

## AWS Workspaces

* DesktopsA
* Exam Tip: To replace on-premises Virtual Desktop Infrastructure (VDI)

## AWS AppSync

* Store and sync data across mobile and web apps in real-time
* GraphQL (mobile technology from Facebook)

## Cost Explorer

* Nothing new
