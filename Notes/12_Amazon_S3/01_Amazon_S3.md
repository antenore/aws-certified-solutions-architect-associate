# Amazon S3

* Amazon S3 is one of the main AWS services.
* "Infinitely scaling" storage.
* Widely popular.
* Used by Website as a backbone
* Many AWS services uses S3 (CloudWatch for instance).

## Objects and Buckets

* Buckets -> directories (almost), more a container for our objects. We don't have real folders on S3
    * Buckets must have a globally unique name
    * Buckets are defined at a regional level.
    * Naming convention:
        * No uppercase
        * No underscore
        * 3-63 chars long
        * Not an IP
        * Start with lower case letter or number.
* Objects -> files
    * Objects have a key
    * The key is the full path:
        * s3://my-bucket/my_file.txt (key: my_file.txt)
        * s3://my-bucket/foo/bar/my_file.txt (key: foo/bar/my_file.txt)
    * The key is the prefix + the object name.
        * prefix foo/bar/
        * object name: my_file.txt
    * there are no folders within buckets, but only keys that contains slashes.
    * The UI trick you to think otherwise.
    * Object have a body, Metadata, tags and eventually a Version ID:
        * Body:
            * Max object size is 5TB (5000 GB).
            * Max upload size is 5GB, for more use multi-part uploading.
        * Metadata:
            * List of key/value pairs (system or user metadata)
        * Tags:
            * Up to 10 Unicode key/value pair (useful for security/lifecycle).
        * Version ID if versioning is enabled.

## Versioning

* Be enabled at the bucket level.
* Using the same key will increment the version.
* It's best practice to version your buckets.
    * Protect form unintended deletes.
    * Easy roll back to previous version.
* Prior to versioning is enabled, the version is null
* Suspending versioning doesn't delete the previous versions.

## Encryption

* 4 Methods:
    * SS3-S3: Objects are encrypted using keys managed by AWS.
    * SSE-KMS: Objects are encrypted using your keys managed by Amazon KMS.
    * SSE-C: If you want to manage your own keys by yourself.
    * Client Side Encryption.
* Which method fit which situation? Important for the exam.

### SSE-S3

* Objects are encrypted using keys managed by AWS.
* Object is encrypted server side.
* AES-256 encryption algorithm
* Must set header: "x-amz-server-side-encryption":"AES256"
* Client -> HTTP/S + header -> S3 -> Encryption

### SSE-KMS

* Objects are encrypted using your keys managed by Amazon KMS (Key Management Service).
* KMS advantages: user control + audit trail
* Server side.
* Must set header: "x-amz-server-side-encryption":"aws:kms"

### SSE-C

* server-side encryption using keys fully managed by the customer.
* Keys are not stored by Amazon
* Must use HTTPS, encryption key is sent with the HTTP headers for each S3 request.
* Not available in the console, only through the AWS cli.

### Client Side Encryption

* The customer is responsible for encrypting and decrypt data.

### Encryption in transit (SSL/TLS)

* S3 exposes both HTTP and HTTPS endpoints.
* You are free to choose the one you want, HTTPS is recommended.

* Remember, HTTPS is required for SSE-C

## Bucket Policies

* User based
    * IAM policies - Allowed API calls for a specific user.
* Resource based
    * Bucket Polocoes - bucket-wide rules (S3 console) - allow cross account access.
    * Object ACL - finer grain on an object level.
    * Bucket ACL - less common, not in the exam.

* An IAM principal can access an S3 object if:
    * the user permissions set in IAM allow it, OR
    * The Resource policy allows it, AND
    * there isn't an explicit DENY.

* S3 Bucket policies are JSON basedA
    * Resources: can be bucket and objects
    * Actions: Set of API to be allowed/denied
    * Effect: Deny/Allow
    * Principal * or the account/user the policy apply to.
* Policy use cases:
    * Grant public access to the bucket.
    * Force objects to be encrypted during upload.
    * Grant access to another account (cross account)

## Bucket settings for Block Public Access

* There are settings to block public access to your bucket (just remember this, the rest is :-O )
* This is because there have been some data leaks on Amazon S3
* So Amazon decided to put in place these settings to strengthen the security,
* It can be set up at account level, to assure none of your buckets will be public by default.

## Other security options

* Networking:
    * VPC endpoints, i.e. for instances in VPC without internet)
* Logging and Audit
    * S3 Access logs stored on (another) S3 bucket
    * API calls can be logged with AWS CloudTrail
* User Security:
    * MFA Delete: MFA needed to delete objects
    * Pre-signed URLs: Expiring URL for limited amount of time (e.g. to download video for premium users)
        * **Limited amount of time ===> Pre-signed URLs**

## S3 Websites

* S3 can host static websites
* The URL can be (depending on the region may change, WHY???):
    * <bucket-name>.s3-website-<AWS-region>.amazonaws.com
    * <bucket-name>.s3-website.<AWS-region>.amazonaws.com
* If you get 403 error (forbidden) it's because of the wrong policy.

## S3 CORS - Cross Origin Resource Sharing

* Cross-Origin Resource sharing is https://example.com => https://foo.example.com (or foo.com)
* Web browser usually block these requests, except foo accept request using CORS headers.
* Access-Control-Allow-Origin and Access-Control-Allow-Methods (GET, PUT, DELETE, etc.)

* In S3 when we have two buckets (they have different origin), you need to setup CORS.
* 2 Buckets, 1st bucket index.html that access extra-page.html on 2nd bucket
* 2nd Bucket needs the CORS policy to accept from 1st bucket.

