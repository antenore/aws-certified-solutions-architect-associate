# AWS DataSync

* Move large amount of data from on-premises to AWS
* NFS/SMB to AWS (S3, ESF, FSx)
    * DataSync agent on on-premises connect to AWS DataSync endpoint ⇒ S3, AWS EFS, Amazon FSx).
* EFS to EFS (within AWS)
    * EC2 instance with DataSync Agent
    * → AWS DataSync Service endpoint → EFS

## Transferring large amount of data scenario

* 200 TB of data into AWS, we have a 100 Mbps internet connection
* Over Internet, Site-toSite VPN:
    * Immediate setup
    * 200(TB)*1000(GB)*1000(MB)*(Mb)/100 Mbps = 16000000s = 185 days.
* Over Direct Connect 1 Gbps
    * One-time set-up over one month
    * 200(TB)*1000(GB)*8(Gb)/1 Gbps = 1600000s = 18,5 days
* Over Snowball:
    * 2, 3 snowballs in parallel
    * About 2 weeks for end-to-end transfer
    * Can be combined with DMS
* For ongoing replication / transfer: Site-to-site VPN or DX with DMS or DataSync
