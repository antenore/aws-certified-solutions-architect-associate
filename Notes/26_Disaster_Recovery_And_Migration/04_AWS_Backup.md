# AWS Backup

* Fully managed service
* Automate backups across AWS services
* Supported services:
    * Amazon EC2 / Amazon EBS
    * Amazon S3
    * Amazon RDS, Amazon Aurora, Amazon DynamoDB
    * Amazon DocumentDB, Amazon Neptune
    * Amazon EFS, Amazon FSx (Lustre and Windows File Server)
    * AWS Storage Gateway (Volume Gateway)
* Cross-region backups
* Cross-account backups
* Support Point In Time Recovery
* On-demand and Scheduled backups
* Tag-based backup policy
* Backup policies → **Backup Plans**
    * Frequency (hours, daily, days, weekly, cron expression)
    * Backup window
    * Transition to Cold Storage (never, days, weeks, months, years)
    * Retention period (always, days, weeks, months, years)

## AWS Backup Vault Lock

* Enforce WORM (Write Once Read Many) for all the backups stored on AWS Backup Vault.
    * For security (avoid accidental delete)
    * Compliance, regulations
    * Even not root can delete the backup
