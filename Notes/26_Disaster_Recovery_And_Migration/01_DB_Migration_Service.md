# Database Migration Service (DMS)

* Quickly and secure.
* DB available during migration (using CDC)
* Continuous Data Replication using CDC
* Support:
    * Homogeneous migrations: like Oracle to Oracle.
    * Heterogeneous migrations: like M$ SQL Server to Aurora
        * Using AWS Schema Conversion Tool (SCT)
        * SCT is not needed with RDS (ex from PostgreSQL, to RDS PostgreSQL)
            * Best practice is to install SCT on premises (faster).
* Require an EC2 instance for the replication tasks.
* Install DMS on EC2.
* Sources Most DB including Azure SQL Database, DB2, etc.
* Target: Any DB supported by AWS.
* Types:
    * On-premises → AWS
    * AWS → AWS
    * AWS → On-premises.
