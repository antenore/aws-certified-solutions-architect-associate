# On-Premises strategies with AWS

Some useful notes

* We can download Amazon Linux 2 AMI as a VM (.iso format)
    * Import in KVM, VMWare, VirtualBox, Microsoft Hyper-V, etc
* VM import/export
    * Migrate on-premises VM into EC2
    * For DRP strategy.
    * Export back from Ec2 to On-premises.
* AWS Application Discovery Service
    * Gather information about on-premises servers to plan a migration
    * Server utilization and dependency mappings
    * Track with AWS Migration Hub
* AWS DMS
    * See previous slide
* AWS Server Migration Service (SMS)
    * Like DMS, but for servers (incremental replication)
