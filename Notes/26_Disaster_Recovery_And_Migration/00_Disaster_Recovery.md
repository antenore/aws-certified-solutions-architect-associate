# Disaster Recovery in AWS

* DRP general concepts apply.
* DRP types:
    * On-premises → On-premises, traditional DRP, expensive.
    * On-premises → AWS cloud: hybrid DRP
    * AWS Cloud Region A → AWS Cloud Region B
* RPO: Recovery Point Objective
    * How much of data you are willing to lose during your DRP
    * You restore to a certain point
* RTO: Recovery Time Objective
    * Downtime

```
               Data Loss              Downtime

   ┌──────────────────────────────────────────────────────────┐
   │    O                     ⚡                    O         ├──►
   └──────────────────────────────────────────────────────────┘
        ▲                     ▲                     ▲
        │                     │                     │
        │                     │                     │
       RPO                Disaster                 RTO
```

* The lower time of RTO and RPO, the higher the cost.

## Disaster Recovery Strategies

* Backup and Restore (High RPO)
    * Backup data every 24 hours, 1 week, etc (RPO)
    * You must recreate the VM, the services, and restore (high RTO).
    * It's cheaper.
* Pilot Light
    * A smaller version of the application is always running in the cloud
    * Faster than Backup and Restore.
    * For Critical systems.
* Warm Standby
    * Full system at minimum size
    * Upon disaster, we can scale it up.
* Hot Site / Multi Site Approach
    * Very low RTO (minutes or seconds), expensive.
    * Full production system running on both AWS and On-premises, or
    * Full production system running on multi-region setup.

```
                  Faster RTO
  ──────────────────────────────────────────►
  ╔═════════════════════════════════════════╗
  ║  Backup &     Pilot     Warm      Multi ║
  ║  Restore      Light    Standby    Site  ║
  ╚═════════════════════════════════════════╝
  ◄──────────────── AWS Multi Region ───────►
```
## Disaster Recovery Tips

* Backup
    * EBS Snapshots, RDS automated backups / Snapshots, etc. …
    * Push to S3, S3 IA / Glacier, Lifecycle Policy, Cross Region Replication
    * From On-premises Snowball or Storage Gateway
* High Availability
    * Route 53 to migrate DNS over from Region to Region
    * RDS Multi-AZ, ElastiCache Multi-AZ, EFS, S3
    * Site to Site VPN as a recovery from Direct Connect
* Replication
    * RDS Replication (cross-region), AWS Aurora + Global DatabasesA
    * DB replication from on-premises to RDS
    * Storage Gateway
* Automation
    * CloudFormation / Elastic Beanstalk to re-create a new environment
    * Recover or reboot EC2 instances with CloudWatch upon alarms.
    * AWS Lambda for custom automation.
* Chaos
    * i.e. Netflix has a "simian-army" randomly terminating EC2



