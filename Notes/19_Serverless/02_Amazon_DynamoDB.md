# Amazon DynamoDB

* Fully managed, high available with repl across multiple AZs
* Distributed
* NoSQL DB
* Massively scales.
* Millions of requests per sec
* Integrate with IAM
* Event driven programming with DynamoDB Streams
* Low cost and auto-scaling
* Standard and infrequent Access (IA) Table Class
* Encryption at rest

## Basics

* Made of Tables
* Each table has a Primary Key (one or 2 columns)
* Infinite number of items (rows)
* Each item can have attributes (can be null)
* Each item max 400KB
* Data Types
    * Scalar: String, Number, Binary, Boolean, Null
    * Document: String Set, Number Set, Binary Set

## Example

|  User_ID        | Game_ID        | Score          | Result         |
|  -------------- | -------------- | -------------- | -------------- |
|  jk45h64kj53h6  | 4421           | 92             | Win            |

* Primary Key
    * User_ID = Partition Key
    * Game_ID = Sort Key
* Attributes
    * Score
    * Result

## Read/write Capacity Modes

* Provisioned Mode (default)
    * number of reads/writes per second
    * Capacity planning in advance
    * Pay for **provisioned** Read Capacity Units (RCU) and Write Capacity Units (WCU)
    * Possibility to add auto-scaling for RCU and WCU
* On-Demand Mode
    * Reads/Writes automatically scale up/down
    * No capacity planning
    * Pay for what you use, more expensive
    * For unpredictable workloads

## DynamoDB Accelerator (DAX)

* Managed in-memory cache for DynamoDB
* To solve read congestion
* Microseconds latency for cached data
* You use it transparently, no API changes

## DynamoDB Streams

* Ordered stream of item-level modifications in a table
* Streams records:
    * Kinesis Data Streams
    * AWS Lamda
    * Kinesis Client Library applications
* Data retention up to 24 hours
* To react to changes in real-time (welcome email to users)
* Analytics
* Insert into derivative tables
* Into ElasticSearchA
* Implement cross-region replication

## DynamoDB Global Tables

* Make a table accessible in any region
* Streams is a requirement.

## DynamoDB Time To Live (TTL)

* Auto delete items after an expiry timestamp
* It's an attribute in the table

## DynamoDB indexes

* Allow to query based on other colums instead of the Primary Keys

## DynamoDB Transactions

* Write a transaction in multiple tables (or None)
