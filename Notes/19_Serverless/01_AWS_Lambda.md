# AWS Lambda

* Virtual **functions**
* Time limited exec (15 minutes)
* on-demand
* Automatic scaling
*

## Benefits

* Pricing
    * Pay per request and compute time
    * Free tier 100000 Lambda requests and 400000 GB-sec of compute time (400K per 1 GB of RAM)
    * ~$0.20 per 1M requests
    * After $1.00 for 600000 GB seconds.
    * Very, very cheap.
* Integration:
    * The whole suit of AWS services
    * Many programming languages supported
    * Cloud watch
* Scaling
    * Up to 10GB of RAM
    * Increasing RAM improve CPU and network

## Security

* AWS Lambda supports resource-based permissions policies for Lambda functions and layers.
* Resource-based policies let you grant usage permission to other AWS accounts on a per-resource basis.
* You also use a resource-based policy to allow an AWS service to invoke your function on your behalf.

## Lambda language support

* Node.js/javascript
* Python
* Java
* C# and Powershell
* Ruby
* Custom Runtime API (example Rust)

* Lambda Container ImageA
    * Run a container on Lambda
    * ECS / Fargate preferred for running Docker Images

## Main Integrations

* API Gateway
* Kinesis
* DynamoDB
* S3
* CloudFront
* CloudWatch Events EventBridge
* CloudWatch Logs
* SNS
* SQS
* Cognito

## Some examples

1. Create serverless thumbnail creation and store metadata in DynamoDB
    1.2. S3 -> new image (event) -> Lambda (2 jobs) -> 1. create the thumbnail, 2. update DynamoDB
2. Serverless cronjob
    2.1. CloudWatch Event EventBridge -> Trigger every 1 hour -> Lambda Function

## Lambda limits summerize

* Limits per region
* Execution:
    * Mem: 128 MB — 10GB
    * Max exec time 15 minutes (900 min)
    * 4 KB env vars
    * 512 MB disk space /tmp
    * 1000 concurrent (can be increased)
* Deployment
    * 50 MB max zip compressed size
    * 250 MB max size uncompressed
    * Can use the /tmp to load additional files at startup
    * 4 KB env vars

# Lamba@Edge

* To change CloudFront requests.
* TODO: ??? I didn't understand anything


