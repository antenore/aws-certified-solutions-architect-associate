# API GAteway

* Client -> REST API -> API Gateway -> Proxy request -> Lamba (example) -> CRUD -> DynamoDB (example)
* Lambda + API Gateway: No infra to manage
* Support WebSocket
* API versioning
* Different envs
* Handle Security (Auth and authorization)
* Create API keys, handle request throttling
* Swagger / Open API import to quikly define APIs
* Transform and validate requests and responses
* Generate SDK and API specs
* Cache API responses.

## Integrations:

* Lambda
* HTTP endpoints
* AWS services (AWS API through the API Gateway)
    * You can do whatever you like with the AWS services API

## Endpoint Types

* Edge-Optimized (default) For global clients
    * It uses CloudFront to route users
    * The API Gateway still lives in only one region
* Regional:
    * For clients within the same region
    * Can manually be combined with CloudFront
* Private:
    * Can only accessed from your VPC, using VPC endpoint (ENI)
    * Using a resource policy to define access.

# API Gateway - Security

* IAM Permissions
    * Create an IAM policy and attach to user/role
    * API Gateway verifies IAM permissions passed by the calling application
    * This is to provide access in your own infrastructure.
    * It uses "Sig v4" capability where IAM credential are in the headers
    * No additional costs
* Lambda Authorizer
    * A Lambda function verify the token passed in the header
    * Option to cache the result of authentication
    * When you have 3rd authentication type like OAuth, SAML, etc
    * Lambda will retunr an IAM policy for the user
* Cognito User Pool
    * Later in details
    * Full user lifecycle
    * API Gateway auto verify auth in Cognito
    * No custom implementaion needed, but
    * Doesn't provide authorizations!
    * Good to integrate with other authenticators, like Google, Facebook, etc


