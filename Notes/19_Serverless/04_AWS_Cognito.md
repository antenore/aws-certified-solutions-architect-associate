# AWS Cognito

* Provide users with an identity to interact with our applications.
* **Cognito User Pools** (CUP):
    * Sign-in functionality
    * Serverless DB of users for your mobile app.
    * emails, phone numbers + MFA
    * Can enable Federated Identities (FB, Google, SAML, …).
        * Do not confuse with Cognito Identity Pools federated Identity
    * Returns a JSON Web Token (**JWT**).
    * Can integrate with **API Gateway** for authentication
* **Cognito Identity Pools (Federated Identity Pools)**:
    * AWS credentials to users to access AWS resources.
    * Integrate with **Cognito User Pools** as identity provider.
    * How:
        * Log-in to federated indentity provider (FB, Google, Twitter, OpenID, SAML, or even **CUP**)
            * There's an option for anonymous access.
        * Get a temporary AWS credentials back from the FIP.
        * The AWS credentials has an IAM policy attached to it with the given permissions.
    * Example:
        * Provide temporary access to write to an S3 bucket using the Google Login.
* **Cognito Sync**:
    * Sync data from device to Cognito
    * It's deprecated and replaced by **AppSync**.
    * Store pref., conf., app state.
    * Cross device sync (iOS, Android, …)
    * Offline (sync when online)
    * **Requires FIP (Federated Identity Pool)**.
    * Each datasets up to 1MB and up to 20 datasets to sync.




