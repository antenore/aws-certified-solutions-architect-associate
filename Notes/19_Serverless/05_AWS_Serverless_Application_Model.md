# AWS SAM - Serverless Application Model

* Framework for developing and deploying serverless applications.
* YAML based
    * Lamda
    * DynamoDB
    * API Gateway
    * Cognito User Pools
* Devs can use SAM locally to test and debug Lamda, DynamoDB, API Gateway, Cognito User Pools.
* With SAM we can use **CodeDeploy** to deploy Lambda functions.

