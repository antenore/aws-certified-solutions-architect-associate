# What is serverless

* Anything that is remotely managed.
* You don't manage servers, but your code.
* Before limited to/as FaaS (function as a service => Lambda)

* Some AWS services:
    * Lambda
    * DynamoDB
    * AWS Cognito
    * AWS API Gateway
    * Amazon S3
    * AWS SNS & SQS
    * Fargate
    * AWS Kinesis Data Firehose
    * Aurora Serverless
    * Step Functions
    * … ?

## Serverless example



                          ,-.
                          `-'
                          /|\
          ┌──────────────  |  ─────────┐
          │               / \         L│
          │ static       R│           O│
          │ content      E│A          G│
          │              S│P          I│
          │              T│I          N│
          │               │            │
          ▼               ▼            ▼
       ┌────┐      ┌───────────┐    ┌───────┐
       │ S3 │      │API Gateway│    │Cognito│
       └────┘      └──────┬────┘    └───────┘
                          │
                          ▼
                     ┌────────┐
                     │ Lambda │
                     └────┬───┘
                          │
                          │
                          ▼
                    ┌──────────┐
                    │ DynamoDB │
                    └──────────┘
                                             ~ Serverless Example ~


