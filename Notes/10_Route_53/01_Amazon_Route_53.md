# Amazon Route 53

* High Available, scalable, fully managed and Authoritative DNS.
    * Authoritative = I (customer) can update the DNS records.
* Domain Registrar.
* Can Health Check my resources.
* The only AWS service with 100% availability SLA.
* Route 53 => 53 => default DNS port.

## Records

* How you route traffic for a domain.
* Each resord contains:
    * Domain/subdomain Name - e.g., example.com
    * Record Type - e.g. A, AAAA, TXT, etc
    * Value - e.g. IP address
    * Routing Policy - How route 53 answers to queries.
    * TTL - Time To Live - DNS Resolver cache timeout.
* Supported DNS record types:
    * (must know) A, AAAA, CNAME, NS
    * (advanced) CAA, DS, MX, NAPTR, PTR, SOA, TXT, SPF, SRV

## Record Types

* **A** - maps a hostname to an IPv4
* **AAAA** - maps a hostname to an IPv6
* **CNAME** - maps a hostname to an hostname
    * Must resolve to a valid A or AAAA record.
    * Can't create a CNAME record for the top node of a DNS namespace (Zone Apex). no: example.com, yes: www.example.com
* **NS** - Name servers for the Hosted Zone

## Hosted Zones

* Container for records that define how to route traffic to a domain and subdomains.
* Public Hosted Zones - Public domain names for internet, like example.com (internet)
* Private Hosted Zones - Private domain names for the company internal VPCs (appl1.company.internal) (intranet)
* $0.50 per month per hosted zone

## Registering a domain (Hands On)

### DNS, ELB, EC2 (Hands On)

* We create 3 instances,
* we create an ELB with the 3 instances in the group,
* we point the domain name to the ELB CNAME (or much better an Alias)

## TTL

* If TTL is 24 hours seconds, the client will cache the result for 24 hours.

## CNAME vs ALIAS

* CNAME:
    * Points a hostname to any other hostname. (app.mydom.com -> foo.any.com)
    * **Only for NON root domain** (aka some.mydom.com)
* Alias:
    * Points a hostname to an AWS resource (app.mydom.com -> foo.amazonaws.com)
    * **Works for ROOT domain and NON ROOT domain** (aka mydom.com)
    * Free of charge
    * Native Health Check capability.

### Alias Records.

* Maps a hostname to an AWS resource.
* It's an extention to the DNS functionality.
* Automatically detects changes in the resource's IP addresses.
* Unlike CNAME, it can be used for the top node of a DNS namespace (Zone Apex), e.g.: example.com
* Alias Record is always of type A/AAAA.
* TTL is set by AWS.
* Targets:
    * ELB.
    * CloudFront Distributions.
    * API Gateway.
    * Elastic Beanstalk.
    * S3 Websites.
    * VPC Interface Endpoints.
    * Global Accelerator
    * Route 53 record in the same hosted zone.
    * **You cannot set an Alias for an EC2 DNS name**

## Routing Policies

* **Nothing** to have with Routing
* DNS does not route traffic, it only answers to DNS queries.
* Route 53 supports these Routing Policies:
    * Simple
    * Weighted.
    * Failover.
    * Latency based
    * Geolocation.
    * Multi-Value Answer
    * Geoproximity using Route 53 Traffic Flow feature.

### Simple

* Typically route to only one resource (foo.mydom.com -> A)
* Can specify multiple A for the same record.
* If multiple values, client will randomly pick one (doesn't depend on the client strategy?)
* When Alias uses a Simple policy, only one resource can be specified.
* Can't be associated with Health Checks.

## Weighted

* % of requests that goes to each resource.
* Assign each record a relative weight
* DNS records must have the same name and type.
* Can be associated with Health Checks.
* Use cases: load balancing between regions, testing new application versions, …
* 0 % to a record will stop sending traffic to a resource.
* 0% to all records, then all records be returned equally.
* The sum up of all weights don't need to be 100.

## Latency based

* Redirect to the resource with the lower latency
* When latency for user is very important (games?).
* Latency is based on traffic between users and AWS regions.
* So it can happen that a German user is redirected to a US region.
* Can be associated with Health Checks

## Health Checks

* HTTP Health Checks only for public resources.
* Health Checks => Automated failover:

1. Those that monitor an end point (app, server, other AWS resources, etc)
2. HC that monitor other HC (Calculated HC).
3. HC that monitor CloudWatch Alarms (best, full control)

* HC are integrated with CloudWatch (CW)

### Monitor Endpoint

* ~15 global health checkers.
    * Healthy/Unhealthy Threashold, by default 3
    * Interval 30 secs (10 sec at higher cost)
    * HTTP, HTTPS, TCP supported.
    * Default, if > 18% health, the Endpoint is healthy, otherwise Unhealthy
* Check pass only with 2xx and 3xx status code.
* HC can be set to pass or fail based on the text of the first **5120** bytes of the response.
* Must configure router/firewall to allow incoming Route 53 requests.

### Calculated Health Checks

* Combine multiple HC into a single one.
* OR, AND, or NOT
* Up to 256 Child HC
* Set the threshold of how many HC have to pass to make the parent HC to pass.
* Usage: Perform website maintainance without causing all health checks to fail.

### Private Hosted Zones

* Route 53 health checkers are outside the VPC
* Cannot access **private** endpoints (private VPC or on-premises)
* To workaround this, we can create a CW Metric associated with a CW Alarm, afterwards an Health Check that check the alarm itself.

## Routing policies

### Failover, Active/Passive

* You use HC to check the availability of the active node (mandatory)
* Route 53 will route (NO, resolve) to the active instance
* Good for Disaster recovery as well. Or simple clusters (Active/Passive)

### Geolocation

* Different from Latency-based!
* Based on user location.
* Location by Continent, Country, State (if overlapping most precise will be used).
* You should create a Default record for no-match location.
* Scenarios: Localization (l10n), content restriction, load balancing, etc.
* Can be associated with HC.

### Geoproximity Routing Policy

* Route traffic based on the Geo location of users and resources.
* Shift more traffic to resources based on the defined bias (peso).
    * Expand (1 to 99) - More traffic to the resource
    * Shrink (-1 to -99) - less traffic to the resource.
    * Default is 0, US users will be sent equally to us-west-1 and us-east-1)
    * e.g. us-east-1 with a bias of 50 will get more traffic
* Resources can be:
    * AWS resources (specify AWS region)
    * Non-AWS resources (specify Latitude and Longitude).
* Using Route 53 Traffic Flow (advanced).

```

                          │
         ,__              │                                   _,
      \~\|  ~~---___      │       ,                          | \
       |      / |   ~~~~~~│|~~~~~| ~~---,                  _/,  >
      /~-_--__| |         │|     \     / ~\~~/          /~| ||,'
      |       /  \        │|------|   {    / /~)     __-  ',|_\,
     /       |    |~~~~~~~│|      \    \   | | '~\  |_____,|~,-'
     |~~--__ |    |       │|____  |~~~~~|--| |__ /_-'     {,~
     |   |  ~~~|~~|       │|    ~~\     /  `-' |`~ |~_____{/
     ╔══════════╗ '-------│-,      \----|   |  |  ,' ~/~\,|`
     ║ us-west-1║   |     │ |~~~~~~~|    \  | ,╔══════════╗
     ╚══════════╝   |     │ |       |     \_-~ ║us-east-1 ║
      ',BIAS: 0-----|-----│-+-------'_____/__--╚══════════╝
       '_   '\|     |     │|~~~|     |    |      BIAS: 50/
         \    |     |     │|   |_    |    /~~|~~\    \,/
          ~~~-'     |     │|     `~~~\___|   |   |    /
              '-,_  | ____│|          |  /   | ,-'---~\
                  `~'~  \ │           |  `--,~~~~-~~,  \
                         \│~\      /~~~`---`         |  \
                          │  \    /                   \  |
                          │   \  |                     '\'
                          │    `~'
                          │
                          ▼

     Based on the bias, more users on us-east-1

````

### Traffic flow

* Visual editor to manage complex routing decision trees.
* Simplify the process of creating and maintaining record.
* Configurations are saved as Traffic Flow Policy
    * Across multiple Hosted Zones
    * Support versioning.

### Multi-Value Routing Policies

* When routing traffic to multiple resources
* It returns multiple value/resources
* Can be associated with HC (returns value for healthy resources)
* Up to 8 healthy resources are returned for each Multi-Value query.
* It's not substitute for ELB.
* It' s the client that choose which one to use.

### Using a 3rd party registrar and Route 53 as DNS

* You register on Godaddy, OVH, etc
* You create an Hosted Zone in AWS
* You take note of the Route 53 Name Servers of your zone
* You set those NS on the Godaddy, OVH, etc console

