# Advanced Amazon S3 and Athena

## MFA-delete

* Only root can enable and only using the CLI.
* Versioning must be enabled.
* MFA required to:
    * Permanent delete object version.
    * Suspend versioning

## Default Encryption

* By default to enforce encryption for S3 objects you need a Policy
* You can set a default encryption.
* Bucket Policy take precedence to Default settings (you can override)

## S3 Access Logs

* Audit logs, web access logs, all access, etc.
* In a different Bucket (you need an additionali or logging loop will happen)

## S3 Replication (CRR, SRR)

* Cross and Same Region Replication
* Only new objects are replicated or use S3 Batch Replication
* Only delete marks are replicated, not permanent (version ID) to avoid malicious delete.
* No chaining, bucket1 -> bucket2 -> bucket3 ==> objs created on bucket1, not replicated on bucket3

## S3 pre-signed URLs

* To upload/download files suing pre-signed URLs, expire in 3600 secs by default, can be configured.
* Using CLI for Downloads (GET) URL
* Using the SDK for Uploads (PUT) URLs
* Download videos
* Upload profile pictures.
* So, your bucket is not publickly accessible, but signed-URLs allow to
* upload/doesnload specific objects.

## S3 Stoage Classes

* S3 Standard.
    * 99.99% Availability
    * Frequenltly acssed data
    * Low Latency, high throughput
    * Up to 2 concurrent facility failures.
    * Big Data, mobile & gaming, content distrib, …
* S3 Infrequent Access
    * Lower cost than S3 Standard, but cost for retrieving.
    * Infrequent access, but fast when needed.
    * S3 Standar-Infrequent Access (S3 Standard-IA)
        * 99.9% Availability
        * DRP, backups
    * S3 One Zone-Infrequent Access (S3 One Zone-IA)
        * Data lost when AZ destroyed.
        * 99.5% Availability
        * Secondary backup copies of on-premises data, or data you can recreate.
* S3 Glacier
    * Low cost for archiving and backup
    * Pay for storing and retrieval.
    * S3 Glacier Instant Retrieval
        * ms retrieval
        * min 90 days duration.
        * For quaterly accessed copies.
    * S3 Glacier Flexible Retrieval.
        * Expedited: 1 to 5 minutes.
        * Standard: 3 to 5 hours.
        * Bulk: 5 to 12 hours.
        * Min 90 days duration.
    * S3 Glacier Deep Archive
        * Standard: 12 hours
        * Bulk: 48 hours
        * Cheapest.
        * Minimum 180 days.
* S3 Intelligent-Tiering
    * Little monthly fee monitor and autro-tiering
    * Auto-move based on usage patterns.
    * No retrieval charges
    * Frequent Access tier, default
    * Infrequent Access tier, not accessed 30 days
    * Archive instant Access tier, n/a 90 days
    * Archive Access tier, (optional), 90 to 700+ n/a
    * Deep Archive Access tier (optional), 180 to 700+ n/a
* S3 Lifecycle rules
    * Moving between storage classes after X days.
    * Delete after X days
    * Rules by prefix (s3://.../mp3/*)
    * Rules by tag
* S3 Analityics
    * To help determine when to transition objects from Standard to Standard_IA
* S3 Performance
    * 3500 PUT/../DELETE per sec
    * 5500 GET/HEAD per sec
    * Very very high perf.
    * KMS can be a limitation
        * Quota per region (5500, 10000, 30000 req/s)
        * can be increased with Service Quota Console
    * Multi-Part upload parallelize to improve perf
    * S3 Transfer Acceleration, uses AWS edge locations to speed-up upload.
    * S3 Byte-Range Fetches
        * To improve speed fo downloads
* S3 Select and Glacier Select
    * Usie SQL to filter S3 objects server side.
    * Up to 400% faster, Up to 80% cheaper.

## S3 Events

* S3:ObjectCreated/Removed/Restored, etc --> SNS, SQS, Lamba Function
* e.g. To create thumbnails upon image upload.
* Seconds to minutes to deliver.
* Integrates with **Amazon EventBridge** --> Over 18 AWS services as destination

## Amazon EventBridge

* Advanced filtering (way better than S3 Select)
* Multiple destinations (over 18)

## S3 - Request Pays

* You can set the object requester to pay the download fee, instead of the Bucket owner
* The requester must be authenticated in AWS.

## Amazon Athena

* Serverless query (SQL) service for S3 analytic, built with Presto.
* 5$ / TB scanned.
* Scan CSV, JSON, ORC, Avro and Parquet files.
* ==> Reports and Dashboard can be generated and managed with A. QuickSight
* Compressed or tabular data cost less.
* To analyze, ELB, EC2, ClouTrail, etc log

## Glacier Vault Lock

* For compliance rules
* Write Once Read Many model (WORM)
* Cannot change data

