# VPC Endpoints

* Used to initiate a private connection from your AWS resources to the AWS managed services like CloudWatch, S3, etc.
* Powered by AWS PrivateLink
* They remove the need of IGW, NATGW, …, to access AWS services through their public IPs (money savings)
* In case of issues:
    * Check DNS resolution in your VPC
    * Chek route Tables that have to be set up manually.
* Interface Endpoint
    * With an ENI (private IP addresses)
    * must attach a SG.
    * Support most AWS services
    * You pay an hourly rate for every provisioned Interface endpoint.
* Gateway Endpoints
    * Provisions a gateway instead of an ENI
    * Must be used as the target of a route table
    * Supports S3 and DynamoDB
    * **No additional costs**, so it's often better than Interface Endpoint, except if you need a private IP
