# NAT Instance (**outdated**, but still relevant)

* Network Address Translation
* To allow EC2 instances in a private subnet to connect to internet
* NAT instance are placed i the public subnet, like the bastion host.
* Source/Destination Check must be **disabled** in the EC2 instance settings.
* Must have an **Elastic IP** attached to it.
* Finally we have to configure the Route Tables to route the traffic from the private subnet through tha NAT instance.
* There's a pre-configured Amazon Linux AMI, that **reached the end of standard support on December 31, 2020**
* They are to be managed like normal EC2 instances, so it's more complex than using other solutions.

* The preferred and new way to put in place NAT in AWS is using **NAT Gateways** (next section)
