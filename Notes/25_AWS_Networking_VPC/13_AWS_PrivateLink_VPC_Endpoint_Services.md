# AWS PrivateLink VPC Endpoint Services

* When Exposing services in your VPC to other VPC we have 3 options

1. Make it public (often unwanted or unsafe)
2. VPC Peering (complex, with more than 1 peering relations needed)
3. VPC Endpoint Services

## AWS PrivateLink (VPC Endpoint Services)

* Most secure and scalable way to expose a service to 1000s of VPC (own or other accounts)
* Does not require VPC peering, internet gateway, NAT, route tables and so forth.
* Requires a network load balancer (Application/Service VPC) and an ENI (Customer VPC), or a GWLB (uncommon).
* When the NLB and the ENI are in multi AZ, the solution is fault tolerant!
