# VPC Peering

* Privately connect two VPCs.
* They are not transitive. (A, B, C -> A + B, B + C, A + C)
* You have also to add Static Routes to allow connectivity between the VPCs
* Cross-account and cross-region peering is allowed.
* SG can reference peered VPC as well.
