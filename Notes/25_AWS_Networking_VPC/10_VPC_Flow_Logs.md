# VPC Flow Logs

* Capture info about IP traffic in the VPC
    * VPC Flow Logs
    * Subnet Flow Logs
    * Elastic Network Interface (ENI) Flow Logs
* Helps monitoring and troubleshooting connectivity issues
* Flow logs can go to S3 and/or CloudWatch Logs
* Captures network information from AWS managed interfaces too: ELB, RDS, ElastiCache, RedShift, WorkSpaces, etc
* You can analyze those logs with S3 + Athena (best) or ClouWatch Logs Insights
* srcaddr/dstaddr, srcport/dstport, Action (ACCEPT, REJECT)
* When we have an inbound ACCEPT and Outbound REJECT, it can only be a NACL issue ;-)
