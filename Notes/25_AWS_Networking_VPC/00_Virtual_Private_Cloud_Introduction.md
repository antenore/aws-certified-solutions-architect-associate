# Virtual Private Cloud (VPC) introduction

## Classless Inter-Domain Routing (CIDR) — IPv4

* A method for allocating IP addresses
* Used in Security Groups and AWS networking in general.
    * XX.YY.ZZ.WW/32 ⇒ one IP
    * 0.0.0.0/0 ⇒ all IPs
    * 192.168.0.0/26 ⇒ 192.168.0.0 – 192.168.0.63 (64 IP addresses)
* A CIDR have two components:
    * Base IP
        * 10.0.0.0, 192.168.0.0, …
    * Subnet Mask
        * How many bits can change in the IP
        * /0, /24, 32
        * Can take two forms:
            * /8 → 255.0.0.0        (last 3 octets can change)
            * /16 → 255.255.0.0     (last 2 octets can change)
            * /24 → 255.255.255.0   (last octet can change)
            * /32 → 255.255.255.255 (no octet can change)
        * 192.168.0.0/32 → 2^0 = 1 IP      → 192.168.0.0
        * 192.168.0.0/31 → 2^1 = 2 IP      → 192.168.0.0 – 192.168.0.1
        * 192.168.0.0/30 → 2^2 = 4 IP      → 192.168.0.0 – 192.168.0.3
        * …
        * 192.168.0.0/24 → 2^8 = 256 IP    → 192.168.0.0 – 192.168.0.255
        * …
        * 192.168.0.0/16 → 2^16 = 65536 IP → 192.168.0.0 – 192.168.255.255
        * 192.168.0.0/0  → All IPs         → 0.0.0.0     – 255.255.255.255
* Example:
    * What does this CIDR 10.0.4.0/28 correspond to?
    * /28 means 16 IPs (=2^(32-28) = 2^4), means only the last digit can change.

## Public vs Private

* Private IP ranges:
    * 10.0.0.0/8 — big private networks
    * 172.16.0.0/12 — Default AWS VPC range
    * 192.168.0.0/16 — e.g. home networks
* Public:
    * All the others.

## Default VPC overview

* All new AWS accounts have a default VPC
* New EC2 instances are launched in the default VPC, except if a subnet is specified.
* The default VPC has internet connectivity and all EC2 instances a public IPv4
* A public and private IPv4 DNS names.
