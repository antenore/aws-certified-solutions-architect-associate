# VPC — Traffic Mirroring

* Mirror all the traffic from one ENI to a Network Load Balancer
* It's almost always sent to EC2 instances where you have Security Appliance to analyse the traffic.
* It's good because it doesn't affect the source traffic.
* You can set a filter in the Traffic Mirroring.
* Content inspection, threat monitoring, troubleshooting, …
* It costs money per each sensor applied to each ENI.
