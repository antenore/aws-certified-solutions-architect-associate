# Direct Connect (DX)

* Dedicated **private** connection from your DC to your VPC.
* To set up between your DC and the AWS Direct Connect locations
* The VPC need a Virtual Private gateway
* It can connect to public (S3) and private (EC2) service using the same link.
    * All goes through the AWS Direct Connect Location.
* Use cases:
    * Higher throughput, so when working with large data sets, it'll lower the costs
    * Better connection than passing through the public network.
    * Good for Hybrid Environments (on prem + cloud)
* IPv4 + IPv6
* Customer Router/firewall → (AWS Direct Connect Location (Customer or partner router → AWS Direct Connect Endpoint)) → S3/EC2

## Direct Connect Gateway

* Used to connect to one or more VPC in Regions on the same account.

## Direct Connect — Connection Types

* **Dedicated Connections**: 1Gbps and 10Gbps capacity
    * Dedicated physical Ethernet port
    * Requested to AWS, completed by an AWS Direct Connect Partners
* **Hosted Connections**: 50Mbps, 500Mbps, up to 10Gbps
    * Request made via AWS Direct Connect Partners
    * You can adapt the capacity on-demand.
* To set up a link it often takes **more than one Month**.
    * In the exam if they want it fast, Direct Connect is the wrong answer.

## Direct Connect — Encryption

* Data in transit is not encrypted, but it's private.
* AWS Direct Connect + VPN provides an IPsec-encrypted private connection
* Good for an extra level of security, but slightly more complex to put in place.

## Direct Connect — Resiliency

* The best is to use separate connections (separated devices) on multi locations.
