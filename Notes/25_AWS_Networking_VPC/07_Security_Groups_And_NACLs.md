# Security Groups and NACLs

* NACL → SG → EC2

## SG

* Evaluated at instance level
* SG are statefull
    * Whatever enter, because the SG rules allowed it (like SSH connection), can also exit
    * So, when I connect with SSH, client has port 12345, server has port 22
    * The SG rule allow incoming connection to port 22
    * Automatically the server can send back traffic to the client port 12345, because the SG is staefull
* SG are allow rules only
* SG rules are all evaluated before to proceed

## NACL

* At subnet level
* NACLs are staless
    * Inbound and outbound rules are evaluated each time and separately
    * Therefore (it's just an example) we would need an outbound rule that allow traffic to the above client (port 12345)
    * By default, all outbound traffic (traffic to any destination) and inbound traffic (from any source) are allowed.
* NACL are like a firewall that controls traffic from and to a subnet.
* **One NACL per subnet**, multiple rules.
* A Default NACL is assigned by default when we create a subnet.
* All rules are composed as follows:
    * Rule number: Every rule is assigned a unique number. The rule’s priority is also based on the number it is assigned.
    * Type: This tells about the type of traffic, like SSH, HTTP, HTTPS.
    * Protocol: Protocol is a set of rules, that is applied to every request, ex: http, https, ICMP, SSH.
    * Portrange: The listening port, which takes in the request from the user, such as HTTP is associated with port 80.
    * Inboundrules: Also known as source. These rules talk about the source from where the request or traffic is coming from, and about the destination port/ the port through which the response is sent.
    * Outboundrules: Also known as destination. These rules talk about where the response should be sent and about the destination port.
    * Allow/Deny: Whether the specific traffic has to be allowed or denied.
* So remember, rules are evaluated in order, lowest to highest, first match win.
* Newaly created NACL will deny everything.
* NACL are great at blocking an IP at the subnet level.

## Ephemeral Ports

* The above mentioned client port 12345 is an Ephemeral Port.
* Windows 49152 – 65535
* Linux 32768 – 60999
* Therefore if we have two resources on different private subnets, we will have to allow the Ephemeral ports range (usually 1024-65535) on both subnet NACL

## VPC Reachability Analyzer to check if everything is fine.

* It's an AWS tool that just analyze your SG and NACL configuration to find connectivity issues.
* $0.10 for each analysis.
