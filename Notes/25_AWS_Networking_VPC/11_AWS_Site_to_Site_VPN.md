# AWS Site-to-Site VPN

* To connect a VPC to a private corporate data center.
* Virtual Private Gateway (VGW)
    * VPN concentrator on the AWS side.
    * The ASn can fbe customized
    * Created and attached to the VPC
* Customer Gateway (CGW)
    * Software application, physical device on the customer side.

## Exam tips

* What IP address to use
    * If the customer has a public IP, you use that one.,
    * Otherwise, on the customer side, you need to put in place NAT-T and provide the NAT-T public IP address
* **Important** Enable **Route Propagation** for the VPG in the route table that is associated with your subnets.
* Remember to allow ICMP in the inbound SG, if you need to ping EC2 instances from on-premises.

## AWS VPN CloudHub

* Low-cost hub-and-spoke model to allow communication between multiple sites and AWS through VPN
* We will have one VGW, multiple CGW
