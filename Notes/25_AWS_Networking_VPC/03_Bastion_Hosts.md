# Bastion Hosts

* The point of having a VPC is to have Private and Public subnets.
* We place our resources in the private subnet
* Therefore the users won't be able to connect to the EC2 instances from internet.
* To make it possible we put in place a Bastion host.
* The Bastion Host is in the public subnet of the VPC
* It can connect to the Private subnet, the users can connect to it.
* **The Bastion Host security Group must be wisely secured**
* **Exam Tip** Make usre the bastion host can only accept SSH (22) connection form specific IPs and not other Security Groups.
