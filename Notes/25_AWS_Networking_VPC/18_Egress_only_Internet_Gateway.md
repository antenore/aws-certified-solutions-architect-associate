# Egress-only Internet Gateway

* For IPv6 only
* like NAT Gateway but for IPv6
* You can connect to internet, but internet cannot connect to you.

So:

1. Internet Gateway → IPv4 and IPv6, bidirectional
2. NAT Gateway → [IPv4,](ipv4,.md) bidirectional
3. Egress-only -> IPv6, NO bidirectional.
