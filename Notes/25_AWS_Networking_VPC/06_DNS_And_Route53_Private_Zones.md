# DNS Resolution Options and Route 53 Private Zones.

* By default, when enableSndSupport is set to True (the default)
* DNS queries go to the Route 53 resolver at 169.254.169.253 or to the DNS reserved IP of the VPC (usually .2)
* You can also set up your own DNS oir use external DNS servers.
* DNS Hostnames (enableDnsHostNames)
    * By default,
        * True -> default VPC
        * False -> newly created VPC
    * Make sense only if enableSndSupport is True
    * It assign a public host name to the EC2 instances if they have a public IPv4
* Having both enabled we will have instances with a private DNS records, that may be really useful (like web.mycompany.private)
