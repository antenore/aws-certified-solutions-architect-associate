# Virtual Private Cloud (VPC) — Overview

* VPC = Virtual Private Cloud
* Soft limit of 5 VPC per AWS region
* Up to 5 CIDR per VPC
    * Min subnet size /28 (16 IP addresses)
    * Max subnet size /16 (65536 IP addresses)
* Only private IPv4 are allowed
    * 10.0.0.0/8
    * 172.16.0.0/12
    * 192.168.0.0/16
* **VPC CIDR should NOT overlap with your other networks (e.g. corporate)**

## VPC — Subnet (IPv4), reserved IPs

* AWS reserves 5 IP addresses in each subnet (first 4 IPs and last one)
* These IPs are not available for your use.
* Example, if CIDR is 10.0.0.0/24, the reserved IPs are
    * 10.0.0.0      — Network Address
    * 10.0.0.1      — Reserved by AWS for the VPC router
    * 10.0.0.2      — Reserved by AWS for mapping to AWS DNSs
    * 10.0.0.3      — Reserved by AWS for future use
    * 10.0.0.255    — Network broadcast. Not supported by AWS, so is reserved.
* Therefore, **important for the exam**:
    * If you need 29 IPs, you can't choose /27 (32 – 5 = 27, that is < 29)
    * You need /26 (64 – 5 = 59 that is > 29)

