# Transit Gateway

* When you have a complex network topology (hundreds or thousands of VPC), the Transit Gateway is the best solution
* Regional resource, can work cross-region.
* Share cross-account using Resource Access manager (RAM)
* You can peer Transit Gateway across regions.
* Route tables limit which VPC can talk with other VPC
* Works with Direct Connect Gateway and VPN connections
* **Exam tip**: Support **IP Multicast**, and it's the sole service that supports it.
* All the VPC, and on-premis can connect to the Transit Gateway simplifying the network design.
* **Exam tip**: If see ECMP (Equal-cost multi-path routing), or
    * Routing strategy to allow to forward a packet over multiple best path
    * Create multiple Site-to-Site VPN connections **to increase the bandwidth of your connection to AWS**
* Price: $$ per GB of TGW processed data.
