# Internet Gateway (IGW)

* Resources in a VPC needs an IGW to connect to internet.
* Automatically scales horizzontally, and it's high available and reduntant.
* Must be created separately from a VPC.
* One IGW per VPC
* You need to set up route tables explicitely to allow Internet access.
* Create VPC, create IGW, attach it to the VPC, create route table and assign the private/public subnets as needed.
