# NAT Gateways (NATGW)

* This solution replaces NAT Instances, that are not supported anymore.
* AWS-managed NAT, higher bandwidth, high available, no admin
* Pay per hour and bandwidth
* NATGW are created in a specific AZ, uses Elastic IP.
* Can't be used by EC2 instances in the same subnet of the NAT
* Requires an IGW
* Bandwidth ≥ 5 Gbps ≤ 45 Gbps
* No SG needed/required.

## NAT Gateway with High Availability

* NATGW are resilient within one AZ
* To have High Availability you need to create multiple NATGW in multiple AZs.
* The thing is, if you lose an AZ, you will lose also all the EC2 instances in that AZ
    * Therefore you need, anyway, multiple AZ, and multiple NATGW to have full fault tolerance.
    * It's not limited to NATGW!
* NATGW cannot be used as a Bastion Host, that is the only advange of NAT Instances (beside the cost eventually).
