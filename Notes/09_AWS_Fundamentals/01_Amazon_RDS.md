# RDS Overview

* [Introduction](#introduction)
* [RDS vs own DBs on EC2](#rds-vs-own-dbs-on-ec2)
* [RDS Backups](#rds-backups)
* [RDS Storage Auto Scaling (exam)](#rds-storage-auto-scaling-(exam))
* [RDS Read Replicas (vs Multi AZ)](#rds-read-replicas-(vs-multi-az))
  * [Read Replicas use cases](#read-replicas-use-cases)
  * [Read Replicas Network costs](#read-replicas-network-costs)
* [RDS Multi AZ (Disaster Recovery)](#rds-multi-az-(disaster-recovery))
* [RDS Security - Encryption](#rds-security---encryption)
  * [RDS Security - Encryption Operations](#rds-security---encryption-operations)
  * [RDS Security - Network and IAM](#rds-security---network-and-iam)

## Introduction

* RDS stands for Relational Database Service
* It's a managed DB service for DB that use SQL as a query language.
* With RDS you can create DB in the cloud fully managed by AWS.
    * PostgreSQL
    * MySQL
    * MariaDB
    * Oracle
    * Microsoft SQL Server
    * Aurora (AWS proprietary)

## RDS vs own DBs on EC2

* Automated provisioning and OS patching.
* Point in Time restore, continuous backup and restore to specific timestamps.
* Monitoring Dashboard.
* Read replicas for better read performance.
* Multi AZ setup for DR (Disaster Recovery).
* Maintenance windows for upgrades.
* Scaling capabilities (vertical and horizontal).
* EBS (gp2 and io1) storage back-end.
* BUT cannot SSH into the servers.

## RDS Backups

* Automatically enabled.
* Automated backups:
    * Daily full backup (during maintenance window)
    * Transactions logs backup every five minutes.
    * => You can restore to any point in time (up to five minutes).
    * 7 day retention (up to 35)
* DB Snapshots:
    * Manually triggered by the user.
    * Retention as longer as you wish.

## RDS Storage Auto Scaling (exam)

* You can enable Storage Auto Scaling.
* It increases automatically the storage when you run out of space.
* No need of manual intervention.
* You must set the **Maximum Storage Threshold** (max limit).
* It add space if:
    * Free storage is less than 10% of allocated storage.
    * Low-storage alarm last at least 5 minutes.
    * 6 hours have passed since the last modification.
* Useful for application with unpredictable workloads.
* Support all RDS database engines.

## RDS Read Replicas (vs Multi AZ)

* Up to 5 Read Replicas.
* Within AZ, Cross AZ, or Cross Region. (3 options, very important).
* ASYNC Replications, data may eventually be consistent.
* Replicas can be promoted to their own DB ( NO CAPISH!!! )

### Read Replicas use cases

* Reporting application
    * Production application point to prod DB.
    * Prod DB ASYNC to Read Replica
    * Reporting application read (SELECT) Read Replica DB.
    * The prod DB is unaffected.

### Read Replicas Network costs

* AWS charges for in-flight data between AZs
* Except for the managed services
* RDS is a managed service, so you DON'T pay for data sync.
* On the contrary for cross-region replicas you MUST pay the fee.

## RDS Multi AZ (Disaster Recovery)

* An RDS Master DB can have a **SYNC** replica to a stand-by DB
* One DNS name - Automatic failover to standby.
* Increase availability.
* No manual intervention
* Not used for scaling
* So, in the exam, can Read Replicas be setup as Multi AZ for DR? **Yes**.
* To do it, it's enough to `Modify` the DB and set Multi AZ, no downtime, no config.
    * Internally this happens:
        * A snapshot of the Master DB is taken.
        * Restored in another AZ.
        * And a SYNC setup between the Master and the Stand-by.

## RDS Security - Encryption

* At rest encryption
    * Option to encrypt the master and read replicas with AWS MKS (AES-256)
    * Done at creation time.
    * **If the master is not encrypted, the read replicas <u>cannot</u> be encrypted**
    * Transparent Data Encryption (TDE) available for Oracle and SQL Server (alternative to).
* in-flight encryption
    * SSL certificates to encrypt data to RDS in flight.
    * To enforce SSL:
        * PostgreSQL: `rds.force_ssl=1` in the AWS RDS Console (Parameter Groups).
        * MySQL: With a SQL select: `GRANT USAGE ON *.* TO 'mysqluser'@'%' REQUIRE SSL;`

### RDS Security - Encryption Operations

* Encrypting RDS backups
    * Snapshots of un-encrypted DBs are un-encrypted.
    * Snapshots of encrypted DBs are encrypted.
    * We can do an encrypted copy of an un-encrypted snapshots
* To encrypt an un-encrypted RDS DB:
    * Take a snapshot of the DB.
    * Do an encrypted copy of the snapshot.
    * Restore in a new DB from the encrypted snapshot.
    * Migrate the applications to use the new DB
    * Delte the old DB.

### RDS Security - Network and IAM

* Network Security
    * RDS DBs are usually deployed in a private network, not in the public.
    * RDS uses Security Groups, as EC2, by allowing/denying which IPs and other security groups can communicate with RDS.
* Access Management
    * There are IAM policies to allow to manage AWS RDS through the RDS APIs.
    * Username / Password to login into the DB.
    * Only for MySQL and PostgreSWQL, IAM-based authentication.
        * No password.
        * Token based authentication, obtained through IAM & RDS API calls.
        * Token lifetime of 15 minutes.
        * SSL is mandatory.
        * Users are managed centrally with IAM.
        * You can use IAM Roles and EC2 instance profiles for easy integration.


```
              API Call
   ┌───────┐  Get Auth Token  ┌─────────────┐
   │  EC2  │◄────────────────►│ RDS Service │
   └───────┘                  └─────────────┘
       ▲
       │ SSL encryption
       ▼ Pass Auth Token
   ┌───────┐
   │ MySQL │
   └───────┘
```


