# Amazon ElastiCache

* Redis is to managed Redis and Memcached as RDS is to managed Relational Databases.
* Caches are high performance in-memory, and low latency DBs.
* They reduce the DB load with high intensive workloads.
* Helps make your application stateless. (?)
* AWS is in chareg of OS maintenance and patching, optimizations, setup, configuration,
* monitoring, failure recovery and backups.
* **It requires some heavy changes in your application if it's not cache ready.**

## ElastiCache - Solution Architectures

## DB Cache.

* Apps query ElastiCache
* if data is not available, get from RDS, and
* store it in ElastiCache.
* Helps reducing the load in RDS.
* Cache must have an invalidation strategy to assure the most updated data.

## User Session Store (stateless application example)

* User logs-in into any applications in the stack.
* The application writes the session data to ElastiCache.
* The use switch to another application.
* The instance retrieve the user' session data from ElatiCache, so the user is still logged-in.

## Redis vs Memcached

* Redis
    * Multi AZ and/or Auto-failover (Multi AZ has Auto-failover).
    * Read Replicas to scale-out and high availability.
    * Data Durability using AOF (Append Only File) persistence.
        * In Redis usually is couple with RDB (Redis Database) persistence.
    * Backup and restore features.
        * Snapshots included + AOF and RDB persistence to have the maximum of restore.
* Memcahed
    * Multi-node for partitioning of data (sharding).
    * **No** High availability (replication).
    * **No** persitence.
    * **No** backup and restore
    * Multi-threaded architecture.

## Solution Architect special notes

### ElastiCache Security

* All caches in ElastiCache:
    * **Do not support IAM authentication.**
    * IAM policies on ElastiCache are only used for AWS API-level security.
* Redis AUTH:
    * Password or token based authentication a cluster creation time.
    * It's an extra security layer on top of security groups.
    * Support SSL in-flight encryption.
* Memcached
    * Support SASL-based authentication

### ElastiCache Patterns

* **Lazy Loading:** all the read data is cached, data can become **stale** in cache. Most typical.
* **Write Through:** Adds or update cache when written to a DB (**no stale data**).
* **Session Store:** store **temporary** session data in a cache (using TTL features).

### ElastiCache Redis Use Case

* Gaming Leaderboards.
* They are computationally complex.
* **Redis Sorted sets** guarantee both uniqueness and element ordering.
* For each new element added, it's ranked in real time and added in the correct order.


