# Amazon Aurora

## Introduction

* Proprietary DB from AWS.
* It's compatible with MySQL and PostgreSQL drivers.
    * When you create an Aurora DB you choose the edition (MySQL or PostgreSQL).
    * It means that you can easily migrate the two DB systems to Aurora without
    * changing anything in the DBs. AWS follows the PostgreSQL and MySQL updates to
    * keep the compatibility aligned. e.g. snapshot from MySQL to Aurora works oob.
* It's 'AWS cloud optimized', 5x faster vs RDS/MySQL, and 3x faster vs RDS/PostgreSQL.
* Storage grows automatically by 10GB increments, up to 128 TB.
* Max 15 reads replicas (vs MySQL 5), repl. faster (< 10ms lag)
* Instant failover (<30 secs). High Availability native.
* 20% more expensive than RDS (but much more efficient).
* Feature resume:
    * Automatic fail-over.
    * Backup and Recovery.
    * Isolation and security.
    * Industry compliance.
    * Push-button scaling.
    * Automated patching with zero downtime.
    * Advanced monitoring.
    * Routine Maintenance.
    * Backtrack: restore data at any point of time without using backups (snapshots).

## Aurora High Availability and Read Scaling

* Aurora create 6 copies of your data across 3 AZ:
    * 4/6 copies needed for writes.
    * 3/6 needed for reads.
    * Self healing with p2p replication.
    * Storage is striped across 100s of volumes (Shared storage Volume).
        * Replication, Self Healing + Auto Expanding.
* One Aurora instance takes the writes oprerations (master).
* Automated failoover for the master in less than 30 secs.
* Master + up to 15 Read Replicas serve reads operations.
* Support for Cross Region Replication.

## Aurora DB Cluster

```
                 ┌──────┐
                 │client│
                 └──┬─┬─┘
          ┌─────────┘ └─────────────┐
          │                         │
          ▼                         ▼
  ┌───────────────┐         ┌───────────────┐
  │Writer Endpoint│         │Reader Endpoint│
  └──────┬────────┘         └───────────────┘
         │                   ▲      ▲    ▲ ▲
         │              ┌────┘      │    │ └──────────────┐
         ▼              │           │    └─────┐          │
      ┌─────┐        ┌──┴──┐     ┌──┴──┐    ┌──┴──┐    ┌──┴──┐
      │     │    ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
      │  M  │    ┃   │  R  │     │  R  │    │  R  │    │  R  │    Auto Scaling ┃
      │     │    ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
      └──┬──┘        └─────┘     └─────┘    └─────┘    └─────┘
         │              ▲           ▲          ▲          ▲
         ▼              │           │          │          │
   ┌───────────────────────────────────────────────────────────────────────────────┐
   │                         Shared Storage Volume                                 │
   │                   Auto Expanding from 10G to 128 TB                           │
   └───────────────────────────────────────────────────────────────────────────────┘

```

## Aurora Security.

* Similar to RDS MySQL and PostgreSQL
* Encryption at rest using KMS.
* Automated backups, snapshots and replicas are also encrypted.
* Encryption in-flight with SSL.
* IAM token authentication (optionally).
* Security groups to protect the Aurora instance (your responsibility).
* You can´t SSH.

## Hands-On notes

* Choose the Aurora edition (MySQL or PostgreSQL)
* Provisioned, you choose and manage the instance type, or serveless (all automatic).
* Single or Multi master.
* Engine versions (e.g. more then 55 MySQL versions).
* Prod or dev/test templates (no free tiers)
* Admin username and password
* Instance type (t2.micro, etc)
* Multi-AZ (advised, more expensive) or not.
* The VPC
* VPC security group (port 3306)
* DB parameters.
* Backup retention (up to 35 days)
* Enable Backtrack (how many hours, e.g. 72)
* Maintenance window.
* We will end up with a cluster with the reader and writer endpoints.
* At this point we can add a cross-region replica and Auto-scaling
* Autoscaling: We can set a policy for Average CPU utilization for example (min, max
replicas).
* We can also add a region (with bigger EC2 instances only).
* To delete, reader -> master -> cluster

## Aurora Replicas - Auto Scaling

* We have one master
* Two reads replicas.
* There's an increase of CPU utilization.
* Auto Scaling will add, based on the policy, two more instances.
* The reader endpoint will automatically extend to includes the two new replicas.
* CPU should hopefully go down.

## Aurora - Custom Endpoints

* when you need different subset of replicas instance type you can set-up custom
endpoints
* For example, one custom endpoint for extra large EC2 instances (db.r5.2xlarge)
* This can be used for high load analytical queries.
* And another custom end point for large EC2 instances (db.r3.large)
* This can be used by the normal application.

## Aurora Serverless

* Automated database instantiation and auto-scaling based on actual usage.
* Good for infrequent, intermittent or unpredictable workloads.
* No capacity planning needed.
* Per per second, can be more cost-effective.
* Behind the scenes, instead of an end-point, the client connect to a Proxy Fleet
(managed by Aurora).

# Aurora Multi-Master

* if you want immediate Master fail over (High availability).
* Each node does R/W, versus promoting a RR as the new master that is done manually.
* Doing the promotion may take up 30 seconds(?).

# Global Aurora

* Aurora Cross Region Read Replicas:
    * Good for DRP
    * Easier to put in place.
* Aurora Global Database (recommended):
    * One Primary region (R/W).
    * Up to 5 secondary regions (RO), < 1 sec lag.
    * Up to 16 Read RTeplicas per secondary region (max 16 * 5)
        * It helps decreasing latency.
    * We can promote a secondary region to primary in case of DRP.
        * RTO (Recovery Time Objective) < 1 minute.

## Aurora Machine Learning

* You can add ML-based predictions to your apps via SQL.
* Integrations between Aurora and AWS ML services.
* Supported services:
    * Amazon SageMaker (use with any ML model in the backend)
    * Amazon Comprehend (sentiment analysis).
* No need to have ML experience.
* Uses: fraud detection, ADS targeting, sentiment analysis, product recommendations.
* Client -> SQL (recommend product?) -> Aurora -> AWS ML services -> Aurora -> client

