# Amazon Inspector

* Automated Security Assessments (continuos scanning)
* Only for:
    * For EC2 instances:
        * Using AWS System Manager agent (SSM)
        * unintended netowrk accessibility
        * running OS against known vulnerabilities (CVE)
    * Containers
        * Assessment of containers as they are pushed to Amazon ECR (CVE)
* Reporting and integration with AWS Security Hub
* Send findings to Amazon Event Bridge
* You get a risk score for prioritizing discovered vulnerabiliites
