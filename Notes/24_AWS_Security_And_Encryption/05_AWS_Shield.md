# AWS Shield

* AWS Shield Standard:
    * Basic DDoS protection
        * SYN/UDP Floods, Reflection attacks, and other layer 3/4 attacks.
    * Free Service activated by default
* AWS Shield Advanced
    * $3000 per Month per organization
    * Protect against more sophisticated attacks on EC2, ELB, CloudFront, AWS Global Accelerator, and Route 53
    * 24/7 AWS DDoS response Team (DRP) support.
    * Protect against higher fees during usage spikes caused by DDoS.
