# Cloud HSM

* KMS → AWS manages the software for encryption
* CloudHSM → AWS provides encryption Hardware
* HSM = Hardware Security module
* You entirely manage your encryption keys.
* FIPS 140-2 Level 3 compliance
* Symmetric and Asymmetric encryption
* No free tier
* Redshift supports CloudHSM for DB encryption and key management.
* You need the CloudHSM Client Software
* Good option if you need **SSE-C encryption**
* SSL/TLS acceleration, Oracle TDE acceleration
* You create users and manage permissions.
* CloudHSM is the **only option** if you want to **import On-premises Asymmetric Keys**.
* MFA support
