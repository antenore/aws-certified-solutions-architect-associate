# AWS Key Management Service (KMS)

* Encryption for AWS → KMS
* Easy access to data, fully managed keys by AWS
* Fully integrates with IAM (who can deal with which keys, etc)
* Seamlessly integrates with all AWS services that may need encryption
* Can be used with CLI and SDK as well.

## KMS — Customer Master Key (CMK) Types

* Symmetric (AES-256 keys)
    * First AWS offering
    * One single encryption key to encrypt and decrypt (that's why symmetric)
    * AWS services integrated with KMS uses Symmetric CMKs
    * Needed for Envelop Encryption
    * You cannot access to the unencrypted key, must call KMS API to use
* Asymmetric (RSA & ECC key pairs)
    * Public for encryption and Private to decrypt
    * Public key is accessible, private key not
    * Use case: outside of AWS by users who can't call the KMS API.

## AWS KMS (Key Management Service)

* To fully manage the keys and policies:
    * Create
    * Rotation policies
    * Disable
    * Enable
    * ( Revoke? )
* To audit key usage (using CloudTrail).
* Three types of Customer Master Keys:
    * AWS Managed Service Default CMK: free
    * User created Keys in KMS: $1 / Month
    * User imported Keys (256 symmetric keys only): $1 / Month
* + pay for API call to KMS ($0.003 / 10000 calls).

## KMS — When, Why, How

* Anytime you need to share sensitive information
    * Database passwords.
    * Credentials to external services
    * SSL certificates Private Key
* The added value is that the CMK used to encrypt data can never be retrieved by the user.
* Additionally CMK can be rotated for extra security.
* With KMS you never store secrets in plain-text, especially in your code.
* KMS can only encrypt up to 4KNB of data per call
* If >4KB use envelope encryption
* KMS security:
    * Make sure the Key Policy allows the user to access KMS
    * Make sure the IAM Policy allows the API calls
* Keys are bound to a region, so you cannot decrypt data across regions
    * When you copy a snapshot from on region to another, specify to use a new KMS key

## KMS Key Policies

* Like S3 bucket policies
* Default KMS Key Policy:
    * Created when you don't provide one.
    * full access for the root user (full AWS account)
* Custom KMS Policy:
    * Define users, roles that can access the KMS key
    * Define can administer the key.
    * Useful for cross-account access of your KMS key.

## KMS Key Rotation

* Only for **Customer-managed CMK** (NOT for AWS managed CMK)
* Automatic:
    * When enabled the automatic key rotation is done every 365 days.
    * Previous key is kept active to decrypt old data
    * New key has the same CMK ID, only the backing key is changed.
* Manual:
    * When you want to rotate keys every 90, 180, …, etc. days
    * The new Key will have a **different CMK ID**
    * Keep the old Key active to decrypt old data
    * Better to use aliases in this case, so that it will be transparent for the application.
    * Good solution to rotate CMK that are not eligible for automatic rotation (like Asymmetric CMK)
