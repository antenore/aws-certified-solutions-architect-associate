# AWS WAF — Web Application Firewall

* Layer 7 (HTTP), protect against common web exploits
* For, → **Application Load Balancer**, **API Gateway**, **CloudFront** ←
* We can define Web ACL
    * Rules can include:
        * IP addresses
        * HTTP headers, HTTP body, or URI strings
    * Protect from common attack, like SQL injection, and Cross-Site Scriptiong (XSS)
    * Size constraints
    * geo-match (Block countries)
    * Rate-based rules (count of event occurrencies) for DDoS protection.

