# AWS Firewall Manager

* Centraly manage all rules in all accounts in your Organization
* Common set of security rules.
* WAF Rules (ALB, API Gateway, CloudFront)
* AWS Shield Advanced (ALB, CLB, Elastic IP, CloudFront)
* Security Groups for EC2 and ENI resources in VPC

