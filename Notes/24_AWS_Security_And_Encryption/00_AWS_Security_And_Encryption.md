# AWS Security And Encryption

* Really important for the exam.

## Overview

* Encryption in flight (SSL)
    * Prevents MITM (Man In Then Middle attacks)
* Server side encryption at rest
    * Data encrypted by the server upon reception.
    * Prevent that someone having access to the server could read the data.
    * The server needs access to the key.
    * Usually coupled with SSL to prevent MITM.
* Client side encryption
    * Could leverage Envelope Encryption (see later)
    * Data is encrypted and decrypted by the sending and receiving clients.
    * The server cannot decrypt the data.
