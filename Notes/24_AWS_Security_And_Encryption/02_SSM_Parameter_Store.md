# SSM Parameter Store

* Secure storage for configuration and secrets
* Optional seamless KMS integration
* Serverless, scalable, durable, easy SDK
* Configurastions and secrets versioning
* Configuration management using paths and IAM
    * /my-path/
        * /my-app/
            * /dev/
                * db-url
                * db-password
            * /prod/
                * db-url
                * db-password
    * /aws/reference/secretsmanager/secret_ID_in_Secrets_Manager
    * /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2
    * …
* CloudWatch Events
* CloudFormation integration
* In the advanced parameters you can set a TTL and assign multiple policies for each secret.
