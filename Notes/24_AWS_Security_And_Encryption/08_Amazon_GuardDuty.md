# Amazon GuardDuty

* Intelligent Threat discovery
* Uses Machine Learning algorithms, anomaly detection and 3rd party data.
* One click to enable, 30 days Trial, no need to install software
* Input:
    * CloudTrail Event Logs
    * VPC Flow Logs
    * DNS Logs
    * (EKS) Kubernetes Audit Logs
    * …
* Can set up CloudWatch Event rules to be notified in case of findings.
* Target can be AWS lambda, or SNS
* **Can protect against CryptoCurrency attaccks** with a dedicated findings (maybe in the exam)


