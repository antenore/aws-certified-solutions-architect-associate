# AWS Secrets manager

* SSM successor with the solely purpose of saving Secrets.
* Has the capabability to **rotate** secrets every X days.
* Automation for generating secrets upon rotation (with Lambda)
* Integrates with Amazon RDS (MySQL, PostgreSQL, Aurora)
* Encrypted with KMS.
* Mostly meant for RDS.
* $0.40 per secret per Month
* $0.05 per 10000 API calls
* 30-day free trial
