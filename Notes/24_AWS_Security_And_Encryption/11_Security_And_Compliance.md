# Security and compliance

## AWS Shared Responsability Model

* AWS responsibility — Security **of** the Cloud
    * Infrastructure
    * Managed Services like S3, DynamoDB, etc
* Customer responsibility — Security **in** the Cloud
    * EC2 instance guest OS management, including OS patches, network config, IAM
    * Encryptiong data
* Shared controls:
    * Patch management, Configuration Management, Awereness and Training
