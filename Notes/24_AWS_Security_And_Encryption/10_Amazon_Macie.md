# Amazon Macie

* Amazon Macie is a fully managed data security and data privacy service.
* It uses machine learning and pattern matching to discover and protect sensitive data in AWS.
* Like personal identifiable information (PII).
* It uses CloudWatch Events or EventBridge to notify.
* Example: S3 Buckets ← analyze ← Macie → notify → CloudWatch Events
