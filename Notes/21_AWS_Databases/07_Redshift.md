# Redshift — Solution Architect Perspective

* Based on PostgreSQL, but it's not OLTP
* Online Analitycal Processing, OLAP, analytics, data warehouse
* 10x better than other warehouse, scale to PBs
* **Columnar** based, instead of row
* Pay as you go, instances provisioned.
* SQL interface
* Integrates with BI Tools like AWS Quicksight or Tableau.
* Data loaded from S3, DynamoDB, DMS, other DBs
* 1 to 128 nodes, up to 128 TB per node
* Leader Node (query planning, results aggregation)
* Compute node (queries, send results to leader)
* Redshift Spectrum, queries directly in S3 without loading data
    * Cool thing to use 100s/1000s Redshift Spectrum Nodes to query in real time S3 data
    * e.g. 1 Leader node, 3 compute nodes, 1000 Spectrum nodes (n° of spectrum is transparent)
* Backup and restore
* Sec with VPC, IAM, KMS and Monitoring
* Redshift Enhanced VPC routing, copy/unload through VPC (not going to public internet)
* No Multi-AZ
    * using snapshot to DRP
* To load data usually through Amazon Kinesis Data Firehose, S3, or using JDBC from an EC2

Remember the 5 Pillars of the **AWS Well-Architected Framework**

* **Operations**:
    * Like RDS
* **Security**:
    * Like RDS
* **Reliability**:
    * Auto healing, cross-region snapshot copy
* **Performance**:
    * 10x vs other warehouse, compression
* **Cost**:
    * Pay per node provisioned, 1/10th of other warehouse solutions.

** Redshift vs Athena**: faster queries, joins and aggregations.

**Redshift = Analytics, BI, Data warehouse**
