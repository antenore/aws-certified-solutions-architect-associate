# Amazon Neptune — Solution Architect Perspective

* Fully managed **graph** database.
* High relationship data.
* Social networking, FOAF, comments likes, relationships (FB, Twitter, etc)
* Knowledge graphs (Wikipedia)
* High available across 3 AZ, with up to 15 read replicas
* Point in time recovery, continuos backup to S3
* KMS encryption at rest + HTTPS

Remember the 5 Pillars of the **AWS Well-Architected Framework**

* **Operations**:
    * Similar to RDS
* **Security**:
    * IAM, VPC, KMS, SSL + IAM authentication
* **Reliability**:
    * Multi AZ, clustering
* **Performance**:
    * Best for graphs, clustering
* **Cost**:
    * Similar to RDS, pay per provisioned resources.
