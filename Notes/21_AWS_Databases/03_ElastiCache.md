# ElastiCache ̣— Solution Architect Perspective

* Managed Redis or Memcached
* Similar offering as RDS but for cahes.
* In-memory data store
* sub-ms latency.
* Redis cluster support, and (all) Multi AZ, sharding (Read Replicas)
* IAM, Security Groups, KMS, Redis Auth
* Backup, snapshots, Point in time restore.
* Managed and Scheduled maintenance.
* Monitoring through CloudWatch
* UC: Key/value, Frequent reads, less writes, cache DB queries result, website session data. No SQL

Remember the 5 Pillars of the AWS Well-Architected Framework

* **Operations**:
    * Same as RDS
* **Security**:
    * AWS responsible for OS. We -> KMS, SG, IAM, users (Redis Auth), SSL
* **Reliability**:
    * Clustering, Multi AZ
* **Performance**:
    * Sub-ms performance, in memory, read replicas for sharding, very popular cache option
* **Cost**:
    * Pay per hour based on EC2 and storage usage.

