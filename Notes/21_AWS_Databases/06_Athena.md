# Athena — Solution Architect Perspective

* Serverless with SQL capabilities.
* SQL layer on top of S3
* Pay per query
* Results back to S3
* IAM
* UC: One tie SQL queries, serverless queries on S3, log analytics


Remember the 5 Pillars of the **AWS Well-Architected Framework**

* **Operations**:
    * No operations
* **Security**:
    * IAM + S3 security
* **Reliability**:
    * Managed service, Presto engine, high available
* **Performance**:
    * scales based on data size
* **Cost**:
    * Pay per query / per TB of data scanned, serverless
