# RDS - Solution Architect Perspective

* Managed PostegreSQL, MySQL, Oracle, SQL Server
* <ust provision EC2 and EBS
* Read Replicas and Multi AZ
* Security by IAM, SG, KMS, SSL in transit
* Backup, Snapshots and Point in time restore
* Managed and Scheduled maintenance
* CloudWatch
* RDBS, SQL queries, transactional inserts, update and deletes, joins, etc

Remember the 5 Pillars of the AWS Well-Architected Framework

* **Operations**:
    * Small downtime during failover.
    * Maintenance -> read replicas
    * Restore EBS implies manual intervention
    * Application change may be needed to adapt to the DB and architecture.
* **Security**:
    * AWS -> OS security
    * We -> KMS, Security Groups, IAM policies, DB users, SSL
* **Reliability**:
    * Multi AZ feature,
    * failover in case of failures.
* **Performance**:
    * EC2 type, EBS volume type
    * Can add Read Replicas.
    * Storage auto-scaling
    * Instance manual scaling
* **Cost**: Pay per Hour based on provisioned EC2 and EBS
