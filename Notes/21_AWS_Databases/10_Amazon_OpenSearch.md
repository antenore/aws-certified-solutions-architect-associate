# Amazon OpenSearch — Solution Architect Perspective

* **OpenSearch = Search / Indexing**
* Previously ElasticSearch
* To be used as a complement to another DB
* e.g. DynamoDB search only indexed and primary+secondary keys,
* OpenSearch can do it for all records
* Usd for Big Data apps.
* Can be provisioned as cluster
* Integrations: Kinesis Data Firehose, AWS IoT, Amazon CloudWatch Logs
* Sec: Cognito and IAM, KMS, SSL, VPC
* OpenSearch Dashboards for visualization


Remember the 5 Pillars of the **AWS Well-Architected Framework**

* **Operations**:
    * like RDS
* **Security**:
    * Cognito and IAM, KMS, SSL, VPC
* **Reliability**:
    * Multi-AZ, clustering
* **Performance**:
    * based on ElasticSearch, petabyte scale
* **Cost**:
    * Pay per provisioned node, like RDS
