# AWS Glue — Solution Architect Perspective

* ETL — Extract, Transform and Load fully serverless and managed service
* Prepare and transform data for analysis
* e.g. From S3 or RDS, trhough Clue ETL, to Redshift
* Glue Data Catalogue
    * Catalogue of datasets (metadata of databases)
    * AWS Glue Data Crawler, crawls data on many DBs (S3, RDS, DynamoDB, JDBC)
    * DB structure is saved as metadata in Glue Data Catalog
    * Glue Jobs do ETL
    * Glue Data Catalogue can do Data Discovery also on Athena, RedShift, EMR
    * Once cataloged, data is immediately searchable, queryable, and available for ETL.
* Glue DataBrew: Simplify data exploration with pre-sets, no need to write code. For data engineers.
* AWS Glue Elastic Views, for developers to use SQL
* AWS Glue Studio, an IDE like tool to simplify the ETL workflows.


Remember the 5 Pillars of the **AWS Well-Architected Framework**

* **Operations**:
    * AWS Glue is serverless, so there are no compute resources to configure and manage.
* **Security**:
    * IAM, KMS, SSL, CloudTrail, Simple Notification Service
* **Reliability**:
    * Auto Scaling
* **Performance**:
    *
* **Cost**:
    * Crawlers and ETL Jobs, hourly rate pay per second model.
    * Catalogue, only monthly fee for storing and accessing metadata. 1st 1M records and 1st 1M access in the free tier
    * DataBrew, interactive sessions are billed per session, and the DataBrew jobs are billed per minute.
