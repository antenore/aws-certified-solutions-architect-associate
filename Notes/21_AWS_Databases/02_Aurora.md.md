# Aurora - Solution Architect Perspective

* Compatible with PostgreSQL or MySQL
* 6 replicas across 3 AZ
* Auto healing
* Multi AZ, Auto Scaling Read Replicas
* Read Replicas can be global
* Aurora can be global for DRP or latency improvements.
* Auto-scaling from 10GB to 128TB
* We can choose the EC2 instance Type
* Same security as RDS
* **Aurora Serverless** - for unpredicable or intermittent workloads
* **Aurora Multi-Master** - writes failover
* So, like RDS + less maintenance, more flexibility and more performance.

Remember the 5 Pillars of the AWS Well-Architected Framework

* **Operations**:
    * Less operations, auto scaling storage
* **Security**:
    * Same as RDS (KMS, SSL, SG, IAM, DB users)
* **Reliability**:
    * Multi AZ
    * High available
    * Serverless option
    * Multi-master option
* **Performance**:
    * 5x time faster then PostgreSQL/MySQL
    * Up to 15 Read Replicas (vx 5 for RDS)
* **Cost**:
    * Pay per hour based on EC2 instances and storage. Less than Oracle DB
