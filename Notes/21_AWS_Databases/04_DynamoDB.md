# DynamoDB — Solution Architect Perspective

* AWS proprietary tech, managed NoSQL DB
* Serverless, provisioned auto scaling, on demand capacity.
* Can replace ElastiCache as key/value store (e.g. for session data)
* High available, Multi AZ by default, decoupled Read and Writes, DAX for read cahce.
* Consistent or strongly consistent (TODO: review)
* authentication and authorisation through IAM
* DynamoDB Streams to integrate with AWS Lambda
* Backup, and restore. Global Table feature (only with DynamoDB Streams)
* Monitoring thorugh CloudWatch
* Can query only on primary, secondary key and indexes
* UC: Serverless apps development, small documents (up to 400KB), distributes serverless cache, No SQL, transactions capabilities.

Remember the 5 Pillars of the **AWS Well-Architected Framework**

* **Operations**:
    * No operations, auto-scaling, serverless
* **Security**:
    * Full IAM, KMS encryption, SSL in flight
* **Reliability**:
    * Multi AZ, Backups
* **Performance**:
    * one digit ms perf, DAX for caching, no perf downgrade with scaling.
* **Cost**:
    * Pay per provisioned capacity and storage, no need of capacity planning if auto-scaling
