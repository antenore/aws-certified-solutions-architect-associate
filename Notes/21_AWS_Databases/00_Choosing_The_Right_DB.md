# Choosing the right DB

* Read, write-heavy? Balanced? Throughput? Scaling?
* How muchh data?
* For how long?
* Average object size?
* How they are accessed?
* Durability?
* Latency?
* How many concurrent users?
* Data model?
* How to query data? Joins? Structured? Semi-structured?
* Strong schema? Flexibility? Reporting? Search? RDMS or NoSQL?
* License costs?
* Migrating from on-premises? Aurora?

## Database Types

* **RDBMS (SQL / OLTP)**: RDS, Aurora -> joins, websites, normalize data, relationships, etc
* **NoSQL DB**: DynamoDB (JSNON), ElastiCache (key/value pairs), Neptune (graphs) -> no joins, no SQL
* **Object Store**: S3 and S3 Glacier
* **Data Warehous (= SQL Analytics / BI)**: Redshift (OLAP, online analytical processing), Athena (Query S3)
* **Search**: ElasticSearch (JSON) — free text, unstructured searches.
* **Graphs**: Neptune — displays relationship between data.
