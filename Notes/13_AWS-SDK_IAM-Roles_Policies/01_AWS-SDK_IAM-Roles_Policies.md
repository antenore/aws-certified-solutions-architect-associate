# AWS IAM Roles, Policies, SDK, CLI, etc

* With policy we can fine grain access rights to all resources on AWS
* Mainly used for API calls, but also for any other needs.
* Pololicy Visual Editor, import JSON
* Amazon Policy Simulator.

## AWS EC2 Instance Metadata

* http://169.254.169.254/latest/meta-data (dynamic, meta-data, user-data)
* Used by EC2 to gather metadata
* http://169.254.169.254/latest/meta-data/local-ipv4 to get local ipv4
* No need to IAM roles.

## AWS SDK Overview.

* java, .NET, Node.js, C, etc
* When to use cli, or when to use SDK
* The AWS cli is developed in Python
* Mainly used in Lambda functions, but not only.
* In general when we need to develop applications that interact with AWS services.
* **The default region is us-east-1**
