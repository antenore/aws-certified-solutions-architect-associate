# AWS Resource Access manager (RAM)

* Share AWS resources with other AWS accounts.
* Any account or within an Organization.
* It helps avoiding resource duplication.
* VPC Subnets:
    * subnetsAll resources launched in the same subnets.
    * Must be in the same AWS Organizations.
    * Cannot share Security Groups and default VPC.
    * Participants can manage their own resources in the shared VPC.
    * Participants can't view, modify or delete resources that belongs to other partticipants or the owner.
* AWS Transit Gateway.
* Route53 Resolver Rules.
* License Manager Configurations.
* And some othhers, see https://docs.aws.amazon.com/ram/latest/userguide/shareable.html
* No additional costs
* There are default limits per Region for each account (about 5000 number of shared resources, and 20 invitation in pending status)


