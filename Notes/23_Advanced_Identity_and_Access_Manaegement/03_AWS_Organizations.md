# AWS Organizations

* Global service
* To manage multiple AWS accounts
* 1 Master account (cannot be changed)
* Other accounts are member accounts
* Member account can be part of only one organization
* Consolidated billing
* Pricing benefits from aggregated usage (volume discount)
* Account creation can be automated using API

## Strategies / use cases

* Create Accounts per:
    * Per departements
    * cost center
    * dev/test/prod
    * regulatory restrictions
    * resource isolation (ex: VPC)
    * per account service limits
    * isolated account logging

* Multi Account vs One Account Multi VPC
    * It's really to separate resources and costs.
* You can use tagging
* Enable CloudTrail on all accounts, send logs to central S3 account
* Send CloudWatch logs to central logging account
* Establish Cross Account Roles for Admin purposes

## Service Control Policies (SCP)

* White/Blacklist IAM actionsA
* They looks like IAM Policies
* Does NOT apply to the Master Account
* SCP are applied to all Users and Roles, including Root
* Scp cannot affect service-linked roles.
    * other AWS services to integrate with AWS Organizations cannot be restricted by SCP
* SCP must have an explicit Allow
* Use cases:
    * Restrict access to certain services (i.e. can't use EMR)
    * To Enforce PCI compliance by explicitely disabling services.

