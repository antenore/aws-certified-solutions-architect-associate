# AWS Single Sign-On (SSO)

* Centraly manage SSO access multiple accounts and 3rd-party apps.
* Integrates with AWS Organizations
* Supports SAML 2.0 markup
* Integrates with on-premises AD
* Permission management
* Audit with CloudTrail.
* Simpler than AssumeRoleWithSAML, where we have to set up a login portal for each identity store and interact with STS.
