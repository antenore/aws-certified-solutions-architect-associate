# Advanced IAM

## IAM Conditions

* SourceIP
    * Deny all the API calls that are not coming from a source IPs
* RequestedRegion
    * Restrict to which region the API calls are made to
* Based on tags
    * Restrict only on certains tags.
* Force MFA
    * Deny API calls if without MFA

## IAM for S3

* ListBucket permission apply at the bucket level
    * The arn is like arn:aws:s3:::test
* GetObject, PutObject, DeleteObject applies at the object level.
    * The arn is like arn:aws:s3:::test/*

## IAM Roles vs Resource Based Policies

AWS allows granting cross-account access to AWS resources, which can be done using IAM Roles or Resource Based policies

* IAM Roles is the UNIX equivalent to su
    * The user temporary gives up his/her own permissions, to take the Role permissions.
    * Most of the AWS services supports Roles.
* Resource based Policies
    * Directly attached to a resource
    * List of AWS account ID that can access the resource.
    * Supported by few AWS services.
    * Mainly used with:
        * S3
        * SNS
        * SQS
        * AWS Lambda functions
        * … see full list https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-services-that-work-with-iam.html `Resource-level permissions` column (Yes)

## IAM Permission Boundaries

* Set the maximum permissions for an IAM entity (users and roles).
* For example the user ShirleyRodriguez can only manage Amazon S3, Amazon CloudWatch, and Amazon EC2

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*",
                "cloudwatch:*",
                "ec2:*"
            ],
            "Resource": "*"
        }
    ]
}
```

* Even if we attach a Policy that would allow ShirleyRodriguez to manage an other Resource, it would not work because of the Boundaries set above.
* Can be used in combination with AWS Organization SCP
* The effective permission is the result of the merge of SCP, Permission Boundaries, Identity-based policies and Resource-based policies (if one deny, all denied)
* Normally used to allow developers to self-assign policies and manage their own permissions
* Or to delegate non administrators to create users, for instance.
* Useful to restrict one specific user instead of a whole Organization.
