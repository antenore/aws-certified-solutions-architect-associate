# Identity Federation in AWS

It uses STS behind the scenes.

* Users outside AWS to assume temporary role for accessing AWS resources.
* Type:
    * SAML 2.0
        * To integrate Active Directory / ADFS with AWS or any SAML 2.0
        * Provides access to AWS Console or CLI (temporary creds)
        * No need to create IAM users
        * Set up a trust between AWS IAM and SAML (both ways)
        * STS API: AssumeRoleWith SAML
        * Please prefer Amazon Single Sign On (SSO)
    * Custom Identity Broker
        * Only if not compatible with SAML 2.0A
        * STS API: AssumeRoile or GetFederationToken
        * Use directly AWS STS
    * Web Identity Federation with Cognito
        * Recommended way
        * Like SAML, but simpler to manage.
    * Web Identity Federation without Cognito
        * Not Recommended
        * Speak directly to AWS STS
    * Single Sign On
    * Non-SAML with AWS Microsoft AD
        * With AWS Managed Microsoft AD
            * Create your own AD in AWS, users, supports MFA.
            * Establish a trust with your own on-premises AD
        * With AD Connector
            * Users are managed on the on-premises
            * The connector is like a proxy
        * Simple AD
            * AD-compatible managed directory on AWS
            * Cannot be joined (trusted) with on-premises AD
* Using Federation there's no need to create IAM users.
