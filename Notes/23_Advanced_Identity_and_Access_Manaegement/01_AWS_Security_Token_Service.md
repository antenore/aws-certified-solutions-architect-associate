# AWS STS — Security Token Service

* Grant temporary and limited access to AWS resources
* Up to one hour (can be refreshed)
* AssumeRole (sudo?)
    * Within account: To enhance security
    * Cross Account Access: Assume role in the target account
* AssumeRole WithSAML
    * For users logged with SAML (federated)
* AssumeRoleWithWebIdentity
    * For users logged with IdP (Facebook, Google, OIDC compatible …)
    * AWS against using this, prefer Cognito instead.
* GetSessionToken
    * For MFA. When you have MFA you must use GetSessionToken

## Using STS to Assume a Role

* Define an IAM Role within your account or cross-account
* Define the principals can access this IAM Role.
* Use AWS STS to retrieve the credentials and impersonate the IAM Role (AssumeRole API)
* Temporary credentials can between 15 minutes and 1 hour.
