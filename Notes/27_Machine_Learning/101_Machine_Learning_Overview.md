# Machine Learning

## Rekognition

* Find text, objects, people, scenes in images and videos using ML.
* Facial analysis and facial search, user verification, people counting.
* Create DB of "familiar faces" or compares against celebrities.
* Use case:
    * Labelling
    * Content moderation
    * Text detection
    * Face Detection and Analysis (gender, age range, emotions, …)
    * Face search and verification
    * Celebrity Recognition
    * Pathing

## Transcribe

* Speech/audio to text

## Polly

* Text into lifelike speech

## Translate

* Natural and accurate language translation

## Amazon Lex & Connect

* Amazon Lex (→ Alexa)
    * Same technology that powers Alexa.
    * Automatic speech recognition (ASR)
    * Conversational Chat-bots and call center bots
* Amazon Connect:
    * Receive calls, create contact flows, cloud-based **virtual contact center**
    * Can integrates with other CRM or AWS
    * No upfront, 80% cheaper than tradition contact center solutions.

Phone call → Connect → Lex → Lambda → CRM

## Comprehend

* Natural Language Processing (NLP)
* Fully managed and server-less service.
* Uses ML to find insights and relationships in text
    * Language of the text
    * Extract key-phrases, places, people, brands or events
    * Positiveness, negativeness of the text
    * Text analysis using tokens
    * Automatically organizes a collection of text files by topic
* Use cases:
    * analyze customers emails to understand what makes them happy/unhappy

## SageMaker

* Fully managed service for developers and data scientists to builds ML models.

## Amazon Forecast

* Fully managed services that uses ML to deliver forecasts
* 50% more accurate than looking at the data itself.
* Reduces forecasting time from months to hours.

## Amazon Kendra

* Fully managed document search service
* Extract answers from within a document (text, pdf, HTML, MS suite, FAQ, … )
* Natural language search capabilities.
* Incremental learning.

## Personalize

* Fully managed ML-service for real-time personalized recommendations
* Like recommendation in Amazon.com

## Textract

* Automatically extract text, handwriting and data from scanned documents using AI and ML.
