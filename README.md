# AWS Certified Solutions Architect Associate

These are my personal hand-taken notes to prepare the AWS Certified Solution Architect Associate certification (SAS-CO2).

They are based on these courses and preparation exams (so far):

* Udemy - Ultimate AWS Certified Solutions Architect Associate 2022, by Stephane Maarek.
* Tutorials Dojo - AWS Certified Solutions Architect Associate SAA-C02 / SAA-C03 [Video Course]
* Tutorials Dojo - AWS Certified Solutions Architect Associate Practice Exams 2022

## TODO - Questions asked, not included in the Udemy course

* Aws Glue
* Making views, and giving column-based permissions
